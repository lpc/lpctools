##
# @author Colin Barschel (colin.barschel@cern.ch)
# @file describe a data file (path, name, type)
#
import string, sys
import os.path
import zipfile
import tarfile
import readconfig

config = readconfig.readconfig( "lumi_config.json" )

YEAR = config['year']
LPC_DATA_DIR = os.path.join( config['workBaseDir'], config['year'], "measurements" )

## Define a unique file name
class cfile():
    #datadir=LPC_DATA_DIR
    
    ## cfile initiation
    # @param expt = experiment (ATLAS,CMS,ALICE,LHCb)
    # @param fillnr Fill number (integer)
    # @param bucket Bunch ID (integer, = 0 if not needed)
    def __init__(self,expt='',fillnr=0,ftype='l',bucket=0):
        self.ftype=ftype
        self.expt=expt
        self.fillnr=int(fillnr)
        self.bucket=int(bucket)
        if os.path.isdir( os.path.join(LPC_DATA_DIR, expt, "lumi") ):
            self.datadir = os.path.join(LPC_DATA_DIR, expt, "lumi")
        else:
            self.datadir = os.path.join(LPC_DATA_DIR, expt)

        if self.fillnr == 1753 and expt != 'CMS':
            self.fillnr = 1750

        # separator to read data file
        if expt == 'ALICE' and fillnr <= 1622 and ftype != 'r':
            self.spl=','
        else:
            self.spl = None
        
        #self.spl = None
        
        
    ## return file string
    # @param ftype l=lumi r=lumireg, b=beamgas
    # ftypes is the ftype string
    def f(self,ftype=''):
        if ftype == '': ftype = self.ftype
        if ftype == 'l': ftypes = 'lumi'
        elif ftype == 'r': ftypes = 'lumireg'
        elif ftype == 's': ftypes = 'summary'
        elif ftype == 'b1': ftypes = 'beam1'
        elif ftype == 'b2': ftypes = 'beam2'
        elif ftype == 'zip':
            return str(self.fillnr)+'.zip'
        elif ftype == 'tgz':
            return str(self.fillnr)+'.tgz'
        else:
            return ftype
        
        if self.bucket != 0:
            #print 'using bucket',self.bucket
            file = str(self.fillnr)+'_'+ftypes+'_'+str(self.bucket)+'_'+self.expt+'.txt'
        else:
            file = str(self.fillnr)+'_'+ftypes+'_'+self.expt+'.txt'
        
        return file
    
    ## return file name with full path as string
    # @param ftype l=lumi r=lumireg, b=beamgas        
    def ff(self,ftype=''):
        fname = self.f(ftype)
        ffull = self.datadir+'/'+str(self.fillnr)+'/'+fname

        if ftype == 'zip' or ftype == 'tgz':
            ffull = self.datadir+'/'+fname
            
        
        return ffull

    def ffile(self,ftype=''):
        file = self.ff(self.ftype)
        zfile = self.ff('zip')
        tgfile = self.ff('tgz')

        #print file,zfile
        #print self.ff(self.ftype)
        
        f = None

        try:
            if os.path.isfile(zfile):
                zf = zipfile.ZipFile(zfile)
                f = zf.open(str(self.fillnr)+'/'+self.f())
                #f = zf.open(self.f())
                #print f
                return f
            if os.path.isfile(tgfile):
                tf = tarfile.open(tgfile)
                f = tf.extractfile(str(self.fillnr)+'/'+self.f())
                return f
            if os.path.isfile(file):
                f = open(file, 'r')
                return f



        except:
            return None
        
    
    def exist(self):
        #print self.ff(self.ftype)
        
        if os.path.isfile(self.ff(self.ftype)):
            return True
        if os.path.isfile(self.ff('zip')):
            zf = zipfile.ZipFile(self.ff('zip'))
            #print self.ff('zip'),str(self.fillnr)+'/'+self.f()
            if str(self.fillnr)+'/'+self.f() in zf.namelist():
                return True
        if os.path.isfile(self.ff('tgz')):
            tf = tarfile.open(self.ff('tgz'))
            #print self.ff('tgz'),str(self.fillnr)+'/'+self.f()
            if str(self.fillnr)+'/'+self.f() in tf.getnames():
                tf.close()
                return True
            tf.close()

        return False


## Test the cfile class
if __name__ == "__main__":
    """
    examples
    python -i cfile.py CMS 1616 1001
    python -i cfile.py CMS 1616 0

    """
    import sys
    if len(sys.argv) > 3:
        expt = sys.argv[1]
        fillnr = int(sys.argv[2])
        bucket = int(sys.argv[3])
    else:
        print "Enter expt fillnr bucket as argument"
        sys.exit(1)
    
    
    for t in ('l','r','s','b1','b2'):
        F = cfile(expt,fillnr,t,bucket)
        print F.f(t)
        print F.ff(t)
        
        # check if file exists
        if F.exist():
            print 'file exists'
        else:
            print 'file does not exists'
