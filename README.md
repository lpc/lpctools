# Project lpc/lpctools

This projects contains the sources for the lpc tools used to 
provide various services to the LHC community. Most of these
services are available via the lpc website which is maintained
in a separate git repository. The tools collected here are run
on lxplus and not on the microsoft webserver of the official 
lpc website. Some of the tools are run by the afs webserver
which provides some services for the main lpc website. The 
reason of this setup is that the microsoft servers of CERN do
not have access to data of the scientific community on afs.

LPC stands for LHC Programme Coordinator.

Initiators of this repository are Christoph Schwick and Jamie Boyd.
(January 2017)
