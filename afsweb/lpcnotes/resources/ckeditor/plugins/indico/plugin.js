// Register the plugin within the editor.
CKEDITOR.plugins.add( 'indico', {

    // This plugin requires the Widgets System defined in the 'widget' plugin.
    requires: 'widget',
    
    // Register the icon used for the toolbar button. It must be the same
    // as the name of the widget.
    icons: 'indico',
    hidpi: true,
    // The plugin initialization logic goes inside this method.
    init: function( editor ) {

	// Register the editing dialog.
	CKEDITOR.dialog.add( 'indico', this.path + 'dialogs/indico.js' );
	
	// Register the indico widget.
	editor.widgets.add( 'indico', {
	    // Allow all HTML elements, classes, and styles that this widget requires.
	    // Read more about the Advanced Content Filter here:
	    // * http://docs.ckeditor.com/#!/guide/dev_advanced_content_filter
	    // * http://docs.ckeditor.com/#!/guide/plugin_sdk_integration_with_acf
	    allowedContent:
	    'div(!indico,align-left,align-right,align-center){width,height};' +
		'table{width};tr;td;p(indico-title,indico-author){display,text-align}[onclick];' +
		'a(indico-link)[href,onclick];'+
		'a(twiki-link)[href,onclick];'+
		'iframe(!indico-iframe)[src,width,height];'+
		'<script>',
	    // Minimum HTML which is required by this widget to work.
	    requiredContent: 'div(indico)',
	    
	    // Define nested editable areas.
	    editables: {
		title: {
		    // Define CSS selector used for finding the element inside widget element.
		    selector: '.indico-title',
		    // Define content allowed in this nested editable. Its content will be
		    // filtered accordingly and the toolbar will be adjusted when this editable
		    // is focused.
		    allowedContent: 'strong'
		},
		author: {
		    selector: '.indico-author',
		    allowedContent: 'em'
		}
	    },

	    // Define the template of a new Indico widget.
	    // The template will be used when creating new instances of the Simple Box widget.
	    template:
	    '<div class="indico">' +
		'<table style="width:100%"><tr><td><p class="indico-title"><Title</p></td><td><p class="indico-author" style="text-align:right"></p></td></tr><tr><td><a class="indico-link">indico</a></td><td style="text-align:right"><a class="twiki-link">twiki</a></td></tr></table><iframe class="indico-iframe"></iframe>' +
             '</div>',
	    
	    // Define the label for a widget toolbar button which will be automatically
	    // created by the Widgets System. This button will insert a new widget instance
	    // created from the template defined above, or will edit selected widget
	    // (see second part of this tutorial to learn about editing widgets).
	    //
	    // Note: In order to be able to translate your widget you should use the
	    // editor.lang.indico.* property. A string was used directly here to simplify this tutorial.
	    button: 'Create an indico window.',
	    
	    // Set the widget dialog window name. This enables the automatic widget-dialog binding.
	    // This dialog window will be opened when creating a new widget or editing an existing one.
	    dialog: 'indico',

	    // Check the elements that need to be converted to widgets.
	    //
	    // Note: The "element" argument is an instance of http://docs.ckeditor.com/#!/api/CKEDITOR.htmlParser.element
	    // so it is not a real DOM element yet. This is caused by the fact that upcasting is performed
	    // during data processing which is done on DOM represented by JavaScript objects.
	    upcast: function( element ) {
		// Return "true" (that element needs to converted to a Simple Box widget)
		// for all <div> elements with a "indico" class.
		return element.name == 'div' && element.hasClass( 'indico' );
	    },

	    // When a widget is being initialized, we need to read the data ("align" and "width" and "height")
	    // from DOM and set it by using the widget.setData() method.
	    // More code which needs to be executed when DOM is available may go here.
	    init: function() {
		var iframe = this.element.getFirst( function(node) { return node.hasClass("indico-iframe"); } );

		var width = iframe.getStyle( 'width' );
		if ( width )
		    this.setData( 'width', width );
		
		var height = iframe.getStyle( 'height' );
		if ( height )
		    this.setData( 'height', height );
		
		if ( this.element.hasClass( 'align-left' ) )
		    this.setData( 'align', 'left' );
		if ( this.element.hasClass( 'align-right' ) )
		    this.setData( 'align', 'right' );
		if ( this.element.hasClass( 'align-center' ) )
		    this.setData( 'align', 'center' );

		var iframe = this.element.getFirst( function(node) { return node.hasClass("indico-iframe"); } );
		if (iframe.hasAttribute( "src" )) {
		    this.setData( 'frameurl', iframe.getAttribute( "src" ));
		}
		var linklist = this.element.getElementsByTag( 'a' );
                for (var i=0; i<linklist.count(); i++ ) {
                    link = linklist.getItem(i);
                    if ( link.hasClass( "indico-link" ) ) {
		        if ( link.hasAttribute( "href" ) ){
		            //debug( "found href " + indicolink.getAttribute( 'href' ) );
		            this.setData( 'indicourl', link.getAttribute( 'href' ));
                        }
                    } else if( link.hasClass( "twiki-link" ) ) {
                        if ( link.hasAttribute( "href" ) ) {
                            this.setData( 'twikiurl', link.getAttribute( 'href' ));
                        }
                    }
		}

	    },
	    
	    // Listen on the widget#data event which is fired every time the widget data changes
	    // and updates the widget's view.
	    // Data may be changed by using the widget.setData() method, which we use in the
	    // Indico dialog window.
	    data: function() {
		// Check whether "width" widget data is set and remove or set "width" CSS style.
		// The style is set on widget main element (div.indico).
		var iframe = this.element.getFirst( function(node) { return node.hasClass("indico-iframe"); } );
		if ( this.data.width == '' )
		    iframeremoveStyle( 'width' );
		else
		    iframe.setStyle( 'width', this.data.width );
		
		if ( this.data.height == '' )
		    iframe.removeStyle( 'height' );
		else
		    iframe.setStyle( 'height', this.data.height );

		// Brutally remove all align classes and set a new one if "align" widget data is set.
		this.element.removeClass( 'align-left' );
		this.element.removeClass( 'align-right' );
		this.element.removeClass( 'align-center' );
		if ( this.data.align )
		    this.element.addClass( 'align-' + this.data.align );

		var titlelink = this.element.findOne( 'p' );
		if ( this.data.frameurl ) {
		    iframe.setAttribute( 'src', this.data.frameurl );
		    titlelink.setAttribute( "onclick", "return setFrameUrl( event, '" + this.data.frameurl + "')");		    
		} else {
		    iframe.removeAttribute( 'src' );
		}

		var linklist = this.element.getElementsByTag('a');
                for (var i = 0; i<linklist.count(); i++) {
                    link = linklist.getItem(i);
                    if ( link.hasClass( "indico-link" ) ) {
		        if ( this.data.indicourl ) {
                            link.removeAttribute( 'style' );
		            link.setAttribute( 'href', this.data.indicourl );
		            link.setAttribute( 'onclick', "return setFrameUrl(event, '" + this.data.indicourl + "')");
		        } else {
                            link.setAttribute( 'style', 'display:none' )
		            link.removeAttribute( 'href' );
		            link.removeAttribute( 'onclick' );
		        }
                    } else if (  link.hasClass( "twiki-link" ) ) {
		        if ( this.data.twikiurl ) {
                            link.removeAttribute( 'style' );
		            link.setAttribute( 'href', this.data.twikiurl );
		            link.setAttribute( 'onclick', "return setFrameUrl(event, '" + this.data.twikiurl + "')");
		        } else {
                            link.setAttribute( 'style', 'display:none' );
		            link.removeAttribute( 'href' );
		            link.removeAttribute( 'onclick' );
		        }
                    }
                }
	    }
	} );
    }
} );
