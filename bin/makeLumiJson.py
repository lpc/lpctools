#!/usr/bin/python

import tarfile
import csv
import glob
import sys
import datetime

DIRBASE="/eos/project/l/lpc/public/MassiFiles/2016/measurements"


def process( exp, fill, of, integlumi ):
    filln = fill['fill']
    print "processing fill " + filln + " for " + exp
    tgz = DIRBASE + "/" + exp + "/" +str(filln)+".tgz"
    tf = tarfile.open( tgz, "r" )
    member = str(filln) + "/" + str(filln) + "_summary_" + exp + ".txt"
    sf = tf.extractfile( member )
    summarr = sf.readline().split(' ')
    fill['integ_fillLumi'] = float(summarr[3])
    fill['peak_fillLumi'] = summarr[2]
    fill['timeSB_massifile'] = float(summarr[1]) - float(summarr[0])
    integlumi = integlumi + float(fill['ilumi'])/1000000000.0
    fill['year_integ_lumi'] = integlumi
    of.write( filln + "," + fill['startDate'] + "," + fill['timeSB']+ "," + str(fill['integ_fillLumi']) + "," + str(fill['peak_fillLumi']) + "," + repr(integlumi) + "\n" )
    return integlumi
    
sbfills = []
state = 'no'
fill = 0
entry = {}
with open( DIRBASE + '/beam_modes.csv', 'rb' ) as csvfile:
    reader = csv.DictReader( csvfile, fieldnames=('timestamp', 'mode', 'filln'), delimiter=',' )
    reader.next()
    for row in reader:
        print row
        if state == 'no' and row['mode'] == '11':
            entry['startUnix'] = int(row['timestamp'])
            entry['startDate'] = datetime.datetime.fromtimestamp( entry['start']/1000 ).strftime('%d/%m/%Y')
            entry['startTime'] = datetime.datetime.fromtimestamp( entry['start']/1000 ).strftime('%H:%H:%S')
            fill = row['filln']
            entry['fill'] = fill
            state = "sb"
        elif state == "sb" and ( row['mode'] !='11' or row['filln'] != fill) :
            entry['stopUnix'] = int(row['timestamp'])
            state = "no"
            entry['timeSB'] = entry['stop'] - entry['start']
            sbfills.append( entry )
            entry = {}

print repr( sbfills )

cmsof = open( 'cms_outfile.csv', 'w')
atlasof = open( 'atlas_outfile.csv', 'w')

cmsinteg = 0.0
atlasinteg = 0.0
for fill in sbfills:
    cmsinteg = process( 'CMS', fill, cmsof, cmsinteg )
    atlasinteg = process( 'ATLAS', fill, atlasof, atlasinteg )

        
