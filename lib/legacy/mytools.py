##
# @author Colin Barschel (colin.barschel@cern.ch)
# @file various tools for io and functions
#
import decimal
from array import array
from scipy import arange, exp
import numpy as np
import scipy as sp
#from scipy.interpolate import interp1d
from scipy import arange
import pylab as P
import matplotlib as mpl
import bz2
import gzip
import os.path,time
import calendar
import types
from time import mktime,gmtime
from datetime import datetime

##########################
### functions
##########################
gnorm = lambda sigma: 1/(np.sqrt(2*np.pi)*abs(sigma))
#xi = lambda b, N0, a: (a*N0)/b
xi = lambda tb, ta: ta/tb

mconst = lambda p, x: p * x
mconstResid = lambda p, x, y: (mconst(p,x)-y)
mconstResidErr = lambda p, x, y, yerr: mconstResid(p,x,y)/yerr

mline = lambda p, x: p[0] * x + p[1]
mlineResid = lambda p, x, y: (mline(p,x)-y)
mlineResidErr = lambda p, x, y, yerr: mlineResid(p,x,y)/yerr

mdecay_ = lambda x, p: p[0] * sp.exp(-p[1]*x)
mdecay = lambda p, x: p[0] * sp.exp(-p[1]*x)
mdecayResid = lambda p, x, y: (mdecay(p,x)-y)

ldecay = lambda p, x: p[0] * sp.exp(-2*(1/p[1])*x)
ldecayResid = lambda p, x, y: (ldecay(p,x)-y)
ldecayResidErr = lambda p, x, y, yerr: ldecayResid(p,x,y)/yerr

convol = lambda p: np.sqrt(p[0]**2 + p[1]**2)
convolResid = lambda p, x: (convol(p)-x)
convolResidErr = lambda p, x, xerr: convolResid(p,x)/xerr

# resolution parametrisation p=[A,B,C]
# x = number of tracks
# y = resolution
sigmares = lambda p, x: (p[0]/x**p[1])+p[2]
sigmaresResid = lambda p, x, y: (sigmares(p,x)-y)
sigmaresResidErr = lambda p, x, y, yerr: sigmaresResid(p,x,y)/yerr

ldecayxi = lambda p, x: p[0] / ( ( (1+(p[2]/p[1]) * sp.exp((1/p[1])*x) ) - (p[2]/p[1]) )**2 )
ldecayxiResid = lambda p, x, y: (ldecayxi(p,x)-y)
ldecayxiResidErr = lambda p, x, y, yerr: ldecayxiResid(p,x,y)/yerr

moffset = lambda p, x, slope: slope * x + p[0]
moffsetResid = lambda p, x, y, slope: (moffset(p,x,slope)-y)
moffsetResidErr = lambda p, x, y, yerr, slope: moffsetResid(p,x,y,slope)/yerr

# p = [A,mu,sigma]
mgauss = lambda p, x: p[0]*gnorm(p[2])*sp.exp(-(x-p[1])**2/(2.0*p[2]**2))
mgaussResid = lambda p, x, y: (mgauss(p,x)-y)
mgaussResidErr = lambda p, x, y, yerr: mgaussResid(p,x,y)/yerr

# convolved gaussian sigma = sqrt(s_res^2 + s_beam^2)
mgaussconv = lambda p, x,sigma: p[0]*gnorm(np.sqrt(sigma**2+p[2]**2))*sp.exp(-(x-p[1])**2/(2.0*(sigma**2+p[2]**2)))
mgaussconvResid = lambda p, x, y,sigmares: (mgaussconv(p,x,sigmares)-y)
mgaussconvResidErr = lambda p, x, y, yerr,sigmares: mgaussconvResid(p,x,y,sigmares)/yerr

rho = lambda s1,s2: s1/s2
rhocorr = lambda rho: (2*rho)/(rho**2 + 1)

dmucorr = lambda dmu,s1,s2: sp.exp(-0.5 * ( (dmu**2)/(s1**2 + s2**2) )  )

trueSigmaZ = lambda szlumi,theta,sxlumi: np.sqrt(np.cos(theta)**2 * 2 * (szlumi**(-2) - (np.tan(theta)**2 * sxlumi**(-2)) )**(-1) )
trueSigmaX = lambda sxlumi,theta: np.sqrt( 2 * np.cos(theta)**2 * sxlumi**2 )

# sigmaz,sigmax are the true width (not lumi spot)
corrtheta = lambda sigmaz,sigmax,theta: (1 + ((sigmaz/sigmax) * np.tan(theta))**2)**(-0.5)

# using Gaussian sigma = sqrt(sn^2+s^2)
# weight list relative number of entries in a Gaussian e/etot
# sigmas list of velo resolutions (1 per Gaussian)
def mgaussmultconv(p, x, weight, sigmas):
    result = 0
    for i in range(0,len(sigmas)):
        result += (weight[i] * mgaussconv(p, x, sigma=sigmas[i]))
    return result
def mgaussmultconvResidErr(p, x, y,yerr,weight,sigmas):
    return mgaussmultconvResid(p, x, y,weight,sigmas)/yerr
def mgaussmultconvResid(p, x, y,weight,sigmas):
    return mgaussmultconv(p,x,weight,sigmas)-y

# Using gaussians
# sa list relative number of entries in a Gaussian e/etot
# sn list of velo resolutions (1 per Gaussian)

def mgaussmults(p, x, weight, sigmas):
    result = 0
    for i in range(0,len(sigmas)):
        result += (weight[i] * mgauss([p[0],p[1],sigmas[i]], x))
    return result
def mgaussmult(p, x):
    result = 0
    for i in range(0,len(p)/3):
        result += (mgauss(p[i*3:i*3+3], x))
    return result
def mgaussmultResid(p, x, y):
    return mgaussmult(p,x)-y
def mgaussmultResidErr(p, x, y,yerr):
    return mgaussmultResid(p,x,y)/yerr

def mu_bb(s1,s2,m1,m2):
    return ((m1*s2**2) + (m2*s1**2))/(s1**2 + s2**2)
def sigma_bb(s1,s2):
    return np.sqrt((s1**2 * s2**2)/(s1**2 + s2**2))
## take 6 measurements of beam spot width on CMS(ATLAS), ALICE, LHCb
# and solve the individual beam width
#
# measurements order:
#   IP1       IP2       IP8
#    0         1         2
#    3         4         5
#
# parameters order:
#  p0 p1     p0 p5     p2 p1
#  p4 p3     p2 p3     p4 p5
#
# @param p is parameters list of unknown
# @param y is list of measured values (sigma_bb)
def sigma_b(p):
    f = np.array(
        [sigma_bb(p[0],p[1]),
        sigma_bb(p[0],p[5]),
        sigma_bb(p[2],p[1]),
        sigma_bb(p[4],p[3]),
        sigma_bb(p[2],p[3]),
        sigma_bb(p[4],p[5])])
    return f

## residuals of sigma_b
def sigma_bResid(p,y,yerr):
    return (sigma_b(p)-y)/yerr

# y = s1,s2,sbb,mu1,mu2,mubb
# p = s1,s2,mu1,mu2
# works if p,y are numpy arrays: p=np.array(p)
# does not work if p is a list
def constrainResidErr(p, y, yerr,mu=True):
    # p are the parameters to fit 2 sigmas 2 mus
    # y are the measured values 3 sigmas 3 mus
    # f are the computed values
    if not mu: # only sigma are used    
        f = np.array([p[0],p[1],sigma_bb(p[0],p[1]),\
                      p[2],p[3],y[5]])
    else:
        f = np.array([p[0],p[1],sigma_bb(p[0],p[1]),\
                      p[2],p[3],mu_bb(p[0],p[1],p[2],p[3])])
    return (f-y)/yerr


#########################################
# Velo resolution parametrization
def resNtracks(n,axis='X'):
    # sigma, delta, epsilon
    px = [0.107,-3.0,-0.0038]
    py = [0.106,-2.5,-0.004]
    p = []
    if axis == 'X': p = px
    elif axis == 'Y': p = py
    return (p[0]/(n**(0.5+(p[1]/n**2))))+p[2]

def correctionZ(z,beam=1, axis='X'):
    # R(z) = m + b * z
    m = 0
    p = []
    result = []
    if beam == 1:
        # m, b
        px = [1.06,-0.0019]
        mx = 1.22
        py = [1.06,-0.0018]
        my = 1.23
        if axis == 'X':
            p = px
            m = mx
        elif axis == 'Y':
            p = py
            m = my
        for zi in z:
            if zi >= -1000 and zi < -100:
                result.append(p[0] + zi * p[1])
            elif zi >= -100 and zi <= 500:
                result.append(mx)
            else:
                result.append(1)

    elif beam == 2:
        # m, b
        px1 = [0.83,0.0014]
        py1 = [0.94,0.0012]
        px2 = [0.3,0.0018]
        py2 = [0.2,0.0018]
        if axis == 'X':
            p1 = px1
            p2 = px2
        elif axis == 'Y':
            p1 = py1
            p2 = py2
        for zi in z:
            if zi >= 0 and zi < 700:
                result.append(p1[0] + zi * p1[1])
            elif zi >= 700 and zi <= 1000:
                result.append(p2[0] + zi * p2[1])
            else:
                result.append(1)

    return result

# given N tracks and Z, return resolution
def resolution(n,z,beam=1, axis='X'):
    if beam == 3:
        return resNtracks(n,axis)
    else:
        return resNtracks(n,axis) * correctionZ(z,beam,axis)

def rfBucketToBunchId(rfbucket):
    if type(rfbucket) == int or type(rfbucket) == float:
        return int((rfbucket + 9)/10)
    else:
        l = []
        for rfb in rfbucket:
            l.append(int((rfb + 9)/10))
    return l
def bunchIdToRfBucket(bunchid):
    return int((bunchid*10)-9)

##########################
### IO and cuts
##########################

## read ASCII file created by ntupleToTxt.py
# the header words are the dict keys
# each key is a np.array
def readtxt(file, entries=0):
    name,ext = os.path.splitext(file)
    if ext == '.bz2':
        f = bz2.BZ2File(file, 'rb')
    elif ext == '.gz':
        f = gzip.open(file, 'rb')
    else:
        f = open(file, 'r')

    haveheader = False
    d = {}
    
    while not haveheader:
        line = f.readline()

        if line[0] == '#':
            continue
        
        # find any of those = header line
        # take the 2nd column to find the header
        # the first word might also be present earlier in the header
        # this works for the files
        # - fills2010.txt
        # - 1_13_8_8_8.txt and co.
        # - Fill_XXXX_reduced.txt.bz2
        start_word = ['dd','evt','beam1']
        #if line[0] == '#': line = line[1:] # strip '#' char

        # store data in a dictionary
        # read the header   
        fields = line.strip().split()

        if len(fields) > 1:# and fields[1] in start_word:
            haveheader = True
            print fields
            for field in fields:
               d[field] = array('d') # array of double

    print 'read data...'
    l = 0
    for line in f:
        #print line
        vals = line.strip().split()
        for i in range(len(vals)):
            d[fields[i]].append(float(vals[i]))
        l += 1
        if l % 200000 == 0: print "read",l,"lines"
        if entries != 0 and l == entries: break

    print l, " entries done"
    
    for field in d:
        print field
        d[field] = np.array(d[field]) # convert to numpy array

    print "closing file "+file
    
    f.close()

    
    #print fields
    
    return d


### store BPM vector into dict
# key 0 == unix time
def readBpmCsv(file):
    print 'read',file

    spl=','
    name,ext = os.path.splitext(file)
    if ext == '.bz2':
        f = bz2.BZ2File(file, 'rb')
    elif ext == '.gz':
        f = gzip.open(file, 'rb')
    else:
        f = open(file, 'r')

    #haveheader = False
    d = {}
    
    # head is composed of 2 lines
    #while not haveheader:
    for l in (0,1,2):
        line = f.readline()
        if l == 2:
            fields = line.strip(spl).split(spl)
            print 'vector length',len(fields)

            for i in range(len(fields)):
                d[i] = array('d') # array of int
                #d[i].append(int(fields[i].split('.')[0]))
                #print fields[i], len(fields[i])
                d[i].append(float(fields[i]))
        
    l = 0
    for line in f:
        vals = line.rstrip('\r\n').split(spl)
        
        for i in range(len(vals)):
            d[i].append(float(vals[i]))
                    
        l += 1
        if l % 200000 == 0: print "read",l,"lines"

    
    for field in d:
        d[field] = d[field][:-5]
        d[field] = np.array(d[field]) # convert to numpy array

    d[0] = d[0]/1000 # convert units  ms => s 

    f.close()

    print l, " entries"
    
    return d


### store FBCT vector into dict
# key 0 == unix time
def readFbctCsv(file):
    #print 'read',file

    spl=','
    name,ext = os.path.splitext(file)
    if ext == '.bz2':
        f = bz2.BZ2File(file, 'rb')
    elif ext == '.gz':
        f = gzip.open(file, 'rb')
    else:
        f = open(file, 'r')

    #haveheader = False
    d = {}
    
    # head is composed of 2 lines
    #while not haveheader:
    for l in (0,1,2):
        line = f.readline()
        if l == 2:
            fields = line.strip(spl).split(spl)
            #print 'vector length',len(fields)

            for i in range(len(fields)):
                d[i] = array('d') # array of int
                #d[i].append(int(fields[i].split('.')[0]))
                #print fields[i], len(fields[i])
                d[i].append(float(fields[i]))
        
    l = 0
    for line in f:
        vals = line.rstrip('\r\n').split(spl)
        
        for i in range(len(vals)):
            d[i].append(float(vals[i]))
                    
        l += 1
        if l % 200000 == 0: print "read",l,"lines"

    
    for field in d:
        d[field] = d[field][:-5]
        d[field] = np.array(d[field]) # convert to numpy array

    d[0] = d[0]/1000 # convert from ms to s

    f.close()

    print l, " entries"
    
    return d

### read CAL_PRE_OFFSETS vectors from DB
# looks like this (fill 1637):
"""
VARIABLE: LHC.BCTDC.A6R4.B1:CAL_PRE_OFFSETS
Timestamp (UNIX Format),Array Values
1300561693317,1.544,1.66133333333333,5.186,22.465
VARIABLE: LHC.BCTDC.A6R4.B2:CAL_PRE_OFFSETS
Timestamp (UNIX Format),Array Values
1300561699421,-0.981666666666666,-2.764,0.091,7.13533333333333
VARIABLE: LHC.BCTDC.B6R4.B1:CAL_PRE_OFFSETS
Timestamp (UNIX Format),Array Values
1300561696365,0.273666666666667,-0.191,3.382,18.991
VARIABLE: LHC.BCTDC.B6R4.B2:CAL_PRE_OFFSETS
Timestamp (UNIX Format),Array Values
1300561702492,0.301,-0.583,3.14066666666667,18.0903333333333
"""
# there is a bug with the LD command (from logginf DB)
# output is not unix time
def readCalPreOffsets(file):
    #print 'read',file

    spl=','
    name,ext = os.path.splitext(file)
    if ext == '.bz2':
        f = bz2.BZ2File(file, 'rb')
    elif ext == '.gz':
        f = gzip.open(file, 'rb')
    else:
        f = open(file, 'r')

    #haveheader = False
    d = {}
    
    var = ''
    for line in f:
        if 'VARIABLE' in line:
            var = line.split()[1]
            continue
            
        if 'Timestamp' in line:
            continue
        
        vals = line.rstrip('\r\n').split(spl)

        d[var] = []
        for i in range(len(vals)):
            if i == 0:
                d[var].append(timeDateEpoch(vals[i]))
            else:
                d[var].append(float(vals[i]))
                    
    for field in d:
        d[field] = np.array(d[field]) # convert to numpy array
        d[field][0] = d[field][0]/1000 # convert from ms to s

    f.close()

    return d

### store DB values (timber) into dict
# key 0 == unix time
def readDbCsv(file,lastifempty=False,type='l'):
    #print 'read',file

    spl=','
    name,ext = os.path.splitext(file)
    if ext == '.bz2':
        f = bz2.BZ2File(file, 'rb')
    elif ext == '.gz':
        f = gzip.open(file, 'rb')
    else:
        f = open(file, 'r')

    #haveheader = False
    d = {}
    
    # head is composed of 1 line = keys
    line = f.readline()
    fields = line.rstrip('\r\n').strip(spl).split(spl)
    fields[0] = 'date'
    print fields,len(fields)

    for field in fields:
        d[field] = array('d') # array of int or dec
    
        
    l = 0
    for line in f:
        vals = line.rstrip('\r\n').split(spl)
        #print vals
        
        for i in range(len(fields)):
            if len(vals[i]) == 0: # no value
                if lastifempty and len(d[fields[i]]) > 0:
                    d[fields[i]].append(d[fields[i]][-1])
                else:
                    d[fields[i]].append(0)
            else:
                #d[fields[i]].append(int(vals[i]))
                d[fields[i]].append(float(vals[i]))
                    
        l += 1
        if l % 200000 == 0: print "read",l,"lines"

    
    for field in d:
        d[field] = np.array(d[field]) # convert to numpy array

    d['date'] = d['date']/1000 # convert from ms to s

    f.close()

    print l, " entries"
    
    return d


# detect steps in continuous array
# correspond to a jump in data (e.g. injection, new current)

def detectSteps(fld,diff):
    dsteps = []


    for i in range(5,len(fld)-1):
        mavg = np.average(fld[i-5:i]) # average of last 5 points
        #diff = 1e12 # use 20 for linear3; 100 otherwise

        excl = range(i-10,i+10)
        #if abs(fld[i+1] - mavg) > diff \ # positive and negative step
        if fld[i+1] - mavg > diff \
                and len([val for val in dsteps if val in excl])==0:

            #print 'step',i,fld[i+1],fld[i],fld[i-1],mavg,fields[0]

            dsteps.append(i+1)


    return dsteps
    
## read ASCII file from LPC
# there is no header
# time = in seconds since january 1, 2010, 00:00:00  (CET).
# @param file the file name
# @param headers list of string to define the each column
# @param entries the number of entries to read (0 == all)
def readlpc2010(file, fields=[''],entries=0):
    name,ext = os.path.splitext(file)
    if ext == '.bz2':
        f = bz2.BZ2File(file, 'rb')
    elif ext == '.gz':
        f = gzip.open(file, 'rb')
    else:
        f = open(file, 'r')

    d = {}
    for h in fields:
        d[h] = array('d')

    l = 0
    warned = False
    for line in f:
        #print line
        vals = line.strip().split()
        if len(fields) != len(vals) and not warned:
            print "WARNING: ",str(len(fields))," fields expected, but found",str(len(vals)),"file",file
            warned = True
        
        if len(vals) == 0: # due to empty line
            continue
        
        for i in range(len(fields)):
            d[fields[i]].append(float(vals[i]))
        l += 1
        if l % 200000 == 0: print "read",l,"lines"
        if entries != 0 and l == entries: break

    for field in d:
       d[field] = np.array(d[field]) # convert to numpy array

    f.close()

    #print l, " entries"
    #print fields
    
    return d

## read ASCII file from LPC
# there is no header
# time = in seconds since january 1, 2010, 00:00:00  (CET).
# @param file the file name
# @param headers list of string to define the each column
# @param entries the number of entries to read (0 == all)
def readlpc(file, fields=[''],entries=0,spl=None):
    if type(file) == types.FileType:
        f = file
    # probably an open zip archive
    elif type(file).__name__ == 'instance' or type(file).__name__ == 'ExFileObject' or type(file).__name__ == 'ZipExtFile':
        f = file
    else:
        name,ext = os.path.splitext(file)
        if ext == '.bz2':
            f = bz2.BZ2File(file, 'rb')
        elif ext == '.gz':
            f = gzip.open(file, 'rb')
        else:
            f = open(file, 'r')

    d = {}
    for h in fields:
        d[h] = array('d')

    l = 0
    warned = False
    for line in f:
        #print line
        if line[0] == '#':
            continue
        
        vals = line.strip().split(spl)
        if len(fields) != len(vals) and not warned:
            print "WARNING: ",str(len(fields))," fields expected, but found",str(len(vals)),"file",file,'line',l
            warned = True
        
        if len(vals) == 0: # due to empty line
            continue
        
        for i in range(len(fields)):
            d[fields[i]].append(float(vals[i]))
        l += 1
        if l % 200000 == 0: print "read",l,"lines"
        if entries != 0 and l == entries: break

    for field in d:
       d[field] = np.array(d[field]) # convert to numpy array

    f.close()

    #print l, " entries"
    #print fields
    
    return d


# FBCT time average (rebinning)
def tavgfbct(d,dt=300,t1=0):
    keys = d.keys()
    nd = {}
    # first copy extra dict fields (e.g. x_avg)
    #for k in d.keys():
    #    nd[k] = d[k][:]
    for k in keys:
        nd[k] = array('l')

    if t1 == 0:
        t1 = d[0][0]

    t2 = t1 + dt

    j = 0 # start index of time interval

    while t2 < d[0][-1]:

        idx = np.where(d[0][np.where(d[0]<t2)]>=t1)
        if len(idx[0]) > 0:

            for k in keys:
                if k == 0:
                    nd[k].append(int((t1+t2)/2.))
                else:
                    nd[k].append(int(np.average(d[k][idx])))

        t1 = t2
        t2 += dt

    for field in nd:
        nd[field] = np.array(nd[field]) # convert to numpy array

    return nd

def tavgdcct(d,dt=300,t1=0):
    keys = d.keys()
    nd = {}
    # first copy extra dict fields (e.g. x_avg)
    #for k in d.keys():
    #    nd[k] = d[k][:]
    for k in keys:
        nd[k] = array('l')

    if t1 == 0:
        t1 = d['date'][0]

    t2 = t1 + dt

    while t2 < d['date'][-1]:

        idx = np.where(d['date'][np.where(d['date']<t2)]>=t1)
        if len(idx[0]) > 0:

            for k in keys:
                if k == 'date':
                    nd[k].append(int((t1+t2)/2.))
                else:
                    idxz = np.where(d[k][idx]!=0)
                    nd[k].append(int(np.average(d[k][idx][idxz])))

        t1 = t2
        t2 += dt

    for field in nd:
        nd[field] = np.array(nd[field]) # convert to numpy array

    return nd

## read ASCII file from LPC
# there is a header, header list read from file
# @param file the file name
def readlpcfills(file):
    name,ext = os.path.splitext(file)
    if ext == '.bz2':
        f = bz2.BZ2File(file, 'rb')
    elif ext == '.gz':
        f = gzip.open(file, 'rb')
    else:
        f = open(file, 'r')

    d = {}
    fields = []

    haveheader = False

    for line in f:
        if not haveheader:
            if line[:5] != '#fill' and line[:4] != '# nr':
                continue
            else:
                fields = line.strip('#').strip().split()
                if fields[2] == 'mm': fields[2] = 'm'
                for h in fields:
                    d[h] = array('d')
                haveheader = True
                continue
        elif line[0] != '#': # read dato into dict
            vals = line.strip().split()
            for i in range(len(vals)):
                d[fields[i]].append(float(vals[i]))

    for field in d:
       d[field] = np.array(d[field]) # convert to numpy array

    f.close()

    #print fields
    
    return d

def readtab(file, entries=0):
    return readcsv(file, entries)
    
## read PVSS CSV file
# lastifempty=True will use last known value if no value found for timestamp
def readcsv(file, entries=0, headerpos=0,isfloat=True,split=',', lastifempty=False,verbose=True):
    #spl='\t'
    # headerpos is the line position
    spl=split
    name,ext = os.path.splitext(file)
    if ext == '.bz2':
        f = bz2.BZ2File(file, 'rb')
    elif ext == '.gz':
        f = gzip.open(file, 'rb')
    else:
        f = open(file, 'r')

    #haveheader = False
    d = {}
    
    # head is composed of 2 lines
    #while not haveheader:
    l = 0
    while l <= headerpos:
        line = f.readline()
        if line[0] == spl or l < headerpos: # take second line for the dict keys
            continue

        # store data in a dictionary
        # read the header
        line = line.rstrip('\r\n')
        if line[0] == spl:
            line = 'date'+line

        fields = line.strip(spl).split(spl)
        if 'imestamp' in fields[0]:
            fields[0] = 'date'
        
        for field in fields:
            if field == 'date' or not isfloat:
                d[field] = []
            else:
                d[field] = array('d') # array of double
        
        l += 1

    #print d.keys(),len(d.keys())
    #print fields,len(fields)
    l = 0
    for line in f:
        vals = line.rstrip('\r\n').split(spl)
        
        for i in range(len(fields)):
            

            if i == 0 or not isfloat: # first column is a string (the date)
                d[fields[i]].append(vals[i])
            else:
                if len(vals[i]) == 0: # insert zero if no value
                    if lastifempty and len(d[fields[i]])>0:
                        d[fields[i]].append(d[fields[i]][-1])
                    else:
                        d[fields[i]].append(0)
                else:
                    d[fields[i]].append(float(vals[i]))

            #print fields[i], d[fields[i]][-1]
                    
        l += 1
        if l % 200000 == 0: print "read",l,"lines"
        if entries != 0 and l == entries: break

    for field in d:
       d[field] = np.array(d[field]) # convert to numpy array

    f.close()

    if verbose:
        print l, " entries"
        print fields
    
    return d

def readpvss(file):
    f = open(file, 'r')

    #haveheader = False
    d = {}
    
    # no header
    # first 6 column: 2011 11 15 19 57 55 

    d['gpsTime'] = array('d')
    d['val'] = array('d')
    

    #print d.keys(),len(d.keys())
    #print fields,len(fields)
    l = 0
    for line in f:
        vals = line.rstrip('\r\n').split()

        timeTuple = (int(vals[0]),int(vals[1]),int(vals[2]),int(vals[3]),int(vals[4]),int(vals[5]))
        tgps = timeTupleGps(timeTuple,toffset)

        d['gpsTime'].append(tgps)
        d['val'].append(float(vals[6]))
                            
        l += 1
        if l % 20000 == 0: print "read",l,"lines"

    for field in d:
       d[field] = np.array(d[field]) # convert to numpy array

    f.close()

    print l, " entries"
    
    return d

## read simple CSV file without header, specify fields
def readcsvsimple(file,fields):
    spl=','
    name,ext = os.path.splitext(file)
    if ext == '.bz2':
        f = bz2.BZ2File(file, 'rbU')
    elif ext == '.gz':
        f = gzip.open(file, 'rb')
    else:
        f = open(file, 'r')

    d = {}
    for field in fields:
        d[field] = array('d') # array of double

    for line in f:
        vals = line.rstrip('\r\n').split(spl)
        
        for i in range(len(fields)):
            d[fields[i]].append(float(vals[i]))

    for field in d:
       d[field] = np.array(d[field]) # convert to numpy array

    f.close()

    print fields
    
    return d

## read DCCT MD data (csv of ADC bins = raw data)
def readMdData(file, entries=0, split=','):
    #spl='\t'
    spl=split
    name,ext = os.path.splitext(file)
    if ext == '.bz2':
        f = bz2.BZ2File(file, 'rb')
    elif ext == '.gz':
        f = gzip.open(file, 'rb')
    else:
        f = open(file, 'r')

    #haveheader = False
    d = {}
    
    # head is composed of 3 lines
    #while not haveheader:
    for l in range(3):
        line = f.readline()
        if l != 2:
            continue

        # store data in a dictionary
        # use header info as dict key
        # this is the last line of the header
        #line = line.rstrip('\r\n')

        fields = line.replace(',','').strip('# ').split(' ')[0:5]
        
        for field in fields:
            d[field] = array('d') # array of double
        
        break

    #print d.keys(),len(d.keys())
    #print fields,len(fields)
    l = 0
    for line in f:
        vals = line.rstrip('\r\n').split(spl)
        
        for i in range(len(fields)):
            d[fields[i]].append(float(vals[i]))
                    
        l += 1
        if l % 200000 == 0: print "read",l,"lines"
        if entries != 0 and l == entries: break

    for field in d:
       d[field] = np.array(d[field]) # convert to numpy array

    f.close()

    #print l, " entries, fields:",fields
    
    return d

def readcsvxls(file, entries=0, skipl=0, split=','):
    #spl='\t'
    spl=split
    name,ext = os.path.splitext(file)
    if ext == '.bz2':
        f = bz2.BZ2File(file, 'rb')
    elif ext == '.gz':
        f = gzip.open(file, 'rb')
    else:
        f = open(file, 'rU')

    #haveheader = False
    d = {}
    
    # head is composed of 2 lines
    #while not haveheader:
    l=-1
    while l<skipl:
        line = f.readline() # read header as keys
        l+=1
        print l,line

    # store data in a dictionary
    # read the header
    line = line.rstrip('\r\n')
    if line[0] == spl:
        line = 'date'+line

    fields = line.strip(spl).split(spl)
    
    for field in fields:
        if field == 'date':
            d[field] = []
        else:
            d[field] = array('d') # array of double

    #print d.keys(),len(d.keys())
    #print fields,len(fields)
    for line in f:
        vals = line.rstrip('\r\n').split(spl)
        print vals
        for i in range(len(vals)):

            if len(vals[i]) == 0: # insert zero if no value
                d[fields[i]].append(0)
            else:
                d[fields[i]].append(float(vals[i]))

    for field in d:
       d[field] = np.array(d[field]) # convert to numpy array

    f.close()

    print fields
    
    return d

def filterOutsiders(a):
    idx = np.where(a!=0)
    avg = np.average(a[idx])
    for i in range(len(a)):
        if a[i] > 15*avg:
            a[i] = 0
    return a

def normalize(a):
    maxval = max(a)
    
    nstr,dec,bool,decades = flist(maxval,1)
    if maxval/10**decades > 1.2:
        decades += 1
    print maxval,nstr,decades
    for i in range(len(a)):
        a[i] = a[i] / 10**decades

    return a,decades
    
    
if __name__ == "__main__":
    import sys
    cut = None
    if len(sys.argv) > 2: cut = sys.argv[2]
    readtxt(sys.argv[1], cut)

def idxAll(d):
    idx = range(len(d[d.keys()[0]]))
    return idx

def idxBxType(d,bxtype,idx=None):
    if 'bxtype' in d.keys():
        bxtkey = 'bxtype'
    if 'ODIN_BXType' in d.keys():
        bxtkey = 'ODIN_BXType'
    
    if idx == None:
        idx = range(len(d[d.keys()[0]]))
    newidx = []
    for i in idx:
        if d[bxtkey][i]==bxtype:
            newidx.append(i)
    return newidx

# routBitBeamGas = 1 -> beam gas
# routBitBeamGas = 0 -> no beam gas
def idxBitBeamGas(d,bg=1,idx=None):
    if idx == None:
        idx = range(len(d['run']))
    if 'routBitBeamGas' not in d.keys():
        print "no routBitBeamGas bit in data"
        return idx
    newidx = []
    for i in idx:
        if d['routBitBeamGas'][i] == bg:
            newidx.append(i)

    return newidx

# remove double events WARNING only useful is some conditions (e.g. MC analysis)
# keep only the first
def idxUniqEvents(d,idx=None):
    if idx == None:
        idx = range(len(d['run']))
    newidx = []
    evts = []
    for i in idx:
        if d['evt'][i] not in evts:
            newidx.append(i)
            evts.append(d['evt'][i])
    return newidx

# separate in beam 1 and beam 2 according to fwd/bwd ratio
# Beam 1 PU can be 0   trBkw<<trFwd
# Beam 2 SPD can be 0  trFwd<<trBkw
def idxBeamSeparate(d,idx=None):
    if idx == None:
        idx = range(len(d['run']))
    newidx1 = [] # beam 1
    newidx2 = [] # beam 2
    for i in idx:
        trBkw = d['trNumBackw'][i]
        trFwd = d['trNumForw'][i]
        if trBkw == 0: newidx1.append(i)
        elif trFwd == 0: newidx2.append(i)
        else:
            if trBkw/float(trFwd) < 0.1: newidx1.append(i)
            elif trFwd/float(trBkw) < 0.1: newidx2.append(i)
            
    return newidx1, newidx2

# separate in beam 1 and beam 2 according to fwd/bwd ratio
def idxBeamBG(d,beam,idx=None):
    if beam == 3:
        return idx
    
    if idx == None:
        idx = range(len(d['run']))
    newidx1 = [] # beam 1
    newidx2 = [] # beam 2
    for i in idx:
        trBkw = d['trNumBackw'][i]
        trFwd = d['trNumForw'][i]
        if trBkw == 0: newidx1.append(i)
        elif trFwd == 0: newidx2.append(i)
        else:
            if trBkw/float(trFwd) < 0.1: newidx1.append(i)
            elif trFwd/float(trBkw) < 0.1: newidx2.append(i)

    if beam == 1:
        return newidx1
    elif beam == 2:
        return newidx2

def idxZrange(d,r=[],idx=None,MC=False):
    if not MC: pvz = 'pvz'
    else: pvz = 'mcpvz'
    
    if idx == None:
        idx = range(len(d['run']))

    if len(r) < 2:
        return idx

    begin = r[0]
    end = r[1]
    
    newidx = []
    for i in idx:
        if (d[pvz][i]>begin and d[pvz][i]<end):
            newidx.append(i)
    return newidx

# maximal error allowed on x,y,z
def idxZmaxError(d,x,y,z,idx=None):
    if idx == None:
        idx = range(len(d['run']))
    newidx = []
    for i in idx:
        if (d['pvxe'][i]<=x and d['pvye'][i]<=y and d['pvze'][i]<=z):
            newidx.append(i)
    return newidx

## return bad vertices
# MC - reco > 10 mm
def idxBadVetricesMC(d,idx=None):
    if 'err' not in d.keys():
        print "not a MC data set or no err key"
        return idx
    if idx == None:
        idx = range(len(d['run']))
    newidx = []
    maxr = 0.1
    for i in idx:
        r = np.sqrt((d['pvy'][i]-d['mcpvy'][i])**2 + (d['pvx'][i]-d['mcpvx'][i])**2)
        if r > maxr:
            newidx.append(i)
        elif abs(d['pvz'][i]-d['mcpvz'][i]) > 10*maxr:
            newidx.append(i)
    return newidx

## separate good and bad vertices 
def idxGoodBadVetricesMC(d,idx=None):
    if 'err' not in d.keys():
        print "not a MC data set or no err key"
        return idx
    if idx == None:
        idx = range(len(d['run']))
    idxg = []
    idxb = []

    dxr = 0.12
    dzr = 2
    for i in idx:
        good = True
        r = np.sqrt((d['pvy'][i]-d['mcpvy'][i])**2 + (d['pvx'][i]-d['mcpvx'][i])**2)
        if r > dxr + abs(d['pvz'][i])/1000.:
            good = False
        elif abs(d['pvz'][i]-d['mcpvz'][i]) > dzr+abs(d['pvz'][i])/100.:
            good = False
        if good:
            idxg.append(i)
        else:
            idxb.append(i)
    return idxg, idxb

# gpsend is either end time or duration in sec
def idxTimeRange(d,trange=[],idx=None):
    
    if idx == None:
        idx = range(len(d['run']))

    if len(trange) < 2:
        return idx
    
    gpsbegin = trange[0]
    gpsend = trange[1]
    if gpsend < gpsbegin: gpsend = gpsbegin + gpsend
    
    newidx = []
    for i in idx:
        if (d['gpsTime'][i]>gpsbegin and d['gpsTime'][i]<gpsend):
            newidx.append(i)
    return newidx

def idxMaxRadius(d,rmax,idx=None):
    if idx == None:
        idx = range(len(d['run']))
    newidx = []
    for i in idx:
        if (np.sqrt(d['pvx'][i]**2 + d['pvy'][i]**2) < rmax):
            newidx.append(i)

    return newidx

def idxRuns(d,runs=[],idx=None):
    if idx == None:
        idx = range(len(d['run']))
    newidx = []
    if len(runs) == 0:
        return idx
    for i in idx:
        if (d['run'][i] in runs):
            newidx.append(i)

    return newidx

def idxRunsExclude(d,runs=[],idx=None):
    if idx == None:
        idx = range(len(d['run']))
    newidx = []
    if len(runs) == 0:
        return idx
    for i in idx:
        if (d['run'][i] not in runs):
            newidx.append(i)

    return newidx

# select bunches
def idxBunches(d,bunches=[],idx=None):
    if idx == None:
        idx = range(len(d['bunchid']))
    newidx = []
    if len(bunches) == 0:
        return idx
    for i in idx:
        if (d['bunchid'][i] in bunches):
            newidx.append(i)

    return newidx

# select events by event ID
def idxEvents(d,events=[],idx=None):
    if idx == None:
        idx = range(len(d['evt']))
    newidx = []
    if len(bunches) == 0:
        return idx
    for i in idx:
        if (d['evt'][i] in bunches):
            newidx.append(i)

    return newidx

# minimum number of reconstructed vertices (typically 0 or 1)
def idxMinVertices(d,v=1,idx=None):
    if 'nPVsInEvent' not in d:
        return idx
    if idx == None:
        idx = range(len(d['run']))
    newidx = []
    for i in idx:
        if (d['nPVsInEvent'][i] >= v):
            newidx.append(i)
    return newidx

# minimum tracks in reconstructed vertex (for any event/vertex)
def idxMinTracks(d,mintr=4,idx=None):
    if idx == None:
        idx = range(len(d['run']))
    newidx = []
    for i in idx:
        if (d['trNum'][i] >= mintr):
            newidx.append(i)

    return newidx

# minimum tracks in reconstructed vertex for luminous region only
def idxMinTracksPP(d,mintr=10,lum=(-250,250),idx=None):
    if idx == None:
        idx = range(len(d['run']))
    newidx = []
    for i in idx:
        if d['bxtype'][i] != 3:
            newidx.append(i)
        elif d['pvz'][i] < lum[0] or d['pvz'][i] > lum[1]:
            newidx.append(i)
        elif (d['trNum'][i] >= mintr):
            newidx.append(i)

    return newidx

# minimum of 3d velo (incl. long) tracks (all of them)
# NOT per vertex
def idxMin3Dtracks(d,tr=0,idx=None):
    if 'nTracks3D' not in d:
        return idx
    if idx == None:
        idx = range(len(d['run']))
    newidx = []
    for i in idx:
        if (d['nTracks3D'][i] >= tr):
            newidx.append(i)

    return newidx

def idxMax3Dtracks(d,tr=100,idx=None):
    if 'nTracks3D' not in d:
        return idx
    if idx == None:
        idx = range(len(d['run']))
    newidx = []
    for i in idx:
        if (d['nTracks3D'][i] < tr):
            newidx.append(i)

    return newidx

# min/maxof 3d velo tracks (all of them)
def idxMinMax3Dtracks(d,trmin=0,trmax=100,idx=None):
    if 'nTracks3D' not in d:
        return idx
    if idx == None:
        idx = range(len(d['run']))
    idxmin = idxMin3Dtracks(d,trmin,idx)
    return idxMax3Dtracks(d,trmax,idxmin)

def idxResolRange(d,begin,end,beam=1,axis='X',idx=None):
    if idx == None:
        idx = range(len(d['run']))
    newidx = []
    r = resolution(d['trNum'],d['pvz'],beam=beam,axis=axis)
    for i in idx:
        if r[i]>=begin and r[i]<end:
            newidx.append(i)

    return newidx

# wrapper to select a beam data (e.g. b1, bb, etc.)
def idxBeam(d,bxtype=3,zmax=1000,rmax=1,idx=None,runs=[],mintr=3):
    if idx == None:
        idx = range(len(d['run']))
    idxrun = idxRuns(d,runs=runs,idx=idx)
    idxradius = idxMaxRadius(d,rmax=rmax,idx=idxrun)
    idxtracks = idxMinTracks(d,mintr=mintr,idx=idxradius)
    newidx = []
    for i in idxtracks:
        if d['bxtype'][i]==bxtype and abs(d['pvz'][i])<zmax:
            newidx.append(i)
    
    return newidx

# select luminous region only bb
def idxLum(d,zdist=250,rmax=1,idx=None,runs=[],mintr=10):
    if idx == None:
        idx = range(len(d['run']))
    idxrun = idxRuns(d,runs=runs,idx=idx)
    idxradius = idxMaxRadius(d,rmax=rmax,idx=idxrun)
    idxtracks = idxMinTracks(d,mintr=mintr,idx=idxradius)
    idxzrange = idxZrange(d,[-zdist,zdist],idx=idxtracks)
    newidx = []
    for i in idxzrange:
        if d['bxtype'][i]==3:
            newidx.append(i)
    
    return newidx


# select events from a beam
# take BXtype = beam
# for pp exclude luminous region
def idxSelectBeam(d,beam=1,idx=None):
    if idx == None:
        idx = range(len(d['run']))

    newidx = []
    for i in idx:
        if d['bxtype'][i]==beam:
            newidx.append(i)
        else:
            if beam==1 and d['pvz'][i] < -300:
                newidx.append(i)
            if beam==2 and d['pvz'][i] > 300:
                newidx.append(i)
    
    return newidx

# keep only unique events
# double events are expected to be continuous
def idxUniqEvents(d,idx=None):
    print "filter unique vertices out of total:",len(idx)
    if idx == None:
        idx = range(len(d['evt']))
    idxu = [idx[:1]]
    for i in range(len(idx)):
        if i>0 and d['evt'][idx[i]] != d['evt'][idx[i-1]]:
            idxu.append(idx[i])

    return idxu

# keep only unique events
# double events are expected to be continuous
def idxFilterBeamGas(d,beam=0,idx=None):
    if idx == None:
        idx = range(len(d['evt']))
    idxgood = []
    idxbad = []
    maxd = 3
    stat=[0,0,0]
    for i in idx:
        b = beam
        # only use reconstructed vertices
        if (d['nPVsInEvent'][i] == 0):
            continue
        dist = np.sqrt(d['pvy'][i]**2 + d['pvx'][i]**2)
        if dist > maxd:
            idxbad.append(i)
            stat[0]+=1
            continue

        ntr  = d['trNum'][i]
        nfwd = d['trNumForw'][i]
        nbkw = d['trNumBackw'][i]
        # detect beam if not known
        bxt = int(d['bxtype'][i])

        if b == 0:
            if bxt != 1 or bxt != 2:
                # use fwd/bkw tracks to find out which beam it is
                if nfwd > nbkw:
                    b = 1
                else:
                    b = 2
            else:
                b = bxt
        
        # use fwd/bwd ratio
        ratio = (nfwd-nbkw)/float(ntr)
        if b == 1:
            #if d['pvz'][i] > -300:
            #    if ratio > 0.95:
            #        idxgood.append(i)
            #if d['pvz'][i] < -300: # tracks ratio has no sence behind the PU
            #    idxgood.append(i)
            
            if ratio > 0.98:
                idxgood.append(i)
            else:
                idxbad.append(i)
        if b == 2:
            if d['pvz'][i] > 990: # far away for sure BG
                idxgood.append(i)
            elif ratio < -0.98:  # closer check assymetry
                idxgood.append(i)
            else:
                idxbad.append(i)
    #print stat,len(idx),len(idxgood),len(idxbad)
    return idxgood,idxbad


##########################
### misc tools
##########################

def float_to_decimal(f):
    # http://docs.python.org/library/decimal.html#decimal-faq
    #"Convert a floating point number to a Decimal with no loss of information"
    n, d = f.as_integer_ratio()
    numerator, denominator = decimal.Decimal(n), decimal.Decimal(d)
    ctx = decimal.Context(prec=60)
    result = ctx.divide(numerator, denominator)
    while ctx.flags[decimal.Inexact]:
        ctx.flags[decimal.Inexact] = False
        ctx.prec *= 2
        result = ctx.divide(numerator, denominator)
    return result 

def flist(number, sigfig):
    # http://stackoverflow.com/questions/2663612/nicely-representing-a-floating-point-number-in-python/2663623#2663623
    if sigfig <= 0: sigfig = 1
    try:
        d=decimal.Decimal(number)
    except TypeError:
        d=float_to_decimal(float(number))
    sign,digits,exponent=d.as_tuple()
    if len(digits) < sigfig:
        digits = list(digits)
        digits.extend([0] * (sigfig - len(digits)))    
    shift=d.adjusted()
    result=int(''.join(map(str,digits[:sigfig])))
    # Round the result
    if len(digits)>sigfig and digits[sigfig]>=5: result+=1
    result=list(str(result))
    # Rounding can change the length of result
    # If so, adjust shift
    shift+=len(result)-sigfig
    # reset len of result to sigfig
    result=result[:sigfig]
    #print "len(res) ",len(result)," shift ",shift
    #print "result ",result ," sigfig ",sigfig," shift ",shift," digits ",digits
    sround=0
    dec = False
    
    if shift >= sigfig-1:
        # Tack more zeros on the end
        result+=['0']*(shift-sigfig+1)
        sround = len(result)
    elif 0<=shift:
        # Place the decimal point in between digits
        result.insert(shift+1,'.')
        sround = len(result)-1
        #dec = True
    else:
        # Tack zeros on the front
        assert(shift<0)
        result=['0.']+['0']*(-shift-1)+result
        sround = len(result)-1
        dec = True
    if sign:
        result.insert(0,'-')

    return ''.join(result),sround,dec,shift

def f(number, sigfig):
    return flist(number,sigfig)[0]

def fe(number,error, sigfig):
    err, srounde, dece, shifte = flist(error,sigfig)
    n, sroundn, decn, shiftn = flist(number,sigfig)
    
    if dece and decn:
        #n, sroundn, decn, shift = flist(number,srounde)
        #print number,srounde,shiftn,srounde+shiftn+1
        n, sroundn, decn, shift = flist(number,srounde+shiftn+1)
        
    elif dece and not decn:
        n, sroundn, decn, shift = flist(number,sroundn+srounde)
    elif not dece and not decn:
        n, sroundn, decn, shift = flist(number,sroundn-srounde+1)
    
    #print number,srounde,shift
    return n,err

# format error into string
# return a string val +- err
def fes(number,error, sigfig):
    n,e = fe(number,error,sigfig)
    return n+"$\pm$"+e

###########
# create data and bin to plot a histogram outline with pyplab.plot
# adapted from
# http://www.scipy.org/Cookbook/Matplotlib/UnfilledHistograms?action=AttachFile&do=get&target=histNofill.py

# takes numpy histogram and its bins
def histOutline(histIn, binsIn):
    """
    Make a histogram that can be plotted with plot() so that
    the histogram just has the outline rather than bars as it
    usually does.

    Example Usage:
    binsIn = numpy.arange(0, 1, 0.1)
    angle = pylab.rand(50)

    (data, bins) = histOutline(angle, binsIn)
    plot(bins, data, 'k-', linewidth=2)

    """

    stepSize = binsIn[1] - binsIn[0]

    bins = np.zeros(len(binsIn)*2 + 2, dtype=np.float)
    data = np.zeros(len(binsIn)*2 + 2, dtype=np.float)    
    for bb in range(len(binsIn)):
        bins[2*bb + 1] = binsIn[bb]
        bins[2*bb + 2] = binsIn[bb] + stepSize
        if bb < len(histIn):
            data[2*bb + 1] = histIn[bb]
            data[2*bb + 2] = histIn[bb]

    bins[0] = bins[1]
    bins[-1] = bins[-2]
    data[0] = 0
    data[-1] = 0
    
    return data, bins
    #return bins, data


# http://www.peterbe.com/plog/uniqafiers-benchmark
def uniqify(seq, idfun=None): # Alex Martelli ******* order preserving
    if idfun is None:
        def idfun(x): return x
    seen = {}
    result = []
    for item in seq:
        marker = idfun(item)
        # in old Python versions:
        # if seen.has_key(marker)
        # but in new ones:
        if marker not in seen:
            seen[marker] = 1
            result.append(item)
            
    return result

mkers = [ '+' , '*' , ',' , '.',
    '1' , '2' , '3' , '4',
    '<' , '>' , 'D' , 'H',
    '^' , '_' , 'd' , 'h',
    'o' , 'p' , 's' , 'v',
    'x' , '|']

toffset = 1259625600

# gpstime is time givent by the event
# returns time tuple (y,m,d,h,m,s)
def gpsTimeTuple(gpstime):
# odin gpstime in microseconds, gps time is 13sec ahead of UTC
# information from http://www.csgnetwork.com/timegpsdispcalc.html says 15sec ahead
# Plamen tuple has offset and is in seconds
 gpss = gpstime+toffset-15
 timeTuple = time.localtime(gpss)
# (tm_year, tm_mon, tm_day, tm_hour, tm_min, tm_sec, tm_wday, tm_yday, tm_isdst)
 sec   = "%02d" % (timeTuple[5])
 minute = "%02d" % (timeTuple[4])
 txt = str(timeTuple[2])+'.'+str(timeTuple[1])+'. '+str(timeTuple[0])+'  '+str(timeTuple[3])+':'+minute+':'+sec
# retun YYYY MM DD time
 #return str(timeTuple[0]),str(timeTuple[1]),str(timeTuple[2]),str(timeTuple[3])+':'+minute+':'+sec
 #return txt
 return timeTuple[0],timeTuple[1],timeTuple[2],timeTuple[3],timeTuple[4],timeTuple[5]

def timeTupleGps(timeTuple, offset=0):
    #print timeTuple
    t = datetime(timeTuple[0],timeTuple[1],timeTuple[2],timeTuple[3],timeTuple[4],timeTuple[5])
    tunix = mktime(t.timetuple())
    tgps = tunix-toffset+15+offset
    return tgps

def timeDateGps(dateString, offset=0):
    # now = '2011-12-19 12:50:00.0'
    # timeDateGps(now)
    # 64669815.0
    #
    # in format '2010/10/15 13:18:28.434'
    # or format '2010-10-15 07:22:42.000'

    if dateString[4] == '/':
        t = datetime.strptime(dateString, '%Y/%m/%d %H:%M:%S.%f')
    elif dateString[4] == '-':
        t = datetime.strptime(dateString, '%Y-%m-%d %H:%M:%S.%f')
#    if UTC:
#        tunix = calendar.timegm(t.timetuple())
#    else:
    tunix = mktime(t.timetuple())
    tgps = tunix-toffset+15+offset
    return tgps

def timeDateEpoch(dateString, utc=False):
    # in format '2010/10/15 13:18:28.434'
    # or format '2010-10-15 07:22:42.000'

    if dateString[4] == '/':
        t = time.strptime(dateString, '%Y/%m/%d %H:%M:%S.%f')
    elif dateString[4] == '-':
        t = time.strptime(dateString, '%Y-%m-%d %H:%M:%S.%f')

    if utc:
        tunix = calendar.timegm(t)
    else:
        tunix = mktime(t)
    

    return tunix

# convert gps time to numDate
def GpsNum(gpstime,offset=0):
    gpss = gpstime+toffset+offset-15 # aaargh
    gpss = gpstime+offset-15
    timeTuple = time.localtime(gpss)
    t = datetime(timeTuple[0],timeTuple[1],timeTuple[2],timeTuple[3],timeTuple[4],timeTuple[5])
    return mpl.dates.date2num(t)
    
def GpsNums(gpstime,offset=0):
    numdates = []
    for j in range(len(gpstime)):
        numdates.append(GpsNum(gpstime[j],offset))

    return np.array(numdates)

# matplotlib numerical date
# http://matplotlib.sourceforge.net/api/dates_api.html?highlight=date#matplotlib.dates
def timeDateNum(dateString, offset=0):
    # in format '2010/10/15 13:18:28.434'
    # or format '2010-10-15 07:22:42.000'

    #ndates = epoch2num(D.d['time'])

    if dateString[4] == '/':
        t = datetime.strptime(dateString, '%Y/%m/%d %H:%M:%S.%f')
    elif dateString[4] == '-':
        t = datetime.strptime(dateString, '%Y-%m-%d %H:%M:%S.%f')
    elif dateString[2] == '/':
        t = datetime.strptime(dateString, '%d/%m/%Y %H:%M:%S')
    elif len(dateString) == 12: # i.e. format 201108300910
        t = datetime.strptime(dateString, '%Y%m%d%H%M')
    else:
        print 'wrong date format', dateString,type(dateString)
        
        return 0
    
    return mpl.dates.date2num(t)

def timeDateNums(dateString):
    # dateString iterable (list or array)
    #print len(dateString)
    numdates = []
    for j in range(len(dateString)):
        numdates.append(timeDateNum(dateString[j]))
    
    return np.array(numdates)

def toBunchSlot(rfbucket):
    return int(rfbucket+9 /10)

# return charges for a given current (LHC only)
#### convert to charges per bin
# 1 A = 1C/s = I
# 1 e = 1.602e-19 C
# f = 11245 1/s
# C = A * s = 1/1.6e-19 e
# Ctot = I/f * 1/e 

def currentToCharges(I):
    flhc = 11245.5 # Hz
    el = 1.602176487E-19 # C
    return (I/flhc) * 1/el

def chargesToCurrent(c):
    flhc = 11245.5 # Hz
    el = 1.602176487E-19 # C
    return flhc * el * c
    

    
# use interpolated function outside boundaries
# from http://stackoverflow.com/questions/2745329/how-to-make-scipy-interpolate-give-a-an-extrapolated-result-beyond-the-input-rang
"""
x = arange(0,10)
y = exp(-x/3.0)
f_i = interp1d(x, y)
f_x = extrap1d(f_i)

print f_x([9,10])

[ 0.04978707  0.03009069]

"""
def extrap1d(interpolator):
    xs = interpolator.x
    ys = interpolator.y

    def pointwise(x):
        if x < xs[0]:
            return ys[0]+(x-xs[0])*(ys[1]-ys[0])/(xs[1]-xs[0])
        elif x > xs[-1]:
            return ys[-1]+(x-xs[-1])*(ys[-1]-ys[-2])/(xs[-1]-xs[-2])
        else:
            return interpolator(x)

    def ufunclike(xs):
        return array(map(pointwise, array(xs)))

    return ufunclike


def extrap1dsame(interpolator):
    xs = interpolator.x
    ys = interpolator.y

    def pointwise(x):
        #print x,xs[0],interpolator(x)
        if x < xs[0]:
            return ys[0]
        elif x > xs[-1]:
            return ys[-1]
        else:
            return interpolator(x)

    def ufunclike(xs):
        #return sp.array(map(pointwise, sp.array(xs)))
        return pointwise(xs)

    return ufunclike
