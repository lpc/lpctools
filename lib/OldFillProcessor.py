#!/usr/bin/python
import sys
import os
import json
import re
import tarfile
import zipfile
import pickle
from lpcutils import Logger, getsrcdst
from readconfig import readconfig

class FillProcessor:
    
    def __init__( self, config ):
        self.config = config
        self.year = config['year']
        self.workBaseDir = config['workBaseDir']
        self.webBaseDir = config['webBaseDir']
        #self.dataFormat = config['dataFormat']
        self.logger = Logger( config['logfile'] )

        self.fillCache = {}
        self.cacheDir = os.path.join( self.workBaseDir, self.year, "fillCache" )
        self.annotDir = os.path.join( self.webBaseDir, self.year )
        if not os.path.isdir( self.cacheDir ):
            os.mkdir( self.cacheDir )


    def analyseLumiReg( self, fill, exp ):
        fills = str(fill)
        self.checkLumiregData( fills, [exp] )
        fill = self.fillCache[ fills ][exp]['fill_lumireg']
        pentry = fill['time'][0]
        tsum = 0
        ie = 0
        mini = 9e20
        maxi = 0
        #for entry in fill[1:]:
        for ix in range(1,len(fill['time'])):
            time = fill['time'][ix]
            diff = time-pentry
            pentry = time
            tsum += diff
#            print fills, diff
            if diff < mini:
                mini = diff
            if diff > maxi:
                maxi = diff
            ie += 1
        mean = tsum/ie
        print exp + ": mean time difference: {0:.2f} sec     from {1:.2f} to {2:.2f}".format( mean,mini,maxi )
        return { 'mean': mean, 'min':mini, 'max':maxi }
    


    # Require fill to be in the cache (in memory),
    #         both experiments need to have data in the cache,
    #         both experiments need to have lumireg data in the cache
    #         at least 2 entries must be in both lumireg datasets. 
    def checkLumiregData( self, fillno, exps ):
        fillno = str(fillno)
        if not fillno in self.fillCache :
            print "Fill "+fills+" not in cache"
            return False
        for exp in exps:
            if not exp in self.fillCache[fillno]:
                print exp + " data not there for fill " + fillno
                return False
            if not 'fill_lumireg' in self.fillCache[fillno][exp]:
                print "No lumireg data for fill " + fillno + " and experiment " + exp
                return False
            if len(self.fillCache[fillno][exp]['fill_lumireg']['time']) < 2:
                print  exp + " data for fill " + fillno + " has not enough entries (2 needed)"
                return False
        return True
        

    
        
    def createRatios( self, fillno, exp1, exp2 ):
        fillno = str(fillno)
        if not self.checkLumiregData( fillno, [exp1, exp2] ):
            return False

        o1 = { 'dat' : self.fillCache[fillno][exp1]['fill_lumireg'],
               'ix' : 0
           }
        o2 = { 'dat' : self.fillCache[fillno][exp2]['fill_lumireg'],
               'ix' : 0
           }

        # find the starting position with possibly small start time difference
        
        timediff_start = o1['dat']['time'][0] - o2['dat']['time'][0]
        stop = False
        
        while not stop:
            if timediff_start > 0:
                #print timediff_start
                if o2['ix']+1 == len(o2['dat']['time']):
                    return False
                tdn = o1['dat']['time'][o1['ix']] - o2['dat']['time'][ o2['ix']+1 ]
                if abs(tdn) < abs(timediff_start):
                    o2['ix'] += 1
                    timediff_start = tdn
                else:
                    stop = True
            else:
                #print timediff_start
                if o1['ix']+1 == len(o1['dat']['time']):
                    return False
                tdn = o1['dat']['time'][o1['ix']+1] -  o2['dat']['time'][ o2['ix'] ]
                if abs(tdn) < abs(timediff_start):
                    o1['ix'] += 1
                    timediff_start = tdn
                else:
                    stop = True
                
        #print "timediff start ", timediff_start

        stop = False;
        ratios = { 'time' : [],
                   'ratio' : [],
                   'dat1' : [],
                   'dat2' : [],
                   'delta' : [],
                   'exp1' : exp1,
                   'exp2' : exp2,
                   'startdiff' : timediff_start}
        while not stop:
            # get the next step: find our who does the bigger step
            o1['ix'] += 1
            o2['ix'] += 1

            if ( o1['ix'] == len(o1['dat']['time']) or o2['ix'] == len(o2['dat']['time']) ):
                stop = True
                continue


            o1['next'] = o1['ix']
            o2['next'] = o2['ix']

            if o1['dat']['time'][o1['next']] > o2['dat']['time'][o2['next']]:
                big = o1
                small = o2
            else:
                big = o2
                small = o1

            deltabig = big['dat']['time'][big['next']] - big['dat']['time'][big['ix'] - 1]

            # now advance the smaller one until it reaches but not surpasses the bigger.
            # during this form the mean
            mean = small['dat']['zsu'][small['next']]
            n = 1
            # !!! need to check i fthe index for small is not too high! (once in the loop it is ok
            # since we check this with the "-1"
            if (small['ix']+1) > (len(small['dat']['time']) - 1):
                stop = True
                continue

            while small['dat']['time'][small['ix']+1] < big['dat']['time'][big['next']]:
                 #advance
                 small['ix'] += 1
                 if small['ix'] == ( len( small['dat']['time'] ) - 1 ):
                     stop = True
                     break
                 small['next'] = small['ix']
                 mean += small['dat']['zsu'][small['next']]
                 n += 1

            mean = mean / n
            ratios['delta'].append( deltabig )
            ratios['time'].append(big['dat']['time'][big['next']])
            if big == o1:
                rat = o1['dat']['zsu'][o1['next']] / mean
                ratios['ratio'].append( rat )
                ratios['dat1'].append( o1['dat']['zsu'][o1['next']] )
                ratios['dat2'].append( mean )
            else:
                rat = mean / o2['dat']['zsu'][o2['next']]
                ratios['ratio'].append( rat )
                ratios['dat1'].append( mean )
                ratios['dat2'].append( o2['dat']['zsu'][o2['next']] )

        # now we have to store the ratios
        self.fillCache[fillno]['lureg_z_analysis'] = ratios

        #self.dumpRatios( fillno )
        return ratios

    def extractFile( self, fillno, srcdst, exp ):
        (lufi, lure, luver, luveranno, rever, reveranno ) = (None,None,None,None,None,None)
        versionfile = os.path.join( fillno, "version.txt")
        verannofile = os.path.join( fillno, "version_annotation.txt")
        elfile = os.path.join( fillno, fillno + "_lumi_" + exp + ".txt")
        erfile = os.path.join( fillno, fillno + "_lumireg_" + exp + ".txt")
        if 'lumi' in srcdst:
            lumi_tgz = os.path.join( srcdst['lumi'][1], fillno + ".tgz")
            lumi_zip = os.path.join( srcdst['lumi'][1], fillno + ".zip")
        else:
            lumi_tgz = os.path.join(srcdst['all'][1], fillno + ".tgz")
            lumi_zip = os.path.join(srcdst['all'][1], fillno + ".zip")
            
        if os.path.isfile( lumi_tgz ):
            tf = tarfile.open( lumi_tgz, "r" )
            try:
                lufi = tf.extractfile( elfile )
            except KeyError as ke:
                print "Could not find lumi file in fill " + fillno + " for " + exp, ke
            try:
                luver = tf.extractfile( versionfile )
                luveranno = tf.extractfile( verannofile )
            except KeyError as ke:
                print "Could not find version files in lumi tar for fill " + fillno + " for " + exp, ke

        elif os.path.isfile( lumi_zip ):
            zf = zipfile.ZipFile( lumi_zip )
            try:
                lufi = zf.open( elfile )
            except KeyError as ke:
                print "Could not find lumi file in fill " + fillno + " for " + exp, ke
            try:
                luver = zf.open( versionfile )
                luveranno = zf.open( verannofile )
            except KeyError as ke:
                print "Could not find version files in lumi zip for fill " + fillno + " for " + exp, ke


        if 'lumiregion' in srcdst:
            lumir_tgz = os.path.join( srcdst['lumiregion'][1], fillno + ".tgz")
            lumir_zip = os.path.join( srcdst['lumiregion'][1], fillno + ".zip")
        else:
            lumir_tgz = os.path.join(srcdst['all'][1], fillno + ".tgz")
            lumir_zip = os.path.join(srcdst['all'][1], fillno + ".zip")

        if os.path.isfile( lumir_tgz ):
            tf = tarfile.open( lumir_tgz, "r" )
            try:
                lure = tf.extractfile( erfile )
            except KeyError as ke:
                print "Could not find lumireg file in fill " + fillno + " for " + exp, ke
            try:
                rever = tf.extractfile( versionfile )
                reveranno = tf.extractfile( verannofile )
            except KeyError as ke:
                print "Could not find version files in lumiregion tar for fill  " + fillno + " for " + exp, ke

        elif os.path.isfile( lumir_zip ):
            zf = zipfile.ZipFile( lumir_zip )
            try:
                lure = zf.open( erfile )
            except KeyError as ke:
                print "Could not find lumireg file in fill " + fillno + " for " + exp + ": " + erfile
            try:
                rever = zf.open( versionfile )
                reveranno = zf.open( verannofile )
            except KeyError as ke:
                print "Could not find version files in lumiregion zip for fill " + fillno + " for " + exp, ke

        return (lufi, lure, luver, luveranno, rever, reveranno)

        
    def cacheFill( self, fillno, expList=None ):
        '''If experiment is given only files from that experiments 
        are processed'''

        fillno = str(fillno)

        if not expList:
            expList = ['ATLAS','CMS','LHCb','ALICE']

        for exp in expList:

            srcdst = getsrcdst( self.config, exp )
            (lufi,lure,luver,luveranno,rever,reveranno) = self.extractFile( fillno, srcdst, exp )

            if lufi == None:
                self.logger.log( "No Massi files in " + exp + " for fill " + fillno )                 
                continue
                    
                   
            # cache the file contents
            ludata = { 'time' : [],
                       'stab' : [],
                       'l'    : [],
                       'dl'   : []
            }
            luregdata = { 'time' : [],
                          'stab' : [],
                          'x'    : [],
                          'y'    : [],
                          'z'    : [],
                          'xsu'  : [],
                          'ysu'  : [],
                          'zsu'  : [] 
                      }
    
            if lufi:
                for line in lufi:
                    #ludata.append( map( float, line.split() ) )
                    fline = map( float, line.split() )
                    ludata['time'].append( fline[0] )
                    ludata['stab'].append( fline[1] )
                    ludata['l'].append( fline[2] )
                    ludata['dl'].append( fline[3] )
    
                if lure:
                    for line in lure:
                        fline = map( float,line.split() )
                        luregdata['time'].append( fline[0] )
                        luregdata['stab'].append( fline[1] )
                        luregdata['x'].append( fline[2] )
                        luregdata['y'].append( fline[4] )
                        luregdata['z'].append( fline[6] )
                        luregdata['xsu'].append( fline[8] )
                        luregdata['ysu'].append( fline[10] )
                        luregdata['zsu'].append( fline[12] )
    
            sf = self.fillCache
            if not (fillno in sf):
                sf[fillno] = {}
            if not exp in sf[fillno]:
                sf[fillno][exp] = {}
            if lufi:
                sf[fillno][exp]['fill_lumi'] = ludata
            if lure:
                sf[fillno][exp]['fill_lumireg'] = luregdata

            self.createRatios( fillno, 'CMS', 'ATLAS')

            vc = self.versionCache
            if luver and luveranno:
                version = luver.read().strip()
                print "exp : ", exp , "  version : ", version
                vc[exp][fillno] = version
                if fillno not in vc['fills']:
                    vc['fills'][fillno] = { 'ALICE' : '', 'ATLAS' : '', 'CMS' : '', 'LHCb' : ''}
                vc['fills'][fillno][exp] = version

                for line in luveranno:
                    line = line.strip()
                    mo = re.match( r"(\d+)\s+:\s*(.+)$",line )
                    
                    if mo:
                        (digit,desc) = mo.groups()
                        desc = mo.group(2)
                        vc[exp]['digit_annotation'][digit] = desc
                        continue
                    mo = re.match( r"(\d+)\s+(\d+)\s+:\s*(.+)$", line)
                    if mo:
                        (digit,value,desc) = mo.groups()
                        if not digit in vc[exp]['value_annotation']:
                            vc[exp]['value_annotation'][digit] = {}
                        vc[exp]['value_annotation'][digit][value] = desc
                        continue
                    mo = re.match( r"(_\d+)\s+:\s*(.+)$", line )
                    if mo:
                        (value,desc) = mo.groups()
                        if not value in vc[exp]['exception_annotation']:
                            vc[exp]['exception_annotation'][value] = {'description' : desc,
                                                                      'fills' : [ fillno ] }
                        else:
                            vc[exp]['exception_annotation'][value]['description'] = desc
                            vc[exp]['exception_annotation'][value]['fills'].append( fillno )
                        continue

                    self.logger.log("Illegal syntax in annotation file for " + exp + " in fill " + fillno)
                    continue

    def loadCache( self, fillarr = [] ):
        for fill in fillarr:
            fill = str(fill)
            filename = os.path.join(self.cacheDir, fill + ".json") 
            if os.path.isfile(filename):
                #print fill
                cfd = open( filename, "r" )
                fillcache = json.load( cfd )
                cfd.close()
                self.fillCache[ fill ] = fillcache
        filename = os.path.join( self.annotDir, "massi_annotations.json")
        if not os.path.isfile( filename ):
            self.versionCache = { 'ALICE' : { 'digit_annotation' : {},
                                            'value_annotation' : {},
                                            'exception_annotation' : {} },
                                  'ATLAS' : { 'digit_annotation' : {},
                                            'value_annotation' : {},
                                            'exception_annotation' : {} },
                                  'CMS'   : { 'digit_annotation' : {},
                                            'value_annotation' : {},
                                            'exception_annotation' : {} },
                                  'LHCb'  : { 'digit_annotation' : {},
                                            'value_annotation' : {},
                                            'exception_annotation' : {} },
                                  'fills' : {}
                                  }
        else:
            fd = open( filename, 'r' )
            self.versionCache = json.load(fd)

    def clearCache( self ):
        self.fillCache = {}
        self.versionCache = {}

    def getFill( self, fillno ):
        fillno = str(fillno)
        if not fillno in self.fillCache:
            self.loadCache( [fillno] )
            return self.fillCache[fillno]

    def dumpCache( self, fillarr = None ):
        if not fillarr:
            fillarr = sorted(self.fillCache.keys())

        for key in fillarr:
            key = str(key)
            if key not in self.fillCache:
                continue
            ndir = os.path.join( self.cacheDir, key + ".json" )
            #print "fill " + key + " to " + ndir
            cfd = open( ndir, "w" )
            json.dump( self.fillCache[key], cfd )
            cfd.close()        
            
        filename = os.path.join( self.annotDir, "massi_annotations.json")
        fd = open( filename, 'w' )
        json.dump( self.versionCache, fd )
        fd.close
        

    def infoCache( self ):
        ifill = 0
        for key in sorted(self.fillCache.keys()):
            exps = self.fillCache[key].keys()
            print key + " : " + repr(exps)
            ifill += 1

        print 
        print str(ifill) + " fills."

    def dumpRatios( self, fillno ):
        fillno = str(fillno)

        self.getFill( fillno )
        if not 'lureg_z_analysis' in self.fillCache[fillno]:
            print "No lumireg-z analysis in fillCache for this fill"
            return

        fillno = str(fillno)
        ratios = self.fillCache[fillno]['lureg_z_analysis']
        
        dint = 0
        for ix in range( 0, len(ratios['ratio'])):
            dint +=  (ratios['delta'][ix]/3600)
            print "Ratio : {0:.3f}    {3:5s}: {1:4.2f} {4:5s}: {2:4.2f}   delta {5:5.0f} sec   acc {6:2.2f} hours".format(ratios['ratio'][ix],ratios['dat1'][ix],ratios['dat2'][ix],ratios['exp1'],ratios['exp2'],ratios['delta'][ix], dint)
        print "Start time difference: {0:5.0f} sec".format( ratios['startdiff'] )

    def addLumiregionInfoToCache( self, fillno ):
        fillno = str(fillno)
        self.loadCache( [fillno] )
        self.createRatios( fillno, 'CMS', 'ATLAS' )
        self.dumpCache( [fillno] )

    def updateCache( self, fillno, expList = None ):
        self.loadCache( [fillno] )
        self.cacheFill( fillno )
        self.dumpCache( [fillno] )

################### test ################
if __name__ == '__main__':
    config = readconfig( "testconfig.json" )
    fp = FillProcessor( config )
    
    
    for fillno in range( 5073, 5576 ):
        print fillno
        fp.updateCache( fillno )
    
    #fp.analyseLumiReg( fillno, 'CMS' )
    #fp.analyseLumiReg( fillno, 'ATLAS' )
    #fp.dumpRatios( fillno )

    #fp.loadCache( range( 4850, 5576 ) )
    #print "loading json"
    
    
    #for fill in range( 5439, 5440 ):
    #fillno = 5005
    #fp.addLumiregionInfoToCache( fillno )
    #print "done"
    
    #fp.loadCache( [fillno] )
    #fp.analyseLumiReg( fill, 'ATLAS' )
    #fp.analyseLumiReg( fill, 'CMS' )
    #fp.createRatios( fill,  'CMS', 'ATLAS' )
    #fp.dumpCache([fill])
    #fp.loadCache( [fill] )
    #fp.dumpRatios( fillno )
    #fp.analyseLumiReg( fill, 'CMS' )
    
    
    #fp.infoCache()
    #print fp.fillCache['4851']['CMS']
    #fp.dumpCacheJson()
    
    #print fp.fillCache['5097']
    
    #for fill in range( 5500, 5600 ):
    #    print fill
    #    fp.cacheFill( fill )
    #fp.dumpCache()
    #    
    #fp.infoCache()
