import json
import sys
CONFIGFILE = "config.json"

def readconfig( configFile = CONFIGFILE ) :
    try:
        fconfig = open( configFile, 'r')
        config = json.load( fconfig )
        fconfig.close()
    except IOError as (errno, strerror):
        print "Problem when trying to read config file '" + configFile + "':"
        print "{1}  (errno {0})".format(errno, strerror)
        sys.exit(-1)
    except ValueError as (error):
        print "Problem when parsing config file '" + configFile + "':"
        print repr(error)
        sys.exit(-1)
       
    except :
        print "Unexpected error when parsing config file '" + configFile + "':"
        print sys.exc_info()[0]
        sys.exit(-1)
    
    return config
