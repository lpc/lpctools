function getDate( tabId, datcol, valcol, txtcol ) {
   var res = [];
   $('table#' + tabId + ' tr').each( function( ix, obj) {
       var datstr = $(obj).find(' td:nth-child(' + datcol + ')' ).html();
       var valstr = $(obj).find(' td:nth-child(' + valcol + ')' ).html();
       var txtstr = $(obj).find(' td:nth-child(' + txtcol + ')' ).html();
       var pat = /(\d{4}-\d{1,2}-\d{1,2}( \d{4}-\d{1,2}-\d{1,2})?)/;
       
       if ( datstr != undefined ) {
           var dat = datstr.match( pat );
           if ( dat ) {
               var entry = { date: dat[0], value:valstr, text:txtstr }
               res.push( entry );
           }    
       }
   });
    return res;
}


function grabFromTab( schedid, tabId, category, color ) {
    entries = getDate( tabId, 1,2,3 );
    $('div#'+tabId).remove();
    $('div#document').append( '<div id="'+tabId+'"></div>');
    for (ix in entries) {
        //debug( entries[ix].date );
        $('div#'+tabId).append( "<p>"+schedid+" ["+category+":"+color+"] "+entries[ix].date + " {" + entries[ix].value + "} " + entries[ix].text + "</p>\n"); 
    }
}

