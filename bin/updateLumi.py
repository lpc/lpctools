#!/usr/bin/python
import os
import sys
import shutil
import pprint
import readconfig
import subprocess
import re
import json
import shlex
import lpcutils
from lpcutils import Logger, Mailer

#####################################################################################
#
# The json config file contains items related to input and output of the scripts.
# The configuration file needs to be located in the current working directory of  
# the script being executed (i.e. the directory in which you launch the command)
# This set of script assumes the a fixed EXISTING directory structure. In detail
# the configuration items are described in the following:
#
# The scripts for the various tasks are in
#    {softwareBase}
# All scripts use the log file
#    {logfile}
# The original location of the Massi files stored by the experiments is in 
#    {srcDir["ATLAS"|"CMS"|"ALICE"|"LHCb]}
#
# The base working directory is 
#    {workBaseDir}
# The year of the data taking period to be analysed 
#    {year}
# Massi files are copied to 
#    {workBaseDir}/{year}/measurements/{experiment}
# The csv file containing the fills with Stable Beams is stored in
#    {workBaseDir}/{year}/measurements/beam_modes.csv
# Plots in PDF format are generated in 
#    {workBaseDir}/{year}/measurements/FIGURES
# Plots in png format are generated in 
#    {workBaseDir}/{year}/measurements/FIGURES/png
#
#####################################################################################





config     = readconfig.readconfig( "lumi_config.json" )
logger     = Logger( config["logfile"] )
year       = config["year"]
plotDir    = os.path.join( config['workBaseDir'],year, "measurements/FIGURES/png" )

############## define the commands we need to launch ###############

softwareDir   = os.path.join( config["softwareBase"], "bin" )

# copy the massi files
copycmd       = os.path.join( softwareDir, "copySummaryFiles.py" )
# update the filltable
getFTcmd      = os.path.join( softwareDir, "getFillTable.py -i 25" )
# generate the old lumi plots (from Collin Barschel)
plotSBcmd     = os.path.join( softwareDir, "plotSB.py" )
ppLumicmd     = os.path.join( softwareDir, "plotLumi.py" )
ionLumicmd    = os.path.join( softwareDir, "plotLumiIons.py" )
copyWebcmd    = os.path.join( softwareDir, "copyToWeb.py" )
# For the new luminosity plots on the lpc web site prepare the data:
lumiDatacmd   = os.path.join( softwareDir, "getLumiJson.py" )
# To update the lumi-cache files for the fill related plots
cachecmd      = os.path.join( softwareDir, "updateLumiCache.py" )
#############  see if there are massi file updates  ################

cpproc = subprocess.Popen( copycmd, stdout=subprocess.PIPE,
                           stderr=subprocess.PIPE )
(copyOutput, copyErr) = cpproc.communicate()

updates = {}

state = 0
searchex = re.compile( r'^\s+(ALICE|CMS|ATLAS|LHCb) (\d+)' )
searchfi = re.compile( r'^\s+-->\s(\d+)\.(tgz|zip)' )
cexp = 'none'
for line in copyOutput.splitlines():
    mo = re.match( searchex, line )
    if mo:
        cexp = mo.group(1)
        updates[cexp] = { 'number' : int(mo.group(2)),
                          'fills' : [] }
    else:
        mo = re.match( searchfi, line )
        if mo:
            fillno = mo.group(1)
            updates[cexp]['fills'].append( fillno )
            

if not updates:
    #print "No updates, nothing to do"
    sys.exit(0)

############# If there are updates continue  #################
logger.log( "updates: " +  str(updates) )
#print updates

############### Update the fill table #########################
updateFTproc = subprocess.Popen( shlex.split( str(getFTcmd) ), 
                                 stdout=subprocess.PIPE, 
                                 stderr=subprocess.PIPE )
(ftOutput, ftErr) = updateFTproc.communicate()
if ftErr:
    logger.log( ftErr )
logger.log( "Updated Fill Table" )

############### Redo the plots for Stable Beams ###############
plotSBproc = subprocess.Popen( plotSBcmd, stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE )
(sbOutput,sbErr) = plotSBproc.communicate()

if sbErr :
    logger.log( sbErr )

logger.log( "generated Stable Beams plots" )

#print sbErr
#print sbOutput
    
    

############## Redo the plots for pp lumi  ###############
ppLumiProc = subprocess.Popen( ppLumicmd, stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE )
(sbOutput,sbErr) = ppLumiProc.communicate()

if sbErr :
    logger.log( sbErr )

logger.log( "generated plots for protons" )

#print sbErr
#print sbOutput
    

############## Redo the plots for ion lumi  ###############
ionLumiProc = subprocess.Popen( ionLumicmd, stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE )
(sbOutput,sbErr) = ionLumiProc.communicate()

logger.log( "generated plots for ions" )

#print sbErr
#print sbOutput
    

############## Copying the fresh plots to the web  ###############

copyWebProc = subprocess.Popen( copyWebcmd, stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE )
(sbOutput,sbErr) = copyWebProc.communicate()

if sbErr :
    logger.log( sbErr )

logger.log( "Copied plots to web area" )

#print sbErr
#print sbOutput

############# re-generate the luminosity data json file ################

getLumiJsonProc = subprocess.Popen( lumiDatacmd, stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE )
(ldOutput,ldErr) = getLumiJsonProc.communicate()

if ldErr :
    logger.log( ldErr )

logger.log( "Re-generated the lumidata.json file" )

#print ldErr
#print ldOutput
    
############ update the lumi-cache json file ################

jsonupdates = json.dumps( updates )
cacheca = shlex.split( str(cachecmd) + " '" + str(jsonupdates) + "'" )

#print repr(cacheca)

updateLumiCacheProc = subprocess.Popen( cacheca, stdout=subprocess.PIPE,
                                        stderr=subprocess.PIPE )
(ldOutput,ldErr) = updateLumiCacheProc.communicate()

logger.log( ldOutput )

if ldErr :
    logger.log( ldErr )

logger.log( "Updated the lumi cache" )

#print ldErr
#print ldOutput
    
