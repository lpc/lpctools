// Global variables //
// Indication of a change of contents:
var changed = 0;
// inhibit the top bar togglers (neessary as a global???
var doNotToggle = 0;
// indicates what is shown in the main content div. According to this 
// variable, certain switches/behaviours should be enabled/disabled.
// Derived classes should use this to control their behaviour.
var contents = "Document";

function cleardebug( str ){
    timestr = (new Date()).toLocaleTimeString();
    $('#debug').html( timestr + " : " + str + '<br>');
}

function debug( str ){
    timestr = (new Date()).toLocaleTimeString();
    $('#debug').append(  timestr + " : " + str + '<br>');
}

function clearerror( str ){
    timestr = (new Date()).toLocaleTimeString();
    $('#error').html(  timestr + " : " + str + '<br>');
}

function error( str ){
    if ( str ) {
        timestr = (new Date()).toLocaleTimeString();
        $('#error').append(  timestr + " : " + str + '<br>');
    } else {
        $('#error').html("");
    }
}

function log( str ){
    if ( str ) {
        timestr = (new Date()).toLocaleTimeString();
        $('#log').append(  timestr + " : " + str + '<br>');
    } else {
        $('#log').html("");
    }
}



function setFrameUrl( event, url ) {
    event.stopPropagation();
    var tg = event.target;
    $(tg).closest("div.indico").find(".indico-iframe").attr( "src", url);
    return false;

}

function toggleControl() {
    if ( doNotToggle == 1 ) return;
    doNotToggle = 1;
    $('div#documentCrtl').slideToggle(400);
    setTimeout(function() { doNotToggle = 0; }, 800);
}

function showDocumentCrtl() {
    $('div#documentCrtl').show();
}

function hideDocumentCrtl() {
    $('div#documentCrtl').hide();
}

function getDocumentTree() {
    Doc.sync( Doc, "rescanDocTree", {}, 1 );
};

function Documents( name, options ) {
    this.name          = name;
    this.divid         = 'divid' in options? options.divid : name; 
    this.baseurl       = 'baseurl' in options? options.baseurl : "";
    this.syncInterval  = 3000;
    // this object is used to pass around information between event handlers and methods of the class
    this.docTreeVer     = 0;
    this.url            = window.location.href
    this.actionurl      = 'url' in options? options.url : 'docaction';
    this.currentDoc     = { docId  : 0,
			    docVer : 1 };
    // Define a jquery-ui delete confirmation dialog. 
    var tl = this;
    this.deldiag = $("div#deleteConfirmation").dialog(
	{   
	    autoOpen  : false,
	    resizable : false,
            width     : 320,
	    height    : "auto",
	    hide      : "blind",
	    show      : "blind",
	    modal     : true,
	    buttons   : {
		"Delete" : function() {
		    tl.sync( tl, 'deleteDocument', tl.currentDoc, 1);
    		    $(this).dialog( "close");
		},
		"Cancel" : function(){
    		    $(this).dialog("close");
		}
	    }
	}
    );

    // schedule the sync task for the first time after setting the inital data version to 0
    // in order to load all data
    this.syncTimer = setTimeout( this.syncronize, this.syncInterval, this);
};

Documents.prototype.makeTagList = function () {
    var tagstr = $('input#searchstring').val();
    if ( tagstr == "" ) {
	tagstr = "@TODO";
    } else {
	tagstr = "@" + tagstr;
    }
    this.sync( this, 'tagList', {'tag' : tagstr}, 1);
}

Documents.prototype.dosearch = function () {
    var searchstr = $('input#searchstring').val();
    if ( searchstr == "" ) {
	error( "You must enter a search string." );
	return;
    } else {
    }
    this.sync( this, 'tagList', {'tag' : searchstr}, 1);
}

Documents.prototype.toggleEdit = function() {
    // Choose != d in case there are types for automatically generated pages which should not be
    // editable. Only documents should be editable (e.g. ToDo list, search results ...)
    if ( this.docH[ this.currentDoc.docId ].type != 'd' ) {
	return;
    }

    if ( 'document' in CKEDITOR.instances ) {
        this.syncronize( this );
        CKEDITOR.instances.document.destroy();
	$('input#editbutton').attr('src', this.baseurl + '/resources/edit.png');
    } else {	
        CKEDITOR.replace( 'document',  {
            customConfig:  '../ckconfig.js'
        });
	$('input#editbutton').attr('src', this.baseurl + '/resources/noedit.png');
    } 
}

Documents.prototype.syncronize = function( self )
{
    //debug( 'treever ' +  self.docTreeVer );
    docRec = { treeVer : self.docTreeVer,
	       docId   : self.currentDoc.docId,
	       docVer  : self.currentDoc.docVer
	     };
    if ( 'document' in CKEDITOR.instances ) {
	editor = CKEDITOR.instances.document;
	if ( editor.checkDirty() ){
	    $('td#state').html( "changed" );
	    docRec.docTxt = editor.getData();
	    if ( self.syncHook ) {
		self.syncHook();
	    }
	}
    }
    self.sync( self, 'syncDocument', docRec, 1 )
}

///////////////////////////////////// Ajax Handlers ///////////////////////////////////////
//                                                                                       //
///////////////////////////////////////////////////////////////////////////////////////////
// Regularly poll for a foreign update (by another client) on the server.                //
// This is also called for actions to be executed on the server (udpate, new record,     //
// delete record, ...                                                                    //
// The callback function will contain a change record which will then be processed so    //
// that the data in the Document is in-sync with the server. This is also done when the  //
// action was triggered by THIS browser. Like this it is convenient to get back record   //
// IDs for newly created records, or handle cases in which the update did not succeed    //
// because another client edited the same record in at the same time (i.e. slightly      //
// earlier.)                                                                             //
///////////////////////////////////////////////////////////////////////////////////////////

$( document ).ajaxError( function( event, request, settings, errorstr )  {
    error( "Error while sending ajax request " + settings.url + " ==> " + errorstr );
    //debug("in ajaxerror");
});

Documents.prototype.sync = function( self, action, data, promptsync ) {
    // clear a scheduled update poll if we have to execute a command
    //debug( action + JSON.stringify(data));
    if (promptsync)
	clearTimeout(self.syncTimer);
    var sendData = {
	'version' : self.docTreeVer,
	'action'  : action,
	'data'    : JSON.stringify(data)
    };
//    $.getJSON( window.location.href + "/" + self.actionurl, sendData,
//    $.post( window.location.href + "/" + self.actionurl, sendData,
    $.ajax( {type: "POST",
	     url: window.location.href + "/" + self.actionurl,
	     data: sendData,
             timeout: 20000,
	     error  : self.errorHandler,
	     success: function(reply,status) {
		 if ( status != "success" ) {
            error( "Error communicating with server: " + status);
	    self.syncTimer = setTimeout( self.syncconize, self.syncInterval, self);
	    return;
        } else {
	    // The following means that the message fields are not changed if the 
	    // corresponding messages are not present in the data returned from the server.
	    if ( reply.state ) $('td#state').html( reply.state );
	    if ( reply.debug ) debug( reply.debug );
	    if ( reply.info  ) log( reply.info );
	    if ( 'error' in reply ) error( reply.error );
	    if ( reply.data ) {
		self.handleServerReply( reply.data );
	    }
	    self.syncTimer = setTimeout( self.syncronize, self.syncInterval, self);
	}
    },
             dataType: "json",
	     self : self }
	  );
};

Documents.prototype.errorHandler = function( settings, status, errmsg ) {
    clearerror( "An error occurred: " + errmsg + "   --    status: " + status );
//    self.syncTimer = setTimeout( self.syncronize, self.syncInterval, self);
}

$( document ).ajaxError( function( event, jqxhr, settings, exception ) {
    clearerror( 'AJAX exception: ' + exception );
    // try again after a longer time though...
    self = settings.self;
    self.syncTimer = setTimeout( self.syncronize, self.syncInterval, self);
});


// Go through the list of action records and dispatch the function calls. 
Documents.prototype.handleServerReply = function( data ) {
    if ( 'records' in data) {	
	var recArr = data.records;
	for ( var i=0; i<recArr.length; i++ ) {
	    var action = recArr[i].action;
	    var rec = recArr[i];
	    if ( ! (action in this) ) {
		error( "I do not know the command : \"" + action + "\"" );
	    } else {
		this[action](rec);	    
	    }

	}
    }
};

/////////////// Handlers for update records from the server ///////////////
Documents.prototype.tagList = function( rec ) {
    this.currentDoc.docId = 0;
    this.currentDoc.docVer = 0;
    taglist = rec.record.taglist;
    $('td#title').html( '<span class="title">Taglist : ' + rec.record.tag + '</span>' );
    
    var html = '<p><table class="taglist">\n<tr><th>Due Date</th><th>Document</th><th>Item</th></tr>';

    for (ix=0; ix<taglist.length; ix++ )  {
	var item = taglist[ix];

	var tl=this;

	html += '<tr> <td>' + item.duedate + '</td><td onclick="(function(doc){ Doc.sync( doc, \'readDocument\', { \'editMode\' : \'edit\', \'docId\' : ' + item.docId + ' } , 1);})(Doc)" >' + item.docName + '</td><td>' + item.item + '</td></tr>\n';	
    }
    html += "</table></p>";
    if ( 'document' in CKEDITOR.instances ) {
	CKEDITOR.instances.document.destroy();
	$('input#editbutton').attr('src', this.baseurl + '/resources/noedit.png');
    }
    $('div#document').html( html );
}

Documents.prototype.saveDocument = function( rec ) {
    this.currentDoc.docVer = rec.record.docVer;
    // to be clean... in case one day we use the version in the tree...
    this.docH[ rec.record.docId ].version = rec.record.docVer;
    if ( 'document' in CKEDITOR.instances ) {
	CKEDITOR.instances.document.resetDirty();
	changed = 0;
    }
    $('td#state').html( "saved" );
}

Documents.prototype.updateDocument = function( rec ) {
    this.currentDoc.docVer = rec.record.docVer;
    this.currentDoc.docId  = rec.record.docId;
    if ( 'document' in CKEDITOR.instances ) {
	CKEDITOR.instances.document.setData( rec.record.docTxt, function() {this.resetDirty();} );
    } else {
	$('div#' + this.divid).html( rec.record.docTxt );
    }
    $('td#state').html("document updated");   
};

Documents.prototype.updateDropDown = function( rec ) {
    var dlist = [{ 'label' : rec['name'],
		   'id'    : rec['id'] }];
    dlist = this.doDropEntry( rec, dlist, "" );
    $('select#categoryChooser').html("");
    for ( var ix = 0; ix < dlist.length; ix++ ) {        
        $('select#categoryChooser').append( '<option value="' + dlist[ix].id + '">' 
                                            + dlist[ix].label + '</option>');
    }
};


Documents.prototype.doDropEntry = function( rec, dlist, level ) {
    for (var ix in rec.children) {
        var child = rec.children[ix];
	this.docH[ child['id'] ] = child;
	child.parent = rec;
        if (child.type == 'c') {
            dlist.push( { 'label' : level + child['name'],
                          'id'    : child['id'] } );
            level = level + "&nbsp;&nbsp;&nbsp;";
            dlist = this.doDropEntry( child, dlist, level );
            level = level.slice(0, -18);
        }
    }
    return dlist;
};

Documents.prototype.updateDocTree = function( rec ) {
    var res = this.doMenuEntry( rec.record, -1, "");
    this.documentTree = rec.record;
    this.docTreeVer = rec.treeVer;
    this.docH = { 0 : rec.record };
    $('div#nav').html( res );
    this.updateDropDown(rec.record);
    if (this.currentDoc.docId > 0) {
	this.updateDocPath( this.currentDoc.docId );
    }
    var title = this.getTitlePath( this.currentDoc.docId );
    $('td#title').html( title );
};

Documents.prototype.doMenuEntry = function( rec, level, res ) {
    level = level + 1;
    var addon = ' class="l' + level + '"';
    res += '<ul' + addon + '>\n';
    var tbs = rec.children;
    if ( tbs )
	tbs.sort( function(a,b) { 
		if (a.name < b.name ) return -1;
		if (a.name > b.name ) return  1;
		return 0;
	    });
    for ( var ix in tbs ) {
	//    for ( var ix in rec.children ) {
        var child = tbs[ix];
	//        var child = rec.children[ix];
	var indic = ' ';
        if ( child.type == 'c' && level > 0) 
            indic = ' > ';
        if ( child.type == 'd' ) 
            var docclass = ' class="document" ';
        else
            docclass = '';
        res += '<li onclick="Doc.selectDoc( event, ' + child.id + ' )" ' + docclass + addon + ' >' + child.name + indic + '\n';
        res = this.doMenuEntry( child, level, res );
        res +=  '</li>\n';
    }
    level = level - 1;
    res += '</ul>\n';
    return res;
};

// called when a document is loaded in the contents div
Documents.prototype.readDocument = function( rec ) {
    this.currentDoc.docVer = rec.record.docVer;
    this.currentDoc.docId  = rec.record.docId;
    contents = "Document";
    if ( this.docH[ this.currentDoc.docId ].type != 'd' ) {
	if ( 'document' in CKEDITOR.instances ) {
	    CKEDITOR.instances.document.destroy();
	    $('input#editbutton').attr('src', this.baseurl + '/resources/noedit.png');
	}
	$('div#' + this.divid).html( "<center>-- category selected: no editing --</center>" );
    } else {
	if ( 'document' in CKEDITOR.instances ) {
	    CKEDITOR.instances.document.setData( rec.record.docTxt, function() {this.resetDirty(); } );
	} else {
	    $('div#' + this.divid).html( rec.record.docTxt );
	    if( rec.record.editMode == "edit" ) {
		this.toggleEdit();
	    }
	}
    }
    $('td#state').html("document loaded");
    var title = this.getTitlePath( this.currentDoc.docId );
    $('td#title').html( title );
}

Documents.prototype.newDocument = function( rec ) {
    // new records are not yet inserted in rech or data. They just exist in the inputs of the
    // task editor. Therefore the new record has just to be added to the existing tree.
    debug( "inserting new record " + repr(rec.record) );
};

// Called when a record has been deleted from the server and the sync-info is coming back
// This might happen if WE delete a record here or if somebody else deletes and the sync
// signals this.
Documents.prototype.deleteDocument = function( rec ) {

    // delete record from rech and data and from the display.    
    // remove from recH and from the data record. then remove from display (search tr#t{rec.taskId})
    debug( "deleting record with id " + repr(rec.record));
};


////////////////////////////////// End: Ajax Handlers /////////////////////////////////////



//function displayDocument( topic, topicTxt ) {
//    //debug( "display " + topic );
//    var dirpath = topic.replace( datadir + "/", "", 'g' );
//    dirpath = dirpath.replace( new RegExp("/","g"), " - " );
//    dirpath = dirpath.slice( 0, -4 );
//    $('div#topic div.topicHead').html( dirpath );
//    $('div#topic textarea#text').val( topicTxt );
//}

// This function is called if a user selects a menu item.
// !!! Actually the updatedocpath call should be triggered by
// the reply of the server (i.e. called by the reply handler
// which also loads the data. 
Documents.prototype.selectDoc = function( event, docId ) {
//    this.updateDocPath( docId );
//    this.currentDoc.docId = docId;
    event.stopPropagation();

    if ( 'document' in CKEDITOR.instances ) {
	this.syncronize( this );
    }
    this.sync(this, "readDocument", { 'docId' : docId }, 1); 
};

Documents.prototype.updateDocPath = function( docId ) {
    var doch = this.docH;
    var path = "";

    if ( ! (docId in doch) ) {
	this.currentDoc.docId = 0;
	$('div#headline').html("");
	return;
    }

    while (docId != 0) {
	path = doch[ docId ]['name'] + " - " + path ;	
	docId = doch[docId]['parent']['id'];
    }
    $('div#headline').html( path.slice(0, -3)  );
}

Documents.prototype.insertNewDocument = function ( type ) {    
    var name = $('input#documentName').val();
    var category = $('select#categoryChooser').val();
    if ( name != "" ) {
        this.sync(this, "newDocument", { 'docName' : name,
					 'catId'   : category,
					 'type'    : type },
		  1 );
    } else {
	error( "You must enter a name for the new document" );
    }
};

Documents.prototype.rename = function() {    
    var newName = $('input#documentName').val();
    newName.trim();
    if ( ! newName ) {
	error( "Make sure that a document or category is selected and you entered a new name." );
        return;
    }
    if ( this.currentDoc.docId > 0 ) {
	this.sync( this, 'rename', { 'docName' : newName,
				     'docId'   : this.currentDoc.docId }, 1);
    } else {
	error( "Make sure that a document or category is selected and you entered a new name." );
        return;
    }
};

Documents.prototype.moveTo = function() {    
    var catId  = $('select#categoryChooser').val();
    // should check that destination is not child of own tree...
    if ( (this.currentDoc.docId > 0) && ( catId >= 0 ) ) {
	this.sync( this, 'moveTo', { 'catId' : catId,
				     'docId' : this.currentDoc.docId }, 1);
    } else {
	error("Make sure a document or category is selected and the destination category is selected.");
    }    
};

// Called when the user presses the delete function
Documents.prototype.delete = function() {
    if ( this.currentDoc.docId > 0 ) {
        $('div#deleteConfirmation').dialog( "open" );
    } else {
	error( "Make sure the document or the category to be deleted is selected.");
    }
};

Documents.prototype.getTitlePath = function( docId ) {
    title = "";
    while ( docId ) {
	var doc = this.docH[ docId ];
	if ( title == "" ) {
	    title = '<span class="title">' + doc.name + "</span>";
	} else {
	    title = doc.name + " - " + title;
	}
	docId = doc.catId;
    }
    return title;
};

/////////////////////// useful? /////////////////////////////

function selectCategory( e, dir, dirpath ) {
    var ndirpath = dirpath.replace( datadir + "/", "", 'g' );
    ndirpath = ndirpath.replace( new RegExp("/","g"), " - " );
    $('form#tasks input#topic').val( dirpath );
    $('div#topic div.topicHead').html( "Directory: " + ndirpath );
    $('div#topic textreaa#text').val( "" );
    $('div#topic textarea#text').prop( "disabled", true );
    e.stopPropagation();
}

function keyPressed() {
    debug("key");
    if ( changed == 0 ) {
        $('td#state').html("changed");
        changed = 1;
    }
}

