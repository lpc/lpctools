##
# @author Colin Barschel (colin.barschel@cern.ch)
# @file generic data container class from a file
#
import string
import os.path
import numpy as np
import scipy as sp
from mytools import *
from cfile import *
from cvirtual import *
from cmode import *

## contains the data from file cfile
class cdata(cfile):
    ok = False
    ## cdata initiation
    def __init__(self,expt='',fillnr=0,ftype='l',bucket=0):
        cfile.__init__(self,expt,fillnr,ftype,bucket)

    """ fields from data file """
    def fields(self):
        ## luminosity files
        if self.ftype == 'l':
            return ['time','stab','l','dl','sl','dsl']
        
        ## luminous region files
        elif self.ftype == 'r':
            return ['time','stab','x','dx','y','dy','z','dz',
                    'xsu','dxsu','ysu','dysu','zsu','dzsu',
                    'ax','dax','ay','day']
        ## summary file
        elif self.ftype == 's':
            return ['timestart', 'timestop', 'plu', 'lui']
        
        ## beam gas files
        elif self.ftype == 'b1' or self.ftype == 'b2':
            return ['time','stab','x','dx','y','dy',
                    'ax','dax','ay','day',
                    'xsu','dxsu','ysu','dysu']
        else:
            return ['']
        
    """ fields containing the main data """
    def dfields(self):
        ## luminosity files
        if self.ftype == 'l':
            return ['l','sl','il']
        
        ## luminous region files
        elif self.ftype == 'r':
            return ['x','y','z','xsu','ysu','zsu','ax','ay']

        ## summary file
        elif self.ftype == 's':
            return ['plu', 'lui']
        
        ## beam gas files
        elif self.ftype == 'b1' or self.ftype == 'b2':
            return ['x','y','xsu','ysu','ax','ay']

        else:
            return ['']
        
    def read(self,ftype='',entries=0,sb=0):
        """
        if not self.exist():
            print 'file not found',self.f(self.ftype)
            self.ok = False
            return False
        """
        
        if ftype != '':
            self.ftype = ftype

        #print self.ftype
            
        try:
            if self.ftype == 'fills2010.txt':
                self.d = readlpcfills(self.ff())
            else:
                self.d = readlpc(self.ffile(),self.fields(),entries,self.spl)


            if len(self.d[self.d.keys()[0]]) > 0:
                self.ok = True
                if self.ftype == 'l':
                    self.sort()
        except Exception,e:
            print 'ERROR file',str(e),self.ff()
            self.ok = False
        
        if self.ok:
            self.sb = sb # stable beam start timestamp
            # calculate integrated luminosity
            # add it as additional dict key
            if self.ftype == 'l': # int lumi makes only sens on a lumi file...
                self.intlum(sb)
                self.avg()
            
            
            if self.ftype == 'r':
                self.avg()
                self.posDev()

            if self.ftype[0] == 'b':
                self.avg()

        return self.ok
               
        

    ## time sort the data
    def sort(self):
        # idx is the array index representing the sorting permutations
        # appying the index to an unsorted array will sort it
        #print "sort time"
        idx = np.argsort(self.d['time'])
        for key in self.d.keys():
            self.d[key] = self.d[key][idx]
            
    ## index of stable beam data stab == 1
    def stabidx(self,stab=1,idx=[]):
        # idx is the array index for stab == 1
        sidx = []
        
        if len(idx) == 0:
            idx = range(len(self.d['time']))
            
        for i in idx:
            if int(self.d['stab'][i]) == stab:
                sidx.append(i)

        return sidx
    
    ## salculate integrated lumonisity
    # @param sb Stable beam timestamp
    def intlum(self,sb=0):
        
        # the data must by time sorted before
        intl = 0
        self.d['il'] = np.copy(self.d['l'])
        self.d['dil'] = np.copy(self.d['dl'])
        
        for i in range(1,len(self.d['time'])):
            if self.d['l'][i] < 0:
                print 'WARNING negative lumi',self.expt,self.fillnr,self.d['l'][i]
                continue # for ALICE!
            
            #delta_t = 0 # time between two data points

            """
            # if data starts after SB timestamp
            if i == 0 and self.d['time'][i] > sb: 
                delta_t = self.d['time'][i] - sb
            elif self.d['time'][i] > sb and self.d['time'][i-1] > sb:
                delta_t = self.d['time'][i] - self.d['time'][i-1]
            """
            delta_t = self.d['time'][i] - self.d['time'][i-1] # in s
            intl += self.d['l'][i] * delta_t * self.d['stab'][i]
            
            self.d['il'][i] = intl

        return intl

    # emittance from luminous reg sigmas
    def eLumireg(self,mode=None):

        if mode == None: # beam mode (class cmode)
            M = cmode(self.fillnr)
        else:
            M = mode

        # Ca = angle correction
        # ei = 2 * sigma_i_lum^2 * Ca_i^2 / B*
        bstar = M.betastar()[self.expt]
        cp = M.crossingPlane(self.expt)
        
        if 'tzsu' not in self.d.keys():
            self.trueSigmaZ(mode=M)
        if 'ctheta' not in self.d.keys():
            self.corrTheta(mode=M)
        
        print self.expt,'B*',bstar
        

        for j in ('x','y'):
            if j == cp:
                # apply crossing angle correction
                self.d['e_'+j] = (2 * self.d[j+'su']**2 * self.d['ctheta']**-2) / bstar
                self.d['de_'+j] = self.d['e_'+j] \
                          * np.sqrt( (self.d['dctheta']/self.d['ctheta'])**2 \
                              + (self.d['d'+j+'su']/self.d[j+'su'])**2
                          )

            else:
                self.d['e_'+j] = (2 * self.d[j+'su']**2) / bstar

                self.d['de_'+j] = self.d['e_'+j] \
                          * (self.d['d'+j+'su']/self.d[j+'su'])

                
        for k in ('e_x','e_y'):
            # warning add 1 to deal with error = 0
            avg = np.average(self.d[k],weights=1/(self.d['d'+k]+1.))
            std = np.std(self.d[k])

            self.d[k+'_avg'] = avg
            self.d[k+'_std'] = std


    """ calculate emittance """
    def emittance(self,mode=None):
        if mode == None: # beam mode (class cmode)
            M = cmode(self.fillnr)
        else:
            M = mode

        V = cvirtual(self.fillnr,M)
        if self.ftype == 'r':
            self.eLumireg(mode=M)
        else:
            V.eLumi(D=self)
        

    """ time average raw data. This will REPLACE the data """
    def tavg(self,dt=300,t1=0):
        keys = self.fields()
        keys = self.d.keys()
        nd = {}
        # first copy extra dict fields (e.g. x_avg)
        #for k in self.d.keys():
        #    nd[k] = self.d[k][:]
        for k in keys:
            nd[k] = []

        if t1 == 0:
            t1 = int(self.d['time'][0])
            
        t2 = t1 + dt

        j = 0 # start index of time interval
        
        while t2 < self.d['time'][-1]:
            
            idx = np.where(self.d['time'][np.where(self.d['time']<t2)]>=t1)
            if len(idx[0]) > 0:

                for k in keys:
                    #print k,idx,len(idx),self.d[k][idx]
                    if k == 'time':
                        nd[k].append(int((t1+t2)/2.))
                    elif '_avg' in k or '_dev' in k or '_std' in k:
                        continue
                    else:
                        nd[k].append(np.average(self.d[k][idx]))

            t1 = t2
            t2 += dt

        for field in nd:
            if '_avg' in field or '_dev' in field or '_std' in field:
                nd[field] = np.array(self.d[field])
            nd[field] = np.array(nd[field]) # convert to numpy array

            
        self.d = nd


        
    """ calculate averages and std dev for each main data field """
    def avg(self,fields=[]):
        if len(fields) == 0:
            fields = self.dfields()
            
        for k in fields:
            # warning add 1 to deal with error = 0
            if min(self.d['d'+k]) == 0:
                avg = np.average(self.d[k])
                std = np.std(self.d[k])
            else:
                avg,sumw = np.average(self.d[k]
                                      ,weights=1/(self.d['d'+k]**2)
                                      ,returned=True)
                std = 1/(np.sqrt(sumw))
            

            self.d[k+'_avg'] = avg
            self.d[k+'_std'] = std

    """ claculate the position deviation from its average """
    def posDev(self):
        for k in self.dfields():
            self.d[k+'_dev'] = self.d[k] - self.d[k+'_avg']
            self.d['d'+k+'_dev'] = self.d['d'+k]

            self.avg([k+'_dev'])


    """ calculate true sigma Z based on half crossing angle theta """
    def trueSigmaZ(self,theta=None,mode=None):
        print "calculate true bunch Z length",self.expt,self.fillnr
        if mode == None: # beam mode (class cmode)
            M = cmode(self.fillnr)
        else:
            M = mode
            
        if theta == None:
            cp = M.crossingPlane(self.expt)
            theta = M.theta(fillnr=self.fillnr)[self.expt][cp] * 1e-6


        self.d['tzsu'] = trueSigmaZ(self.d['zsu'],theta,self.d[cp+'su'])

        self.d['dtzsu'] = self.d['tzsu'] \
            * np.sqrt( (self.d['dzsu']/self.d['zsu'])**2 \
                          + (self.d['d'+cp+'su']/self.d[cp+'su'])**2
                      )

        # return index of good value (exclude outsiders)
        idx = np.where(self.d['zsu'][np.where(self.d['zsu']>10)]<150)
        return idx[0]
            
    """ calculate the crossing angle correction 1/(1+(za/x)**2)**1/2 """
    def corrTheta(self,theta=None,mode=None):
        print "calculate crossing angle correction",self.expt,self.fillnr
        if mode == None: # beam mode (class cmode)
            M = cmode(self.fillnr)
        else:
            M = mode
            
        if theta == None:
            cp = M.crossingPlane(self.expt)
            theta = M.theta(fillnr=self.fillnr)[self.expt][cp] * 1e-6

        print self.expt,'angle',theta

        if 'tzsu' not in self.d.keys():
            self.trueSigmaZ(theta=theta,mode=M)

        self.d['ctheta'] = corrtheta(self.d['tzsu'],self.d[cp+'su']*np.sqrt(2),theta)

        self.d['dctheta'] = self.d['ctheta'] \
            * np.sqrt( (self.d['dtzsu']/self.d['tzsu'])**2 \
                          + (self.d['d'+cp+'su']/self.d[cp+'su'])**2
                      )

        # return index of good value (exclude outsiders)
        idx = np.where(self.d['zsu'][np.where(self.d['zsu']>10)]<150)
        return idx[0]
            
   

## Test the cfile class
if __name__ == "__main__":
    import sys
    if len(sys.argv) > 3:
        expt = sys.argv[1]
        fillnr = int(sys.argv[2])
        bucket = int(sys.argv[3])
    else:
        print "Enter expt fillnr bucket as argument"
        sys.exit(1)
    
    # test reading data
    t = 'l'
    
    D = cdata(expt,fillnr,t)
    print D.f(t)
    print D.ff(t)

    # check if file exists
    if D.exist():
        print 'file exists'
        D.read()
        print D.d['time'][:2]
    else:
        print 'file does not exists'
