function togglePmControl() {
    if ( contents != "ProjectManager" ) return;
    if ( doNotToggle == 1 ) return;
    doNotToggle = 1;
    setTimeout(function() { doNotToggle = 0; }, 800);

    if (! ('tag' in Doc)) return;
    
    if ( Doc.displayMethod == 'catOrder' ) {
	Doc.displayMethod = 'timeOrder';
    } else {
	Doc.displayMethod = 'catOrder';
    }
    Doc.drawGantt( );
}

function ProjectManager( name, options ) {
    Documents.call( this, name, options );
    this.pixperday = 5;
    this.recordHeight = 12;
    this.recordDistance = 4;
    this.displayMethod = "timeOrder";
    this.xoff = 200;
    this.defaultRecordColor = "#6060ff";
    this.defaultTag = "@LPC";
    this.month=new Array();
    this.month[0]="January";
    this.month[1]="February";
    this.month[2]="March";
    this.month[3]="April";
    this.month[4]="May";
    this.month[5]="June";
    this.month[6]="July";
    this.month[7]="August";
    this.month[8]="September";
    this.month[9]="October";
    this.month[10]="November";
    this.month[11]="December";
};


ProjectManager.prototype = Object.create( Documents.prototype );
ProjectManager.prototype.constructor = ProjectManager;

ProjectManager.prototype.getGantt = function( rec ) {    
    var tagstr = $('input#searchstring').val();
    if ( tagstr == "" ) {
	tagstr = this.defaultTag;
    } else {
	tagstr = "@" + tagstr;
    }
    this.sync( this, 'getGantt', {'tag' : tagstr}, 1);
};

ProjectManager.prototype.checkRecords = function( recs ) {
    for( var i=0; i<recs.length; i++ ) {
	var rec = recs[i];
	var sdate = rec.dates[0];
	
	if ( ! /\d{4}-\d{1,2}-\d{1,2}/.test(sdate) ) {
	    error( sdate + " is not a valid start=date in \"" + rec.shorttxt + "\" (" + rec.docId + ")" );
	}

	if( rec.dates.length > 1 ) {
	    var edate = rec.dates[1];
	
	    if ( ! /\d{4}-\d{1,2}-\d{1,2}/.test(edate) ) {
		error( edate + " is not a valid end-date in \"" + rec.shorttxt + "\" (" + rec.docId + ")" );
	    }
	}
    }
};

// callback from the server to display the gantt chart.
ProjectManager.prototype.displayGantt = function( orec ) {
    contents="ProjectManager";
    this.pmrec = orec;
    this.tag = orec.tag;
    var data = orec.record;
    this.records = data.entries;
    this.catlist = data.catlist;
    var recs = this.records;
    if (recs.length == 0) {
        error( "no gantt data for " + this.tag);
        return;
    }
    this.checkRecords(recs);

    recs.sort( function(a,b) {
        return a.dates[0].localeCompare(b.dates[0]);
    } );
    this.startdate = new Date(recs[0].dates[0]);
    this.enddate = this.startdate;

    // the time in ms where the plot starts
    this.startx = this.startdate.getTime();
    
    // first pass: add x coordinates to the items, find the max date
    // the y coordinates will be defined incremental when drawing (depends on the way
    // we want to draw/order the records
    var maxx = 0;    
    for( var i=0; i<recs.length; i++ ) {
	var rec = recs[i];
	this.convertDates( rec );
	if ( rec.dates[1] > this.enddate ) {
	    this.enddate = rec.dates[1];
	    maxx = rec.datesx[1];
	}
    }
    this.canvasWidth = maxx + this.xoff;

    this.drawGantt();

};
    ///////////////////// general data preparation over //////////////////////

ProjectManager.prototype.drawGantt = function() {
    var recs = this.records;

    if ( this.displayMethod == "timeOrder" ) {
	// better: some offset for every category
	this.totheight = recs.length * (this.recordHeight+this.recordDistance) + 100;
    } else if( this.displayMethod == "catOrder" ) {
	this.totheight = recs.length * (this.recordHeight+this.recordDistance) + 100 + Object.keys(this.catlist).length * this.recordHeight;
    }



//    var html = '<div class="over" >Hi</div><svg width="'+this.canvasWidth+'" height="'+this.totheight+'"></div>'
    //var html = '<svg width="'+this.canvasWidth+'" height="'+this.totheight+'">'
    var html = '<script>$(".doc").scroll(function(){$(".over").css("left", 20 - $(".doc").scrollLeft());})</script>'
    html += this.htmlTimeOverlay();
    html += '<svg width="'+this.canvasWidth+'" height="'+this.totheight+'">';
    html += '\
<defs>\
<filter id="bevel" >\
  <feGaussianBlur in="SourceAlpha" stdDeviation="2" result="blur"/>\
  <feOffset in="blur" dx="3" dy="3" result="offsetBlur"/>\
<!--\
  <feSpecularLighting surfaceScale="5" specularConstant=".75"\
      specularExponent="20" lighting-color="#bbbbbb" in="blur"\
      result="highlight">\
    <fePointLight x="-5000" y="-10000" z="20000"/>\
  </feSpecularLighting>\
  <feComposite in="highlight" in2="SourceAlpha" operator="in" result="highlight"/>\
  <feComposite in="SourceGraphic" in2="highlight" operator="arithmetic"\
               k1="0" k2="1" k3="1" k4="0" result="highlightText"/>\
-->\
  <feMerge>\
    <feMergeNode in="offsetBlur"/>\
    <feMergeNode in="SourceGraphic"/>\
  </feMerge>\
</filter>\
</defs>\
';

    html = this.makeGrid( html );

    for ( var ir = 0; ir < recs.length; ir++ ) {
	if ( ! 'cat' in recs[ir] ) { recs[ir]['cat'] = "AOB"; }
    }
    this.catlist['AOB']= {'color' : "#808080",
			  'seqno' : 0 };
    // second pass: draw the stuff
    if ( this.displayMethod == "catOrder" ) {
	var y = 50;
	var list = this.sortCatlist(Object.keys( this.catlist ));
	for ( var i=0; i<list.length; i++ ) {
	    var cat = list[i];
	    html = this.catLine( cat, y, html );
	    y = y + this.recordHeight + this.recordDistance;
	    for ( ir = 0; ir < recs.length; ir++ ) {
		var rec = recs[ir];
		if (rec.cat != cat) { continue; }
		html = this.chartLine( rec, y, html );
		y += this.recordHeight+this.recordDistance;
	    }
	}
    } else {
	for( var i=0; i<recs.length; i++ ) {
	    var y = i*(this.recordHeight+this.recordDistance)+ 50;
	    var rec = recs[i];
	    html = this.chartLine( rec, y, html );
	}
    }

    html += '</svg>';
    //debug (html);
        if ( 'document' in CKEDITOR.instances ) {
	CKEDITOR.instances.document.destroy();
    }

    $('td#title').html( '<span class="title">GANTT Chart : ' + this.tag + '</span>' );

    $('div#document').html( html );

};

ProjectManager.prototype.sortCatlist = function( list ) {
    var cl = this.catlist;
    list.sort( function(a,b) {
	//debug( "sort " + a + " " + cl[a].seqno + "-" +b+" " + cl[b].seqno);
	if (parseInt(cl[a].seqno) == 0) return 1;
	if (parseInt(cl[b].seqno) == 0) return -1;
	if ( cl[a].seqno < 0 && cl[b].seqno < 0 ) {
	    return ( cl[b].seqno - cl[a].seqno );
	}
	if ( (cl[a].seqno * cl[b].seqno) < 0 ) {
	    return (parseInt(cl[b].seqno) - parseInt(cl[a].seqno));
	} else {
	    return (parseInt(cl[a].seqno)-parseInt(cl[b].seqno));
	}
    });
    //for ( var i=0; i<list.length; i++ ) {
	//debug(list[i]);
    //}
    return list;
};

ProjectManager.prototype.chartLine = function( rec, y, html ) {
    if ( 'cat' in rec && 'color' in this.catlist[rec.cat] ) {
	   var color = this.catlist[rec.cat]['color'];
    } else {
	   var color = this.defaultRecordColor;
    }
    if ( rec.datesx[0] == rec.datesx[1] ) { //milestone
//        html += '<line filter=url("#bevel") x1="'+(rec.datesx[0]-4)+'" y1="'+y+'" x2="'+(rec.datesx[0]+4)+'" y2="'+y+'" style="stroke:'+ color +';stroke-width:'+(this.recordHeight-2)+';"><title> '+rec.dates[0].toDateString()+'</title></line>\n';
        html += '<polygon  points="' + rec.datesx[0] + ',' + (y - this.recordHeight/2) + ' ' +
                                                           (rec.datesx[0] + this.recordHeight/2) + ',' + y + ' ' +
                                                           rec.datesx[0] + ',' + (y + this.recordHeight/2) + ' ' +
                                                           (rec.datesx[0] - this.recordHeight/2) + ',' + y + 
                '" style="stroke:black; stroke-width=1px; fill:'+ color + ';"><title> '+rec.dates[0].toDateString()+'</title></polygon>\n';
    } else {
    	html += '<line x1="'+rec.datesx[0]+'" y1="'+y+'" x2="'+rec.datesx[1]+'" y2="'+y+'" style="stroke:'+color+';stroke-width:'+(this.recordHeight-2)+';stroke-linecap:round;" ><title>'+rec.dates[0].toDateString()+' - ' + rec.dates[1].toDateString()+'</title></line>\n';
//    	html += '<line filter=url("#bevel") x1="'+rec.datesx[0]+'" y1="'+y+'" x2="'+rec.datesx[1]+'" y2="'+y+'" style="stroke:'+color+';stroke-width:'+(this.recordHeight-2)+';stroke-linecap:round;" ><title>'+rec.dates[0].toDateString()+' - ' + rec.dates[1].toDateString()+'</title></line>\n';
    }

    html += '<text text-anchor="end" x="'+(rec.datesx[0]-8)+'" y="' + (y+4) + '" onclick="(function(doc){ Doc.sync( doc, \'readDocument\', { \'editMode\' : \'edit\', \'docId\' : ' + rec.docId + ' } , 1);})(Doc)" >' + rec.shorttxt + '<title>'+rec.cat+'<br>'+rec.longtxt +'</title></text>';

    return html;
}

ProjectManager.prototype.catLine = function( cat, y, html ) {
    if ( 'color' in this.catlist[cat] ) {
	var color = this.catlist[cat]['color'];
    } else {
	var color = this.defaultRecordColor;
    }

    var tel = '<text style="font-size:14px;font-weight:bold;fill:#000000;" x="'+ 5 +'" y="' + y + '" >' + cat + '</text>';
    html += tel;
    var l = cat.length;
    html += '<line x1="'+ 5 +'" y1="'+(y+3)+'" x2="'+ this.canvasWidth+'" y2="'+(y+3)+'" style="stroke:'+color+';stroke-width:'+(this.recordHeight/4)+';" ></line>\n';

    return html;
}

ProjectManager.prototype.makeGrid = function( html ) {
    //html += '<div class="over">'
    // months
    var workdate = new Date( this.startdate );
    workdate.setDate(1);
    if (workdate < this.startdate ) {
	workdate.setMonth( workdate.getMonth()+1 );
    }
    while (workdate < this.enddate) {
	var x = this.xofdate( workdate );
	lwidth = '2';
	if (workdate.getMonth() == 0) {
 //	    html += '<text x="'+(x-35)+'" y="15">' + ( workdate.getYear() + 1900 ) + '</text>';
	    lwidth = '5';
	}
	html += '<line x1="'+x+'" y1="0" x2="'+x+'" y2="'+this.totheight+'" style="stroke:black; stroke-width:' + lwidth + '; stroke-opacity:0.2" />';
//	html += '<text x="'+(x+5)+'" y="15">'+ this.month[workdate.getMonth()] +'</text>';
	workdate.setMonth( workdate.getMonth()+1 );
    }
    var x = this.xofdate( workdate );

    // weeks
    workdate = new Date( this.startdate );
    workdate.setDate( workdate.getDate() - workdate.getDay() + 1 );
    while (workdate < this.enddate) {
	var x = this.xofdate( workdate );
	html += '<line x1="'+x+'" y1="20" x2="'+x+'" y2="'+this.totheight+'" style="stroke:black; stroke-width:1; stroke-opacity:0.1" />';
//	html += '<text x="'+(x+5)+'" y="35">'+ workdate.getDate() +'</text>';
	workdate.setDate( workdate.getDate()+7 );
    }
    x = this.xofdate( workdate );

    // finally a line representing today
    var today = new Date();
    var xtoday = this.xofdate( today );
    html += '<line x1="'+xtoday+'" y1="20" x2="'+xtoday+'" y2="'+this.totheight+'" style="stroke:red; stroke-width:1; stroke-opacity:0.8" />';
    html += '<text fill="red" x="'+(xtoday+5)+'" y="35">today</text>';
    //html += '</div>'
    return html;
};

ProjectManager.prototype.htmlTimeOverlay = function( ) {

    var ovbar = '<div class="over">';
    // months
    workdate = new Date( this.startdate );
    workdate.setDate(1);
    if (workdate < this.startdate ) {
	workdate.setMonth( workdate.getMonth()+1 );
    }
    while (workdate < this.enddate) {
	var x = Math.round(this.xofdate( workdate ));
	ovbar += '<div class="overtxt" style="width:130px;position:absolute;left:'+(x+3)+'px; top:14px;">'+ this.month[workdate.getMonth()] + ' (' + (workdate.getYear() + 1900) + ') </div>';
	workdate.setMonth( workdate.getMonth()+1 );
    }
    x = this.xofdate( workdate );

    // weeks
    workdate = new Date( this.startdate );
    workdate.setDate( workdate.getDate() - workdate.getDay() + 1 );
    while (workdate < this.enddate) {
	var x = Math.round(this.xofdate( workdate ));
	ovbar += '<div class="overtxt" style="position:absolute;left:'+(x+3)+'px; top:34px;">'+ workdate.getDate() +'</div>';
	workdate.setDate( workdate.getDate()+7 );
    }
    x = this.xofdate( workdate );

//    // finally a line representing today
//    var today = new Date();
//    var xtoday = this.xofdate( today );
//    ovbar += '<div style="foreground-color:red; position:absolute;left:'+(xtoday+4)+'px; top:34px;">today</div>';
    ovbar += '</div>';
    return ovbar;
}

//ProjectManager.prototype.sortDates = function( recs ) {
//    
//};


ProjectManager.prototype.xofdate = function( date ) {
    return this.pixperday * (date.getTime() - this.startx)/(24*3600000) + this.xoff;
};


// if there is only one date the second date is made equal to the first
// the Dates in the records are converted to javascript dates.
// the x coordinates of the dates are added to the records (datesx[2])
ProjectManager.prototype.convertDates = function( rec ) {
    rec.dates[0] = new Date(rec.dates[0]);
    rec.datesx = new Array();
    rec.datesx[0] = this.xofdate( rec.dates[0] );
    if (rec.dates.length > 1) {
	rec.dates[1] = new Date(rec.dates[1]);
	rec.datesx[1] = this.xofdate( rec.dates[1] );
    } else {
	rec.dates[1] = rec.dates[0];
	rec.datesx[1] = rec.datesx[0];
    }
    if ( rec.datesx['x2'] - rec.datesx['x1'] < 0 ) {
	error( "end-date < start-date for " + rec.shorttxt );
    }

};
