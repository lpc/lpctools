#!/usr/bin/python
import matplotlib as mpl
mpl.use('Agg')
#mpl.interactive(True)
from mytools import *
from scipy import optimize
from datetime import datetime
import time
import numpy as np
import scipy as sp
import pylab as P
import matplotlib.pyplot as plt 
import matplotlib.offsetbox as offsetbox
from matplotlib.dates import *
import matplotlib.ticker as ticker
from scipy import interpolate
from scipy import optimize
from scipy import integrate
import string, sys, os
from cfile import *
from cdata import *
from cplot import *
from cmode import *
#from config import *
import readconfig
from lpcutils import Logger

config = readconfig.readconfig("lumi_config.json")

YEAR = config['year']
YEAR = config['year']
LPC_FIG_DIR = os.path.join( config['workBaseDir'], config['year'], "measurements/FIGURES" )
LPC_DATA_DIR = os.path.join( config['workBaseDir'], config['year'], "measurements" )

firstFill = int(config["firstFillIon"])
lastFill = int(config["lastFillIon"])

logger = Logger(config['logfile'])

print "Making Ion plots for the year " + str(YEAR)
logstr =  "Making pp lumi plots for the year " + str(YEAR) + "\n"

# rsync -rvL --exclude=2010 ~/Documents/Doktorarbeit/analysisLumi/codeLPC/ cbarsche@lxplus.cern.ch:codeLPC/
# rsync -rvL --exclude=2010 --exclude=config.py --exclude=plots/ ~/Documents/Doktorarbeit/analysisLumi/codeLPC/ cbarsche@lxplus.cern.ch:codeLPC/

# show an overview for one data set (Fill/bucket)
# and compare all experiments
save = True
species = "PbPb"
# save basic lumi plots in FIG_DIR and FIG_LUMI_DIR
LPC_FIG_LUMI_DIR = LPC_FIG_DIR+'/lumi_plots'
#if not os.path.isdir(LPC_FIG_LUMI_DIR):
#    os.mkdir(LPC_FIG_LUMI_DIR)

def getLumi(fillnr,expt):
    #print 'getLumi',fillnr,expt
    
    fillnr = int(fillnr)
    lui_s = lui_l = lui = 0 # integrated lumi  Hz/ub
    plu_s = plu_l = plu = 0 # peal lumi        1/ub

    s = cdata(expt,fillnr,'s')
    s.read()

    if s.ok:
        lui_s = np.sum(s.d['lui'])
        lui = lui_s
        plu_s = max(s.d['plu'])
        plu = plu_s

    if lui == 0 or plu == 0:# or expt == 'LHCb':
        d = cdata(expt,fillnr,'l')
        d.read()
        if d.ok:
            lui_l = d.intlum()
            lui = lui_l
            plu_l = max(d.d['l'])
            plu = plu_l

            #print 'LHCb intLumi %4d %4d %4d' % (fillnr,lui_s/1000.,lui_l/1000.)
        

    sformat = '%6s %4d integrated %9d peak %6.1f use summary: %5s'
    print sformat % (expt,fillnr,int(lui),plu,s.ok)
    #print expt,fillnr,'integrated:',lui,'peak:',plu,'use summary:',s.ok

    return lui,plu
    
    
if len(sys.argv) > 1:
    log = sys.argv[1]
else:
    log = ''

################# for pp 2015 #####################

# exclfills = range(1650,1659)
# exclfills.append(1706) # buggy fill
# exclfills.append(1717) # data is in the fill 1716
# # Take out the heavy ion fills which start with 4658 (last pp fill is 4643)
# # Fills 4634 to 4647 are the reference run. 4569 is the last stable beams pp run
# exclfills.extend(range(4570,4721))


exclfills = range( 3000, 4658 )
#exclfills_exp = { 'LHCb' : [4658,4659,4660,4661,4664,4666,4669,4671], # wrongly
#exclfills_exp = { 'LHCb' : [4658,4659,4660], # wrongly calibrated x-section
exclfills_exp = { 'LHCb' : [],
                  'ALICE' : [],
                  'CMS' : [],
                  'ATLAS' : [] }

###################################################

M = cmode()

# if SB modes not used for some reason
ATLASDIR=LPC_DATA_DIR+'/ATLAS'
fills = os.walk(ATLASDIR).next()[1]#[-6:]
fills.sort()

fills = M.sbFills()#[-10:-5]
#fills = [1717,1718,1721] # debug set some fills manually

#lumi_scale = 1e6
lumi_scale = 1
#lumi_unit = 'pb'
lumi_unit = '$\mu$b'

#################################################
#
#       read data files - create luminosity dict
#
# lui  = integrated luminosity total
# luif = integrated luminosity for this fill
# plu  = peak luminosity for this fill
#
#################################################

print 'Luminosity based on fills',fills

L = {'fill':[2358],'tstart':[0],'tstop':[0],
     'ATLAS_lui':[0],'CMS_lui':[0],'LHCb_lui':[0],'ALICE_lui':[0],
     'ATLAS_luif':[0],'CMS_luif':[0],'LHCb_luif':[0],'ALICE_luif':[0],
     'ATLAS_plu':[0],'CMS_plu':[0],'LHCb_plu':[0],'ALICE_plu':[0]}

for fillnr in fills:
    if fillnr < firstFill:
        continue
    if fillnr > lastFill:
        continue

    if fillnr in exclfills:
        continue
    
    tsb,tdump = M.sb(fillnr,format=1) # SB and dump times
    L['fill'].append(fillnr)
    L['tstart'].append(tsb)
    L['tstop'].append(tdump)
    
    for expt in ['ATLAS','CMS','LHCb','ALICE']:

        if fillnr in exclfills_exp[ expt ]:
            lui = plu = 0
        else:
            lui,plu = getLumi(fillnr,expt)
            
        L[expt+'_luif'].append(lui)
        L[expt+'_lui'].append(lui + L[expt+'_lui'][-1])
        L[expt+'_plu'].append(plu)

for k in L.keys():
    L[k] = np.array(L[k])

# generate data for daily integrated luminosity
luistep = {'time':[],'ATLAS':[],'CMS':[],'LHCb':[],'ALICE':[]} # int lumi steps
luif = {'fill':[],'ATLAS':[],'CMS':[],'LHCb':[],'ALICE':[]} # int lumi per fill
luid = {'time':[],'ATLAS':[],'CMS':[],'LHCb':[],'ALICE':[]} # int lumi per day

for fillnr in fills:
    if fillnr < firstFill:
        continue
    if fillnr > lastFill:
        continue
    if fillnr in exclfills:
        continue

    tsb,tdump = M.sb(fillnr,format=1) # SB and dump times
    luistep['time'].append(tsb)
    luistep['time'].append(tsb)
    luistep['time'].append(tdump)
    luistep['time'].append(tdump)

    dt = (tdump - tsb) # fill duration in unit of day

    # per fill:
    luif['fill'].append(fillnr)

    index = np.where(L['fill'] == fillnr)[0][0]
    
    for expt in ['ATLAS','CMS','LHCb','ALICE']:
        
        lui = L[expt+'_luif'][index]
        luistep[expt].append(1e-9)
        luistep[expt].append(lui/dt)
        luistep[expt].append(lui/dt)
        luistep[expt].append(1e-9)

        luif[expt].append(lui)
    
sie = {} # exptrapolation function for int lumi
for expt in ['ATLAS','CMS','LHCb','ALICE']:
    si = interpolate.interp1d(luistep['time'], luistep[expt], kind='linear')
    sie[expt] = extrap1dsame(si)
    

tint,tdump = M.sb(3545,format=1)
datstr = str(YEAR) + "-02-01 00:00:1.000"
tint = timeDateNum(datstr)
tint = int(tint - 1)
sbti = 0
td = [] # timestamp
sd = [] # time per day in SB

print "do daily integration"

while tint < luistep['time'][-1]:

    luid['time'].append(tint)
    
    for expt in ['ATLAS','CMS','LHCb','ALICE']:
        sbresult = integrate.quad(sie[expt],tint,tint+1,full_output=1)
        sbt = sbresult[0]

        luid[expt].append(sbt)
        luid[expt].append(sbt)

    tint += 1
    luid['time'].append(tint)


luid['time'].append(tint)  # time ticks
for expt in ['ATLAS','CMS','LHCb','ALICE']:
    luid[expt].append(1e-9)     # daily integration per 24h

for k in luid.keys():
    luid[k] = np.array(luid[k])
    
for k in luif.keys():
    luif[k] = np.array(luif[k])
    
for k in luistep.keys():
    luistep[k] = np.array(luistep[k])

# total integrated luminosity
for expt in ['ATLAS','CMS','LHCb','ALICE']:
    L[expt+'_label'] = '%5s %1.3f %s$^{-1}$' % (expt,L[expt+'_lui'][-1]/lumi_scale,lumi_unit)
#    if expt == 'ALICE':
#        L[expt+'_label'] = '%5s %1.3f %s$^{-1}$' % (expt,L[expt+'_lui'][-1]/1e6,'pb')
    print L[expt+'_label']


    
#################################################
#
#         plots
#
#################################################

lastfill = int(L['fill'][-1])
tnow = datetime.now()
tstr = str(tnow.strftime("%Y-%m-%d %H:%M"))
genstr = '(generated '+tstr+' including fill '+str(lastfill)+')'
print genstr

prop = mpl.font_manager.FontProperties(size='small')
color = {'ATLAS':'k','CMS':'g','LHCb':'b','ALICE':'r'}
mew = {'ATLAS':1,'CMS':1,'LHCb':1,'ALICE':1} # markeredgewidth
liw = {'ATLAS':1,'CMS':1,'LHCb':1,'ALICE':1} # line width
mfc = {'ATLAS':'0.5','CMS':'0.8','LHCb':'0.9','ALICE':'1'} # markerfacecolor
msz = {'ATLAS':5,'CMS':6,'LHCb':4,'ALICE':5} # markersize
marker = {'ATLAS':'o','CMS':'^','LHCb':'D','ALICE':'s'}

def custom_date_axis_month(ax):
    ax.xaxis_date()
    
    #ax.xaxis.set_major_locator(WeekdayLocator(byweekday=MO))
    #ax.xaxis.set_major_formatter( DateFormatter('%V') )
    #ax.set_xlabel('Week in 2012')
    
    ax.xaxis.set_major_locator(MonthLocator())
    ax.xaxis.set_minor_locator(MonthLocator(bymonthday=15))
    ax.xaxis.set_minor_formatter(DateFormatter('%b')) #abbreviated month name (use '%B' for full name)
    ax.set_xlabel('Month in ' + str(YEAR))

    ax.xaxis.set_major_formatter(ticker.NullFormatter())
    ax.grid(b=True,linewidth=0.3,color="0.1",alpha=0.6)
    for tick in ax.xaxis.get_minor_ticks():
        tick.tick1line.set_markersize(0)
        tick.tick2line.set_markersize(0)
    
def custom_date_axis(ax):
    ax.xaxis_date()

    ax.xaxis.set_major_locator(WeekdayLocator(byweekday=MO))
    ax.xaxis.set_major_formatter( DateFormatter('%V') )
    ax.set_xlabel('Week in ' + str(YEAR))

    ax.set_xlim(735700,None)
    #ax.xaxis.set_major_locator(MonthLocator())
    #ax.xaxis.set_minor_locator(MonthLocator(bymonthday=15))
    #ax.xaxis.set_major_formatter(ticker.NullFormatter())
    #ax.xaxis.set_minor_formatter( DateFormatter('%B') ) # full month name
    #ax.xaxis.set_minor_formatter( DateFormatter('%b') )  # abbreviated month name
    #ax.set_xlabel('Month in 2011')
    
    ax.grid(b=True,linewidth=0.3,color="0.1",alpha=0.6)
    for tick in ax.xaxis.get_minor_ticks():
        tick.tick1line.set_markersize(0)
        tick.tick2line.set_markersize(0)

for log in ['','log']:
    lloc = 2 # legend position upper left : 2 lower right : 4
    if log == 'log':
        logscale = True
        lloc = 4
    else:
        logscale = False   
    lstr = ''

    #################################################
    #
    #    daily integrated luminosity per fill
    #
    #################################################


    fig = plt.figure(figsize=(5,5))
    fig.subplots_adjust(left=0.15,bottom=0.14)
    ax = fig.add_subplot(111)

    ax.set_title('LHC '+str(YEAR)+' RUN (6.5 TeV/beam)')
    ax.set_xlabel('Fill number')
    ax.set_ylabel('Integrated luminosity per fill ('+lumi_unit+'$^{-1}$/fill)')

    if logscale:
        ax.set_yscale('log')
        lstr = '_log'

    for expt in ['ATLAS','CMS','LHCb','ALICE']:

        #(data, bins) = histOutline(luif[expt]/lumi_scale,luif['fill'])
        # workaround if array is only zero (it will crash)
        if max(luif[expt]) == 0.0:
            luif[expt] = luif[expt]+1e-20
            
        ax.bar(luif['fill'],luif[expt]/lumi_scale,
                color=color[expt],
                #linestyle='-',
                alpha=0.5,
                linewidth=liw[expt],
               align='center',
                label=expt)

        #ax.fill_between(luif['fill'], luif[expt]/lumi_scale, 9e-5,
        #ax.fill_between(bins,data, 9e-5,
        #                color=color[expt],alpha=0.25,)



    ax.set_xlim(None,int(L['fill'][-1])+2)
    ax.set_ylim(9e-5,None)
    
    l = ax.legend(loc=2,prop=prop,numpoints=1) # 2=upper left
    l.legendPatch.set_alpha(0.7)
    pre=offsetbox.TextArea("PRELIMINARY",textprops=dict(size='small'))
    box = l._legend_box
    box.get_children().append(pre)
    box.set_figure(box.figure)
    box.align="left"

    t1 = ax.text(0.,-0.16,genstr,transform=ax.transAxes,fontsize='xx-small')

    fig.savefig(LPC_FIG_DIR+'/'+species+'_luminosity_integrated_daily_fill_' + str(YEAR)+lstr+'.pdf'
                #,bbox_inches='tight'
                ,bbox_extra_artists=[t1])
    fig.savefig(LPC_FIG_DIR+'/png/'+species+'_luminosity_integrated_daily_fill_' + str(YEAR)+lstr+'.png'
                #,bbox_inches='tight'
                ,bbox_extra_artists=[t1])
    
    #################################################
    #
    #    daily integrated luminosity per day
    #
    #################################################


    fig = plt.figure(figsize=(5,5))
    fig.subplots_adjust(left=0.15,bottom=0.14)
    ax = fig.add_subplot(111)

    ax.set_title('LHC '+str(YEAR)+' RUN (6.5 TeV/beam)')
    #ax.set_xlabel('Week number')
    ax.set_ylabel('Integrated luminosity per day ('+lumi_unit+'$^{-1}$/day)')

    if logscale:
        ax.set_yscale('log')
        lstr = '_log'

    for expt in ['ATLAS','CMS','LHCb','ALICE']:

        ax.plot(luid['time'],luid[expt]/lumi_scale,
                color=color[expt],
                linestyle='-',
                alpha=0.9,
                linewidth=liw[expt],
                label=expt)

        ax.fill_between(luid['time'], luid[expt]/lumi_scale, 9e-5,
                        color=color[expt],alpha=0.25,)

    custom_date_axis_month(ax)
    ax.set_ylim(9e-5,None)
    
    l = ax.legend(loc=2,prop=prop,numpoints=1) # 2=upper left
    l.legendPatch.set_alpha(0.7)
    pre=offsetbox.TextArea("PRELIMINARY",textprops=dict(size='small'))
    box = l._legend_box
    box.get_children().append(pre)
    box.set_figure(box.figure)
    box.align="left"

    t1 = ax.text(0.,-0.16,genstr,transform=ax.transAxes,fontsize='xx-small')

    fig.savefig(LPC_FIG_DIR+'/'+species+'_luminosity_integrated_daily_date_' + str(YEAR)+lstr+'.pdf'
                #,bbox_inches='tight'
                ,bbox_extra_artists=[t1])
    fig.savefig(LPC_FIG_DIR+'/png/'+species+'_luminosity_integrated_daily_date_' + str(YEAR)+lstr+'.png'
                #,bbox_inches='tight'
                ,bbox_extra_artists=[t1])

    #################################################
    #
    #         integrated luminosity per fill
    #
    #################################################

    fig = plt.figure(figsize=(5,5))
    fig.subplots_adjust(left=0.15,bottom=0.14)
    ax = fig.add_subplot(111)

    ax.set_title('LHC '+str(YEAR)+' RUN (6.5 TeV/beam)')
    ax.set_xlabel('Fill number')
    ax.set_ylabel('Delivered integrated luminosity ('+lumi_unit+'$^{-1}$)')


    if logscale:
        ax.set_yscale('log')
        lstr = '_log'

    for expt in ['ATLAS','CMS','LHCb','ALICE']:
        # workaround if array is only zero (it will crash)
        # only needed at the beginning when some experiments have no data published yet
        if max(L[expt+'_lui']) == 0.0:
            L[expt+'_lui'] = L[expt+'_lui']+1e-10

        ax.errorbar(x=L['fill'][1:],
             y=L[expt+'_lui'][1:]/lumi_scale,
             marker=marker[expt],
             markersize=msz[expt],
             markeredgecolor=color[expt],
             markeredgewidth=mew[expt],
             color=color[expt],
             markerfacecolor=mfc[expt],
             alpha=1,
             linewidth=liw[expt],
             label=L[expt+'_label'],
             linestyle='-')

    ax.set_xlim(None,int(L['fill'][-1])+2)
    ax.set_ylim(0,None)

    t1 = ax.text(0.,-0.16,genstr,transform=ax.transAxes,fontsize='xx-small')

    l = ax.legend(loc=lloc,prop=prop,numpoints=1) # 1=upper right
    l.legendPatch.set_alpha(0.7)
    pre=offsetbox.TextArea("PRELIMINARY",textprops=dict(size='small'))
    box = l._legend_box
    box.get_children().append(pre)
    box.set_figure(box.figure)
    box.align="left"


    fig.savefig(LPC_FIG_DIR+'/'+species+'_luminosity_integrated_fills_' + str(YEAR)+lstr+'.pdf'
                #,bbox_inches='tight'
                ,bbox_extra_artists=[t1])
    fig.savefig(LPC_FIG_DIR+'/png/'+species+'_luminosity_integrated_fills_' + str(YEAR)+lstr+'.png'
                #,bbox_inches='tight'
                ,bbox_extra_artists=[t1])


    #################################################
    #
    #         integrated luminosity per date
    #
    #################################################

    fig = plt.figure(figsize=(5,5))
    fig.subplots_adjust(left=0.15,bottom=0.14)
    ax = fig.add_subplot(111)

    ax.set_title('LHC '+str(YEAR)+' RUN (1.38 TeV/beam)')
    #ax.set_xlabel('Week number')
    ax.set_ylabel('Delivered integrated luminosity ('+lumi_unit+'$^{-1}$)')


    if logscale:
        ax.set_yscale('log')
        lstr = '_log'

    """
    # use two entries: first for start fill, 2nd for end fill
    Ld = {'date':[],
         'ATLAS_lui':[],'CMS_lui':[],'LHCb_lui':[],'ALICE_lui':[],
         'ATLAS_plu':[],'CMS_plu':[],'LHCb_plu':[],'ALICE_plu':[]}

    for i in range(1,len(L['tstart'])):
        Ld['date'].append(L['tstart'][i])
        Ld['date'].append(L['tstop'][i])

        for expt in ['ATLAS','CMS','LHCb','ALICE']:
            for j in ('_lui','_plu'):
                Ld[expt+j].append(L[expt+j][i-1])
                Ld[expt+j].append(L[expt+j][i])

    for k in Ld.keys():
        Ld[k] = np.array(Ld[k])
    """


    for expt in ['ATLAS','CMS','LHCb','ALICE']:
        ax.errorbar(
             #x=Ld['date'],
             x=L['tstop'][1:],
             y=L[expt+'_lui'][1:]/lumi_scale,
             marker=marker[expt],
             markersize=msz[expt],
             markeredgecolor=color[expt],
             markeredgewidth=mew[expt],
             color=color[expt],
             markerfacecolor=mfc[expt],
             alpha=1,
             linewidth=liw[expt],
             label=L[expt+'_label'],
             linestyle='-')

        
    custom_date_axis_month(ax)
    ax.set_ylim(0,None)


    #ax.text(0.02,0.93,'PRELIMINARY $\pm$ 10%',transform=ax.transAxes)
    t1 = ax.text(0.,-0.16,genstr,transform=ax.transAxes,fontsize='xx-small')

    l = ax.legend(loc=lloc,prop=prop,numpoints=1) # 1=upper right
    l.legendPatch.set_alpha(0.7)
    pre=offsetbox.TextArea("PRELIMINARY",textprops=dict(size='small'))
    box = l._legend_box
    box.get_children().append(pre)
    box.set_figure(box.figure)
    box.align="left"


    fig.savefig(LPC_FIG_DIR+'/'+species+'_luminosity_integrated_date_' + str(YEAR)+lstr+'.pdf'
                #,bbox_inches='tight'
                ,bbox_extra_artists=[t1])
    fig.savefig(LPC_FIG_DIR+'/png/'+species+'_luminosity_integrated_date_' + str(YEAR)+lstr+'.png'
                #,bbox_inches='tight'
                ,bbox_extra_artists=[t1])

    #################################################
    #
    #         peak luminosity per fill
    #
    #################################################

    om = 32  # order of magnitude e.g. lumi = x10^32
    conv = 1/100. # convertion from ub (1 ub = 10^-30 cm^2)

    fig = plt.figure(figsize=(5,5))
    fig.subplots_adjust(left=0.15,bottom=0.14)
    ax = fig.add_subplot(111)

    ax.set_title('LHC '+str(YEAR)+' RUN (6.5 TeV/beam)')
    ax.set_xlabel('fill number')
    ax.set_ylabel(r'Peak luminosity ($10^{'+str(om)+'}$ cm$^{-2}$s$^{-1}$)')


    if logscale:
        ax.set_yscale('log')
        lstr = '_log'

    for expt in ['ATLAS','CMS','LHCb','ALICE']:
        # workaround if array is only zero (it will crash)
        # only needed at the beginning when some experiments have no data published yet
        if max(L[expt+'_plu']) == 0.0:
            L[expt+'_plu'] = L[expt+'_plu']+1e-10
            
        ax.errorbar(x=L['fill'][1:],
             y=L[expt+'_plu'][1:]*conv,
             marker=marker[expt],
             markersize=6,
             markeredgecolor=color[expt],
             markeredgewidth=mew[expt],
             color=color[expt],
             markerfacecolor=mfc[expt],
             alpha=1,
             linewidth=liw[expt],
             label=expt,
             linestyle='')

    ax.set_xlim(None,int(L['fill'][-1])+2)
    ax.set_ylim(0,None)
    t1 = ax.text(0.,-0.16,genstr,transform=ax.transAxes,fontsize='xx-small')



    l = ax.legend(loc=lloc,prop=prop,numpoints=1) # loc=1: upper right
    l.legendPatch.set_alpha(0.7)
    pre=offsetbox.TextArea("PRELIMINARY",textprops=dict(size='small'))
    box = l._legend_box
    box.get_children().append(pre)
    box.set_figure(box.figure)
    box.align="left"

    fig.savefig(LPC_FIG_DIR+'/'+species+'_luminosity_peak_fills_' + str(YEAR)+lstr+'.pdf'
                #,bbox_inches='tight'
                ,bbox_extra_artists=[t1])
    fig.savefig(LPC_FIG_DIR+'/png/'+species+'_luminosity_peak_fills_' + str(YEAR)+lstr+'.png'
                #,bbox_inches='tight'
                ,bbox_extra_artists=[t1])


    #################################################
    #
    #         peak luminosity per date
    #
    #################################################

    om = 32  # order of magnitude e.g. lumi = x10^32
    conv = 1/100. # convertion from ub (1 ub = 10^-30 cm^2)

    fig = plt.figure(figsize=(5,5))
    fig.subplots_adjust(left=0.15,bottom=0.14)
    ax = fig.add_subplot(111)

    ax.set_title('LHC '+str(YEAR)+' RUN (6.5 TeV/beam)')
    #ax.set_xlabel('Week number')
    ax.set_ylabel(r'Peak luminosity ($10^{'+str(om)+'}$ cm$^{-2}$s$^{-1}$)')


    if logscale:
        ax.set_yscale('log')
        lstr = '_log'

    for expt in ['ATLAS','CMS','LHCb','ALICE']:
        ax.errorbar(x=L['tstop'][1:],
             y=L[expt+'_plu'][1:]*conv,
             marker=marker[expt],
             markersize=6,
             markeredgecolor=color[expt],
             markeredgewidth=mew[expt],
             color=color[expt],
             markerfacecolor=mfc[expt],
             alpha=1,
             linewidth=liw[expt],
             label=expt,
             linestyle='')


    custom_date_axis_month(ax)
    ax.set_ylim(0,None)

    t1 = ax.text(0.,-0.16,genstr,transform=ax.transAxes,fontsize='xx-small')


    l = ax.legend(loc=lloc,prop=prop,numpoints=1) # loc=1: upper right
    l.legendPatch.set_alpha(0.7)
    pre=offsetbox.TextArea("PRELIMINARY",textprops=dict(size='small'))
    box = l._legend_box
    box.get_children().append(pre)
    box.set_figure(box.figure)
    box.align="left"

    fig.savefig(LPC_FIG_DIR+'/'+species+'_luminosity_peak_date_' + str(YEAR)+lstr+'.pdf'
                #,bbox_inches='tight'
                ,bbox_extra_artists=[t1])
    fig.savefig(LPC_FIG_DIR+'/png/'+species+'_luminosity_peak_date_' + str(YEAR)+lstr+'.png'
                #,bbox_inches='tight'
                ,bbox_extra_artists=[t1])

######################################################
#
# generate txt file with the data used in the plots
#
######################################################
txtfile = LPC_FIG_DIR+'/png/luminosity_data_ions.txt'
f = open(txtfile, 'w')

f.write('# lui = integrated luminosity in 1/ub; plu = peak luminosity in Hz/ub; time = UNIX UTC time; file generated '+time.strftime("%a, %d %b %Y %H:%M:%S",time.gmtime())+' UTC\n')
f.write('# fill  tstart  tstop  ATLAS_lui  CMS_lui  LHCb_lui  ALICE_lui  ATLAS_plu  CMS_plu  LHCb_plu  ALICE_plu\n')

sline = ['fill','tstart','tstop','ATLAS_lui','CMS_lui','LHCb_lui','ALICE_lui','ATLAS_plu','CMS_plu','LHCb_plu','ALICE_plu']
sformat = '%5d %11d %11d %12d %12d %12d %12d %8.3e %8.3e %8.3e %8.3e\n'

for i in range(1,len(L['fill'])):
    line = sformat % (L['fill'][i]
                      ,num2epoch(L['tstart'][i])
                      ,num2epoch(L['tstop'][i])
                      ,L['ATLAS_lui'][i]
                      ,L['CMS_lui'][i]
                      ,L['LHCb_lui'][i]
                      ,L['ALICE_lui'][i]
                      ,L['ATLAS_plu'][i]
                      ,L['CMS_plu'][i]
                      ,L['LHCb_plu'][i]
                      ,L['ALICE_plu'][i]
                      )

    f.write(line)
    
f.close()

"""
# LHCb
for i in range(1,len(L['fill'])):
    line = '%d %9d %9d' % (L['fill'][i]
                      ,L['LHCb_luif'][i]/1000.
                      ,L['LHCb_lui'][i]/1000.
                      )

    print line
"""

logstr += 'Plots for pp lumi are updated in "' + LPC_FIG_DIR + '\n'
logger.log( logstr )
