import os
import os.path
import glob
import re
import fcntl
import threading
from FileModel import FileModel
import pprint
import json
from shutil import move,Error

IDFILE         = "docId"
DOCTREEVERFILE = "docTreeVer"
LOCKFILE       = "lock"
WASTEDIR       = "wastebasket"

pp = pprint.PrettyPrinter( indent=2 )

class NoteFileModel( FileModel ):

    def __init__( self, repoPath ):
        FileModel.__init__( self, repoPath )


    def getGantt( self, params ):
        res = self.initres()
        tag = params['data']['tag'].strip()
        res['data']['records'] = [ { 'action' : "displayGantt",
                                     'tag' : tag } ]; 

#        pat = re.compile( tag+"(?:\s|(?:&nbsp;))*(\d\d\d\d-\d\d-\d\d)?(?:\s|(?:&nbsp;))*(.*?)(\.|(</p>)|(<br)|(</li))", re.MULTILINE ) 
        pat = re.compile( tag+"(.*?)((</p>)|(<br)|(</li))", re.MULTILINE ) 

        ganttdata = { 'catlist' : {},
                      'entries' : [] }
        for docId,doc in self.DocumentH.iteritems():
            if doc['type'] != 'd' :
                continue
            filepath = self.getDocPath( docId )            
            f = open( filepath )
            contents = f.read()
            #print contents
            f.close()
            nlist = pat.findall( contents )
            for it in nlist :
                hit = it[0]
                #print hit
                self.analyzeHit( docId ,hit, ganttdata )

        res['data']['records'][0]['record'] = ganttdata

        #pp.pprint( res['data']['records'][0]['record'] )
        return res


# @CMSTAG [topic] 2014-03-01 2014-03-20 {Install Pixel} Pixel and BRM/PLT/BCM installation.
    def analyzeHit( self, docId, hit, res ):
        hit = re.sub("&nbsp;", " ", hit)
        hit = re.sub("\s+", " ", hit).strip()

        # find start and end-data
        topicpat = "\[([^\:\]]*):?(\#[^\:\]\s]+){0,1}:?(-?[\d]+){0,1}\s*\]"
        datepat = "(\d{4})-(\d{1,2})-(\d{1,2})"
        shortpat = "\{(.*?)\}"

        entry = { 'dates' : []}
        
        m = re.search( topicpat, hit )
        if m:
            topic = m.group(1).strip()

            if topic in res['catlist']:
                res['catlist'][topic]['count'] += 1
            else :
                res['catlist'][topic] = {'count': 1}
            entry['cat'] = topic

            if m.group(2):
                color = m.group(2)
                res['catlist'][topic]['color'] = color

            if m.group(3):
                res['catlist'][topic]['seqno'] = int(m.group(3))
            else:
                if not  'seqno' in res['catlist'][topic]:
                    res['catlist'][topic]['seqno'] = -1 # will go in last position

        for m in re.finditer( datepat, hit ):

            entry['dates'].append( m.group(0) )

#        if len(entry['dates']) == 1:
#            entry['dates'].append( entry['dates'][0] )

        m = re.search( shortpat, hit )
        if m:

            entry['shorttxt'] = m.group(1).strip()

        hit = re.sub( topicpat,"",hit)
        hit = re.sub( datepat, "", hit)
        longtxt = re.sub( shortpat, "", hit).strip()


        entry['longtxt'] = longtxt
        if 'shorttxt' not in entry:
            entry['shorttxt'] = longtxt

        entry['docId'] = docId

        res['entries'].append( entry )

        return res
