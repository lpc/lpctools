#!/usr/bin/python
import os
import os.path
import readconfig
import subprocess
import shlex
import re
from lpcutils import Logger, Mailer, getsrcdst

config = readconfig.readconfig( "lumi_config.json" )

logger = Logger( config["logfile"] )

dstbase = os.path.join( config['workBaseDir'], str(config["year"]), 'measurements' )

logger.log( "Copying massi files from " + dstbase )

EXPERIMENTS = [ "ATLAS", "CMS", "LHCb", "ALICE" ]

###########################################################

def makeExclusion( firstFill, ext ):

    excl = []
    tmp = map( int, str(int(firstFill) - 1) )
    pref = ""
    for d in tmp:
        for i in range(0,d):
            excl.append( "--exclude=" + pref + str(i)+"*" + ext )
        pref += str(d)

    return ' '.join( excl )

###########################################################

def makeExclusionLast( lastFill, ext ):

    #print lastFill
    excl = []
    tmp = map( int, str(lastFill) ) # tmp contains the digits
    pref = ""
    for d in tmp:
        for i in range(d+1,10):
            excl.append( "--exclude=" + pref + str(i)+"*" + ext )
        pref += str(d)

    # make a pattern for fills with one more digit:
    ndigit = len(tmp) + 1 
    lpat = ""
    for i in range(0,ndigit):
        lpat += "?"
    lpat += "*" + ext
    excl.append( "--exclude=" + lpat)
    return ' '.join( excl )

###########################################################
# The string returned by this script is parsed by other scripts.
# In case you change it without changing the other scritps the 
# automatic Massi file processing breaks.
returnstr = ""
for exp in EXPERIMENTS:
#    if exp=="ATLAS": continue 
    logger.log("Looking for new MASSI files from "+exp)
    extraoptions = " "

    selection = config["dataFormat"][exp]

    f1 = int(config["firstFill"])
    f2 = int(config["firstFillIon"])

    firstFill = min( f1, f2 )
    if f1 == 0:
        firstFill = f2
    elif f2 == 0:
        firstFill = f1


    l1 = int(config["lastFill"])
    l2 = int(config["lastFillIon"])
    lastFill = max( l1,l2 )

    excl = makeExclusion( firstFill, selection )

    if ( "lastFill" in config ):
        excl += " " + makeExclusionLast( lastFill, selection )

    wildcard = "/*"

    if re.match( r"\..{3}", selection ):
        wildcard +=  selection

    srcdst = getsrcdst( config, exp )

    for k,val in srcdst.iteritems():
        src = val[0]
        dst = val[1]

        #print val
        if exp == "ATLAS":
            extraoptions = " --checksum "
        command = "rsync -rtzv --out-format='--> %n (%b bytes)' " + extraoptions + excl + " " + src + wildcard + " " + dst

        logger.log( command )

        (out,err) = subprocess.Popen( command, stdout=subprocess.PIPE, shell=True ).communicate()
        #for testing:(out,err) = ("","")

        if err:
            returnstr +=  "PROBLEM --> an error during rsync with " + exp + "\n" + err + "\n"
            logger.log( returnstr )
            continue
    
        fileType=dst.split("/")[-1]  # a bit hackish
        
        updatecnt = 0
        fillstr = ""
        for line in out.splitlines():
            if re.match(r'^-->',line):
                updatecnt += 1
                fillstr += line + "\n"
                if "lumi" in fileType:
                    # archive to nxcals through interface from michi
                    fileName=os.path.join(dst,line.split()[1])
                    cmd="curl -v http://cs-ccr-lhcswgpn:4242/update/massi"+fileType+" -F file=@"+fileName
                    logger.log(cmd)
                    (outNXCals,errNXCals) = subprocess.Popen( cmd, stdout=subprocess.PIPE, shell=True ).communicate()
                    if errNXCals:
                        logger.log("ERROR in upload: "+errNXCals)
                        returnstr +=  "PROBLEM --> an error during upload to NXCals with " + exp + "\n" + errNXCals + "\n"

        if updatecnt > 0:
            returnstr +=  exp + " " + str(updatecnt) + "\n" + fillstr
            logger.log( out )

if returnstr :
    msg = logger.log( returnstr )
    mailer = Mailer( logger, "/afs/cern.ch/user/l/lpc/tools/mailcredentials.txt" )
    mailer.sendMail( "Massi Files updated", msg )
    print msg
