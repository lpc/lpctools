#!/usr/bin/python
import readconfig
import os
import csv
import json
from pprint import pprint
from datetime import datetime, timedelta

from lpcutils import Logger

config = readconfig.readconfig( "lumi_config.json" )

logger = Logger( config['logfile'] )

YEAR = config['year']

logger.log( "Processing Z-counting data for " + YEAR ) 


WORKBASE = config['workBaseDir']
WEBDIR = os.path.join( config['webBaseDir'],YEAR )
datadir = os.path.join( WORKBASE, YEAR, 'measurements' )

resultFilename = "zCountingData.json"

inputname="/afs/cern.ch/user/l/lumipro/public/ZRatesFiles/%sRates/ATLAS_CMS_ZRatiosPerFill_%s_v1.csv" % (YEAR,YEAR)
result = {}
with open(inputname) as datafile:
    dreader = csv.DictReader( datafile )
    for row in dreader:
        fill = row['Fill']
        if int(fill) in config['ignoreForZCount']: continue
        zrat = float(row['Z-Ratio'])
        zerr = float(row['Z-Ratio uncertainty'])
        result[fill] = { 'fillno' : fill,
                         'rat' : zrat,
                         'err' : zerr }

resultPath = os.path.join( WEBDIR, resultFilename )
fd = open( resultPath, 'w' )
json.dump(result, fd )
fd.close()

