import urllib
import json
from werkzeug.wrappers import Request

from logger import debug, error, log

class ActionHandler :

    def __init__( self ):
        self.registry = { 'noaction' : self }
        
######################################################        

    def register( self, action, actionHandler ):
        #print "registering action " + action
        self.registry[action] = actionHandler


######################################################
    def handleAction( self, action, params ):
        #debug("noaction to be handled")
        return None

    def handleRequest( self, request ):
        params = request.values.to_dict()
        action = params['action']
        # in Werkzeig arguments are assumed to be name value pairs where the value is 
        # a simple type as a string or an integer but now JSON objects. Objects have
        # to be stringified at the client side and here re-loaded into an python object.
        if ( 'data' in params ):
            params['data'] = json.loads( params['data'] )

        if action in self.registry:            
            ret =  self.registry[ action ].handleAction( params )
            return ret
        else :
            res = { 'error' : "Server error: No action registered for \"" + str(action) + "\"!" };            
            return json.dumps( res )
        return


