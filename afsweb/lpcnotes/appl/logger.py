import string

debugmsg = ""
logmsg = ""
errormsg = ""

def flushmsg():
    global debugmsg
    global logmsg
    global errormsg
    res = ""
    res = "<script>$('#debug').append('" + str(debugmsg) + "');</script>\n"
    res += "<script>$('#log').html('" + str(logmsg) + "');</script>\n"
    res += "<script>$('#error').html('" + str(errormsg) + "');</script>\n"
    debugmsg = ""
    errormsg = ""
    logmsg = ""
    return res

def debug( msg ):
    global debugmsg
    debugmsg += string.replace( msg, "'","\\'") + "<br>"

def log( msg ):
    global logmsg
    logmsg += string.replace( msg, "'","\\'") + "<br>"

def error( msg ):
    global errormsg
    errormsg += string.replace( msg, "'","\\'") + "<br>"

