import re
import urllib
from werkzeug.wrappers import Request
import json

from logger import debug, error, log

class DocumentHandler :

############################################################################

    def __init__( self, model, actionHandler ):
        """The class registers with the actionHandler various basic operations
        which should always be possible for documents. Those commands which 
        have not -Category or -Document postfix are applicable to Categories
        and Documents."""
        actionHandler.register( "readDocument", self )
        actionHandler.register( "newDocument", self )
        actionHandler.register( "rescanDocTree", self )
        actionHandler.register( "syncDocument", self )
        actionHandler.register( "newCategory", self )
        actionHandler.register( "tagList", self )
        actionHandler.register( "deleteDocument", self )
        actionHandler.register( "moveTo", self )
        actionHandler.register( "rename", self )
        self.model = model;
        

###########################################################################        

    def handleAction( self, params ):
        """ This function handles the actions for which this class registered
        with the ActionHandler in the constructor. These actions actually 
        operate on the data represented by this class."""

        model = self.model
        action = params['action']
        # execute the registered function
        #print "action is ", action
        res = getattr(self.model, action)( params )
        #print "returning ", json.dumps( res )
        return json.dumps( res )

############################################################################
#
#    def printDocumentActions( self ):
#        """Generate the html for the basic operations to creat, rename, or delete
#        a document or a directory (=category)."""
#
#        res = 'Name:<input type="text" name="newDocument" id="newDocument" value="" />'
#        res += ' Directory:<select name="directory" id="directory">'
#        for diry in self.getDocumentDropDown():
#            res += '<option value="'+diry['directory']+'">'+diry['label']+'</option>'
#        res += '</select>'
#        res += '<input type="button" onclick="insertNewDocument()" value="new topic"/>'
#        res += '<input type="button" onclick="insertNewDir()" value="new directory"/>'
#        res += '<input type="button" onclick="moveTo()" value="move to directory"/>'
#        res += '<input type="button" onclick="rename()" value="rename"/>'
#        return res
#
#############################################################################
#
#
#    def getJqueryUIMenu( self ):
#        """ Generate a menu compatible with jquery-ui (all vertical)"""
#
#        documenttree = self.model.documents()
#        res = '<ul id="menu">\n'
#        res = self.doJUEntry( documenttree, res, -1)
#        res += '</ul>\n'
#        return res
#
#    # called with dir entry in tree
#    def doJUEntry( self, entry, res, level ):
#        level += 1
#        if level > 0:
#            addon = ''
#        else:
#            addon = ' id="menu"'
#        res += '<ul' + addon + '>\n'
#        for child in entry['children']:
#            res += '<li><a href="#">' + child['dirname'] +'></a>\n'
#            res = self.doEntry( child, res, level )
#            res += '</li>\n'
#        level -= 1
##        for filen in entry['files']:
##            res += '<li class="file" onclick="readDocument(\'' + filen['filepath'] + '\')">'+filen['documentname']+'</li>\n'
#        res += '</ul>\n'
#        return res
