##
# @author Colin Barschel (colin.barschel@cern.ch)
# @file plotting class for typical LPC data sets
#
from mytools import *
from scipy import optimize
from datetime import datetime
import numpy as np
import scipy as sp
import pylab as P
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.ticker import ScalarFormatter, FormatStrFormatter
from time import mktime
from matplotlib.dates import *
import string, sys
from cfile import *
from cdata import *
from cfill import *

class FixedOrderFormatter(ScalarFormatter):
    """Formats axis ticks using scientific notation with a constant order of 
    magnitude"""
    def __init__(self, order_of_mag=0, useOffset=True, useMathText=True):
        self._order_of_mag = order_of_mag
        ScalarFormatter.__init__(self, useOffset=useOffset, 
                                 useMathText=useMathText)
    def _set_orderOfMagnitude(self, range):
        """Over-riding this to avoid having orderOfMagnitude reset elsewhere"""
        self.orderOfMagnitude = self._order_of_mag

## plot one data set to the given axes ax (see it as a canvas)
# call plot() multiple times to compare the experiments
class cplot():
    color = {'ATLAS':'k','CMS':'g','LHCb':'b','ALICE':'r'}
    mew = {'ATLAS':1,'CMS':0.5,'LHCb':0.5,'ALICE':0.5} # markeredgewidth
    mfc = {'ATLAS':'0.5','CMS':'0.8','LHCb':'0.9','ALICE':'1'} # markerfacecolor
    marker = {'ATLAS':'o','CMS':'^','LHCb':'D','ALICE':'s'}
    ylabel = {'l':'Lumi. (Hz/${\mu}$b)'
              ,'sl':'Specific Lumi. (Hz/${\mu}$b)'
              ,'il':'Int. Lumi. (${\mu}$b$^{-1}$)'
              ,'x':'X (mm)'
              ,'y':'Y (mm)'
              ,'z':'Z (mm)'
              ,'x_dev':'X - average (mm)'
              ,'y_dev':'Y - average (mm)'
              ,'z_dev':'Z - average (mm)'
              ,'xsu':'$\sigma_X$ (mm)'
              ,'ysu':'$\sigma_Y$ (mm)'
              ,'zsu':'$\sigma_Z$ (mm)'
              ,'ax':r'$\theta$ x ($\mu$rad)'
              ,'ay':r'$\theta$ y ($\mu$rad)'
              ,'e_x':'$\epsilon_X$ (mm)'
              ,'e_y':'$\epsilon_Y$ (mm)'
              ,'elum':'$\epsilon$ (mm)'
              }
    title = {'l':'Luminosity'
             ,'sl':'Specific Luminosity'
             ,'il':'Integrated Luminosity'
              ,'x':'IP position'
              ,'y':'IP position'
              ,'z':'IP position'
              ,'x_dev':'Position dev'
              ,'y_dev':'Position dev'
              ,'z_dev':'Position dev'
              ,'xsu':'Beam spot width'
              ,'ysu':'Beam spot width'
              ,'zsu':'Beam spot width'
              ,'ax':'Half crossing angle'
              ,'ay':'Half crossing angle'
              ,'e_x':'Emittance X'
              ,'e_y':'Emittance Y'
              ,'elum':'Emittance'
    }
    limits = {'l':(0,None)
              ,'sl':(0,1e-21)
              ,'x':(-1.5,1.5)
              ,'y':(-1.5,1.5)
              ,'z':(-30,30)
              }
    
    def __init__(self,ax):
        self.ax = ax
    
    ## plot one data set
    # @param D the data object (cdata)
    # @param x the x values to plot (string like x,xsu,l...)
    # @param F the fill object (cfill)
    # @param label (if=='' take the experiment name as label)
    # @param what tuple with boolean: what legend to plot: title, xlabel, ylabel, legend
    def plot(self,D,x,label='',what=(True,True,True,True),ylim=(None,None)):
        if len(D.d['time']) == 0:
            return

        
        
        xerr = 'd'+x
        if label == '':
            label = D.expt

        scale = 1 # axis scale if unit convertion needed
        if x == 'ax' or x == 'ay':
            scale = 1e6

        # selection index. In principle we'd like to plot everything
        # but with idx index it is possible to exclude data points
        idx = range(len(D.d['time']))
        prop = mpl.font_manager.FontProperties(size='x-small')

        xavg = D.d[x+'_avg']
        xstd = D.d[x+'_std']
        val,val_err = fe(xavg*scale,xstd*scale,1)
        ylabelstr = self.ylabel[x]+'='+val+'$\pm$'+val_err
        preylabel = ''

        if x == 'il':
            ylabelstr = self.ylabel[x]+':'+f(D.d[x][-1],3)
        if x == 'l':
            ylabelstr = ''
        if x == 'sl':
            ylabelstr = ''
            # select data points with stab > 0 only
            idx = []
            for i in range(len(D.d['time'])):
                if D.d['stab'][i] > 0 and D.d[x][i] < 1e-21:
                    idx.append(i)
        if x == 'ax' or x == 'ay':
            preylabel = D.ftype+' '
            ylabelstr = D.ftype+' '+ylabelstr

                    
        if x[1:] == '_dev':
            valx,valx_err = fe(D.d[x[0]+'_avg'],D.d[x[0]+'_std'],1)
            ylabelstr = self.ylabel[x][0]+'='+valx+'$\pm$'+valx_err+' mm'
        
        print 'plot',x,xerr,label,val,val_err,ylabelstr
        
        # x and xerr is a string to define the axis. e.g. l, sl, x, xs etc.
        # see class cdata
        
        #ax.set_ylim(-0.38,0.05)
        #sb = F.sb()
        #sbstr = str(sb[0])+'.'+str(sb[1])+'.'+str(sb[2])+' '+str(sb[3])+':'+str(sb[4]) # time string

        self.ax.grid(True)
        #self.ax.set_xlim(-0.5,F.sbGps()[1]+0.5)
        #tend = F.sbGps()[1]+0.5
        #if max(D.d['time']) > tend*3600 + F.sbGps()[0]:
        #    tend = max(D.d['time']-F.sbGps()[0])/3600 + 0.5
        #self.ax.set_xlim(-0.5,None)
        #self.ax.yaxis.set_label_coords(-0.16, 0.5)

        tstart = D.d['time'][0]
        ttuple = gpsTimeTuple(tstart-toffset)
        tstartstr = str(ttuple[0])+'/'+str(ttuple[1])+'/'+str(ttuple[2])
        #print ttuple
        #self.ax.errorbar((D.d['time'])/3600,
        # convert unix time to mpl num time
        # x=mpl.dates.date2num([datetime.fromtimestamp(ts) for ts in D.d['time']])
        #self.ax.errorbar(x=mpl.dates.date2num([datetime.fromtimestamp(ts) for ts in D.d['time']]),
        #self.ax.errorbar(((D.d['time']-tstart)/3600),
        #ndates = mpl.dates.date2num([datetime.fromtimestamp(ts) for ts in D.d['time']])
        ndates = epoch2num(D.d['time']) # num date is UTC per default
        self.ax.errorbar(x=ndates[idx],
                     y=D.d[x][idx]*scale,
                     yerr=D.d[xerr][idx]*scale,
                     marker=self.marker[D.expt],
                     markersize=4,
                     markeredgecolor=self.color[D.expt],
                     markeredgewidth=self.mew[D.expt],
                     color=self.color[D.expt],
                     markerfacecolor=self.mfc[D.expt],
                     alpha=0.8,
                     linewidth=0.5,
                     label=label+' '+ylabelstr,
                     linestyle='')
        
        if what[0]:
            self.ax.set_title(self.title[x]+' fill '+str(D.fillnr))
        if what[1]:
            self.ax.set_xlabel('UTC Time ('+tstartstr+')')
        if what[2]:
            self.ax.set_ylabel(preylabel+self.ylabel[x])

        self.ax.xaxis_date()
        #hourLoc = mpl.dates.HourLocator(byhour=range(24),interval=1)
        
        self.ax.xaxis.set_major_locator(HourLocator(byhour=range(24)))
        self.ax.xaxis.set_major_formatter( DateFormatter('%H') )
        
        #self.ax.xaxis.set_major_formatter( HourLocator(arange(0,25,6)))
        #self.ax.xaxis.set_major_formatter( DayLocator())
        #minuteLoc = mpl.dates.MinuteLocator(byminute=arange(0,61,15),interval=15)
        #self.ax.xaxis.set_minor_locator(MinuteLocator(byminute=arange(0,61,15),interval=15))
        
        self.ax.xaxis.set_minor_locator(MinuteLocator(byminute=arange(0,61,15)))
        


        """ set limits """
        print 'limits now and given:',self.ax.get_ylim(),ylim
        ymin_n,ymax_n = self.ax.get_ylim() # plot limits as of now
        ymin,ymax = ylim               # plot limits given as arg

        # problems on x time axis when only one data point. T interval huge
        tmin,tmax = self.ax.xaxis.get_view_interval()
        if tmax-tmin > 3*24:
            tmin = min(ndates[idx])-(24/3600.)
            tmax = max(ndates[idx])+(24/3600.)
            self.ax.set_xlim(tmin,tmax)
            


        """
        if ymin < ymin_n:
            ymin = ymin - (ymax_n-ymin)*0.1
        if ymax > ymax_n:
            ymax = ymax + (ymax-ymin_n)*0.1
        """

        if (x=='il' or x=='l' or x=='sl'):
            # force scale at 0
            ymin = 0

            print x,ymax,ymax_n
            if x=='sl':# and ymax_n > 1e-21:
                ymax = None
                if np.average(D.d[x][idx][0:20]) > ymax_n*0.9:
                    ymax = np.average(D.d[x][idx][0:20])*1.2
                    
            if x=='il' and max(D.d[x][idx]) > ymax_n:
                ymax = max(D.d[x][idx])*1.1
            #else:
            #    ymax = None

            #if x=='il':
            #    ymax = None
            #    ymax = np.average(D.d[x][idx][0:20])*0.2
            #    print x,ymax,ymax_n
                #if ymax < ymax_n and ymax_n < 1e-21:
                #ymax = ymax_n
                
            # om is order of magnitude
            # flist(0.0000012,1) = ('0.000001', 6, True, -6)
            # make sure a value of 0 don't mess up the scale
            n,m,o,om = flist(max(D.d[x][idx]),1) # see mytools.py
            if om > 3 or om <-1 and float(n) != 0:
                self.ax.yaxis.set_major_formatter(FixedOrderFormatter(om))
            #if n != 0:
            #    self.ax.set_ylim(0, float(n)*1.1)
            #self.ax.set_ylim(0, 0)

            # increase y limit of axis if needed
            #ymin, ymax = self.ax.get_ylim()
            print om,ymin, ymax,x,label,'max',max(D.d[x][idx])

            
            """
            if ymax < max(D.d[x]):
                self.ax.set_ylim(0, max(D.d[x])*1.1)
            else:
                self.ax.set_ylim(0,None)
            """
            #self.ax.set_ylim(0, max(D.d[x])*1.1)
            #else:
            #    self.ax.set_ylim(0, ymax)

            
                
        else:
            self.ax.yaxis.set_major_formatter(ScalarFormatter(useMathText=True))

        self.ax.set_ylim(ymin,ymax)

        # zero line
        dmin, dmax = self.ax.yaxis.get_view_interval()
        if dmax > 0 and dmin < 0:
            self.ax.axhline(y=0, color='0.25',linewidth=0.5)

        # at the end print legend is requested
        if what[3]:
            l = self.ax.legend(loc=1,prop=prop,
                               numpoints=1,
                               labelspacing=0.,
                               handletextpad=0.1,
                               borderaxespad=0) 
            l.legendPatch.set_alpha(0.75)
            
            ymin, ymax = self.ax.get_ylim()
            ymax = ymax + (ymax - ymin)* 0.2
            self.ax.set_ylim(ymin,ymax)
        

    """ write PRELIMINARY at lower left corner """
    def preliminary(self):
        self.ax.text(0.02,0.03,'PRELIMINARY',transform=self.ax.transAxes)
        
## Test the cfile class
if __name__ == "__main__":
    # plot a single data set
    # using cplot class
    if len(sys.argv) > 2:
        expt = sys.argv[1]
        fillnr = int(sys.argv[2])
        bucket = int(sys.argv[3])
        ptype = sys.argv[4]
        x = sys.argv[5]
        
    else:
        print "Enter expt fillnr bucket plotType as argument"
        sys.exit(1)
    
    #ptype = 'l'
    #x = 'il'
    
    D = cdata(expt,fillnr,ptype,bucket)
    print D.ff(ptype)
    if D.exist():
        print 'file exist'
        D.read()
        if D.ok:
            D.tavg(dt=300)
            fig = plt.figure(1)
            ax = fig.add_subplot(111)
            #fig.subplots_adjust(left=0.1, wspace=0.6)
        
            pl = cplot(ax)
            pl.plot(D,x)
        else:
            print "File not OK, empty"
    else:
        print 'file does not exists'
