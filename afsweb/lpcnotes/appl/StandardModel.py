import pprint

class StandardModel(object):

    def __init__( self ):
        self.initDocTree()

    def initDocTree( self ):
        # make the root node (no parent)
        root = { 'name'     : "",
                 'id'       : 0,
                 'version'  : 1,
                 'catId'    : None,
                 'type'     : 'c',
                 'children' : []}
        self.Documents = root
        self.treeVer = 0
        self.DocumentH = { 0 : root}

    def initres( self ):
        res = { 'debug' : "",
                'info'  : "",
                'warn'  : "",
                'error' : "",
                'state' : "",
                'data'  : {'records' : []}
                }
        return res


    def docTreeVerHandler( self, new = 0 ):
        if ( new ):
            self.treeVer = self.treeVer + 1
        return self.treeVer

    def updateDocTree( self, params, res = None ):
        if res == None:
            res = self.initres()
        res['data']['records'].append( { 'action'  : 'updateDocTree',
                                         'record'  : self.Documents,
                                         'treeVer' : self.treeVer } )
        res['state'] = "updated Document Tree"

        return res


    def newDocument( self, params ):
        '''Makes new documents and categories. Expects in the data record a 
        type key of which the value 'd' indicates a new document and 'c' a new 
        category.'''
        res       = self.initres()
        data     = params['data']
        catId    = int(data['catId'])
        docName  = data['docName']
        dtype    = data['type']
        newId    = self.newId()
        res['debug'] = "New doc: " + docName 
        res['data']['version'] = 1
        res['state'] = "inserted new document"
        res['newId'] = newId
        ndoc = { 'name'     : docName,
                 'id'       : newId,
                 'version'  : 1,
                 'catId'    : catId,                 
                 'type'     : dtype,
                 'children' : [] }

        self.DocumentH[catId]['children'].append( ndoc )
        self.DocumentH[newId] = ndoc
        self.docTreeVerHandler( 1 )
        return res

    def deleteDocument( self, params ):
        res = self.initres()
        docId = int(params['data']['docId'])
        doc = self.DocumentH[ docId ];
        catId = doc['catId']
        if catId == None:
            res['error'] = "You cannot delete the document root node!"
            return res
        children = self.DocumentH[ catId ]['children']
        for ch in children:
            if ch['id'] == docId:
                children.remove( ch )
                #res['debug'] = "Removed document ", ch['name']
                self.docTreeVerHandler( 1 )
                return res
        res['warn'] = "No document removed"
        return res
        
        
    def rename( self, params ):
        docId = params['data']['docId']
        newName = params['data']['docName'].strip()
        doc = self.DocumentH[ docId ]
        doc['name'] = newName
        self.docTreeVerHandler( 1 )
        res = self.updateDocTree( params )
        res['state'] = "Document renamed"
        return res


    # what happens when user1 moves it and user2 has posted an update? shortly after?
    # documents are identified by their unique docId
    # moving/renaming documents results in a new document tree. This should be propagated
    #    during the sync. 
    def moveTo( self, params ):
        docId = int(params['data']['docId'])
        newCatId = int(params['data']['catId'])
        doc = self.DocumentH[ docId ];
        parent = self.DocumentH[ doc['catId'] ]
        for ch in parent['children']:
            if ch['id'] == docId:
                parent['children'].remove( ch )
                break
        self.DocumentH[ newCatId ]['children'].append( doc )
        doc['catId'] = newCatId
        self.docTreeVerHandler( 1 )
        return  self.updateDocTree( params )

#############################


#    def sync( self, params ):
#        res = self.initres()
#        res['debug'] = "Server: sync command " + params['action']
#        print "Standard Model returning " + repr(res)
#        return res

    def readDocument( self, params ) :
        res = self.initres()
        document = params['document']
        res['debug'] = "readDocument " + document
        return res

#### dummy
    def syncDocument( self, params ):
        res = self.initres()
        docId = params['data']['docId']        
        return res;
