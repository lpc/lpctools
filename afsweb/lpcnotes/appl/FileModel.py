import os
import os.path
import glob
import re
import fcntl
import threading
from StandardModel import StandardModel
import pprint
import json
from shutil import move,Error

base = "/afs/cern.ch/user/l/lpc/www/lpcnotes/"
IDFILE         = base + "docId"
DOCTREEVERFILE = base + "docTreeVer"
LOCKFILE       = base + "lock"
WASTEDIR       = base + "wastebasket"

class FileModel( StandardModel ):

    def __init__( self, repoPath ):
        StandardModel.__init__( self )
        self.lockfile = open( LOCKFILE, 'w' )
        self.repoPath = repoPath
        self.haveLock = 0
        #print repoPath
        if not os.path.isfile( IDFILE ):
            f = open( IDFILE, "w")
            f.write('0')
            f.close()
        if not os.path.isfile( DOCTREEVERFILE ):
            f = open( DOCTREEVERFILE, "w")
            f.write('0')
            f.close()
            self.treeVer = 0
        if not os.path.isdir( WASTEDIR ):
            os.makedirs( WASTEDIR )
        self.loadDocumentTree()

    def lock( self ):
        """Implement a lock for different threads or processes"""
        fcntl.lockf( self.lockfile, fcntl.LOCK_EX )
        self.haveLock = 1;

    def unlock( self ):
        self.haveLock = 0;
        fcntl.lockf( f, fcntl.LOCK_UN )


    # get a new unique document Id number
    def newId( self ):
        f = open( IDFILE, 'r+' )
        id = (f.read())
        id = int(id) + 1
        f.seek(0)
        f.write(str(id))
        f.close()
        return id

    def docTreeVerHandler( self, new = 0 ):
        f = open( DOCTREEVERFILE, 'r+' )
        ver = (f.read())
        if ( new ):
            ver = int(ver) + 1
            f.seek(0)
            f.write(str(ver))
        f.close()
        self.treeVer = ver
        return ver

    def loadDocumentTree( self ):
        """ Read the document tree structure from the filesystem.
        Two "views" of the document strucuture are maintained:
           - The self.Documents dictionary contains the tree.
           - The self.DocumentH is a dictionary pointing to the Document
             data structures with the docId as a key."""

        # scan the file system tree in python
        self.initDocTree()
        maxDocId = 0
        for root,dirs,files in os.walk( self.repoPath ):
            #print repr( root )
            #print repr( dirs )
            #print repr( files )
            #print root, self.repoPath
            if root == self.repoPath:
                rootId = 0
            else:
                m = re.search( "(\d+)_(\d+)_([^/]+)$", root )
                if m:
                    rootId = int(m.group(1))
                    version = int(m.group(2))
                #print "rootId now " , rootId
                else:
                    assert("error: wrong file name for root id: " + root )
            # scan all directories and enter them as categories in the data structure
            for catn in dirs:
                catpath = str(root + "/" + catn)
                m = re.match( "(\d+)_(\d+)_(.+)$", catn )
                if m :
                    id = int(m.group(1))
                    version = int(m.group(2))
                    if id > maxDocId : maxDocId = id
                    catname=m.group(3)
                    ncat = { 'name'     : catname,
                             'id'       : id,
                             'version'  : version,
                             'type'     : 'c',
                             'catId'    : rootId,
                             'children' : [] }
                    #print "ncat ", repr(ncat)
                    self.DocumentH[ id ] = ncat
                    self.DocumentH[rootId]['children'].append( ncat )

            # treat all files ending .doc and enter them as documents in the data structure
            for nfile in files:
                m = re.match( "(\d+)_(\d+)_(.+)\.doc$", nfile )
                if m :
                    id = int(m.group(1))
                    if id > maxDocId : maxDocId = id
                    version = int(m.group(2))
                    docname=m.group(3)
                    ndoc = { 'name'      : docname,
                             'id'        : id,
                             'version'   : version,
                             'type'      : 'd',
                             'catId'     : rootId,
                             'children'  : None,
                             }
                    self.DocumentH[ id ] = ndoc
                    self.DocumentH[rootId]['children'].append( ndoc )

# Only create a new idfile if no old one exists. Clear the wastebasket if you want to 
# create a new idfile! Otherwise OS like MacOS cause problems if you move a file in 
# the wastebasket if is exists there already.
        if not os.path.isfile( IDFILE ):
            f = open( IDFILE, "w")
            f.write(str(maxDocId))
            f.close()

        return self.Documents

    def rescanDocTree( self, params, res = None ):
        self.loadDocumentTree()
        return self.updateDocTree( params )


################# the generic actions on documents #############################

    def newDocument( self, params ):
        res = super(FileModel, self).newDocument( params )

        # Now the file relevant part: make a new dir
        data   = params['data']
        catId  = int(data['catId'])
        version = 1
        dtype  = data['type']
        newId = res['newId']
        npath = self.getDocPath( newId, version, catId, data['docName'], dtype )
        if dtype == 'c':
            os.makedirs( npath )
        else:
            open( npath, 'a' ).close()

        res = self.updateDocTree( {} )
        if dtype == "d" :
            res['data']['records'].append( { 'action' : 'readDocument',
                                             'record' : { 'docId' : newId,
                                                          'docVer' : version,
                                                          'docTxt' : "",
                                                          'editMode' : 'edit' }})
        return res


    def deleteDocument( self, params ):
        docId = int(params['data']['docId'])
        doc = self.DocumentH[ docId ]
        docPath = self.getDocPath( docId )

        move( docPath, WASTEDIR )

        res = super(FileModel, self).deleteDocument( params )

        #res['debug'] += "Removing path ", docPath        
        
        return self.updateDocTree( {}, res )


    def rename( self, params ):
        docId = int(params['data']['docId'])
        doc = self.DocumentH[ docId ]
        docPath = self.getDocPath( docId )
        newName = str(docId) + '_' + str(doc['version']) + '_' + params['data']['docName'].strip()
        if doc['type'] == "d":
            newName += ".doc"
        newDocPath = os.path.dirname(docPath) + '/' + newName
        res = super(FileModel, self).rename( params )
        move( docPath, newDocPath)
        return res


    # what happens when user1 moves it and user2 has posted an update? shortly after?
    # documents are identified by their unique docId
    # moving/renaming documents results in a new document tree. This should be propagated
    #    during the sync. 
    def moveTo( self, params ):
        docId = int(params['data']['docId'])
        doc = self.DocumentH[ docId ]
        docPath = self.getDocPath( docId )
        newCatId = int(params['data']['catId'])
        newPath = self.getDocPath( newCatId ) + '/' + str(docId) + '_' + str(doc['version']) + '_' + doc['name']
        if doc['type'] == 'd':
            newPath = newPath + ".doc"

        try:
            move( docPath, newPath )
        except Error as ex:
            res = self.initres()
#            res['error'] = str(ex)
            res['error'] = "illegal destination: Cannot move a category into a sub-category of its own."
            return res
        #print "From ", docPath, "to", newPath

        return super(FileModel, self).moveTo( params )

    def readDocument( self, params ) :
        docId = int(params['data']['docId'])
        doc = self.DocumentH[ docId ]
        filepath = self.getDocPath( docId )
        if 'editMode' in params['data']:
            editMode = params['data']['editMode']
        else:
            editMode = "noedit"

        doctxt = None
        if doc["type"] == 'd':
            f = open( filepath )
            doctxt = f.read()
            f.close()
        
        res = self.initres()
        #res['debug'] = "readDocument " + doc['name']
        res['data'] = {'records' : [ { 'action' : 'readDocument',
                                       'record' : { 'docId'   : docId,
                                                    'docVer'  : doc['version'],
                                                    'docTxt'  : doctxt,
                                                    'editMode': editMode
                                                }
                                   }]}
        return res

#############################################################################

#    def sync( self, params ):
#        res = self.initres()
#        res['debug'] = "Server: sync command " + params['action']
#        print "Standard Model returning " + repr(res)
#        return res

    def tagList( self, params):
        res = self.initres()
        tag = params['data']['tag']

#        pat = re.compile( tag+"(?:\s|(?:&nbsp;))*(\d\d\d\d-\d\d-\d\d)?(?:\s|(?:&nbsp;))*(.*?)(\.|(</.+>))", re.MULTILINE ) 
        pat = re.compile( tag+"(?:\s|(?:&nbsp;))*(\d\d\d\d-\d\d-\d\d)?(?:\s|(?:&nbsp;))*(.*?)(\.|(</p>)|(<br)|(</li))", re.MULTILINE ) 
        taglist = []
        for docId,doc in self.DocumentH.iteritems():
            if doc['type'] != 'd' :
                continue
            filepath = self.getDocPath( docId )
            f = open( filepath )
            contents = f.read()
            f.close()

            nlist = pat.findall( contents )
            for it in nlist :
                taglist.append( { 'docName'  : doc['name'],
                                  'docId'    : docId,
                                  'item'     : it[1],
                                  'duedate'  : it[0] } )

        taglist = sorted(taglist,  cmp = self.tagsort )        

        res['data'] = {'records' : [ { 'action' : 'tagList',
                                       'record' : { 'taglist' : taglist,
                                                    'tag' : tag }
                                   }]}
        return res

    def tagsort( self, a, b ):
        if a['duedate'] == '' :
            return 1
        elif b['duedate'] == '':
            return -1

        return cmp( a['duedate'], b['duedate'] )

    def syncDocument( self, params ):
        res = self.initres();
        #print "syncdoc ", repr( params['data'] )
        treeVer = int(params['data']['treeVer'])

        if treeVer != self.treeVer:
            res = self.updateDocTree( {}, res )

        docId = int(params['data']['docId'])
        doc = self.DocumentH[ docId ]
        if doc['type'] == 'c':
            # We do not want messages to be deleted at the client.            
            return {'data': { 'records' : [] } }

        docVer = params['data']['docVer']
        docTxt = None
        if 'docTxt' in params['data']:
            docTxt = params['data']['docTxt']

        if doc['version'] != docVer:
            if docTxt :
                res['info'] = "Somebody edited the document behind your back. Your changes are lost... me:" + str(doc['version']) + " you:" + docVer
            filepath = self.getDocPath( docId )
            f = open( filepath )
            docTxt = f.read()
            f.close()

            res['data']['records'].append( { 'action' : 'updateDocument',
                                             'record' : { 'docId'  : docId,
                                                          'docVer' : doc['version'],
                                                          'docTxt' : docTxt }
                                         })
            return res
        
        if doc["type"] == 'd' and docTxt:
            filepath = self.getDocPath( docId )
            f = open( filepath, "w" )
            f.write( docTxt.encode('utf-8') );
            f.close()
            doc['version'] = docVer + 1
            newPath = self.getDocPath( docId )
            #print "Save From ", filepath, "to", newPath
            move( filepath, newPath)
            res['data']['records'].append( { 'action' : 'saveDocument',
                                             'record' : { 'docId'  : docId,
                                                          'docVer' : doc['version'] }
                                         })
        return res
        
        
########################### utilities ##############################
    def getDocPath( self,  id, version = None, catId = None, name = None, dtype = None ):
        npath = self.repoPath
        if name == None:
            name = self.DocumentH[ id ]['name']
        if dtype == None:
            dtype = self.DocumentH[ id ]['type']
        if catId == None:
            catId = self.DocumentH[ id ]['catId']
        if version == None:
            version = self.DocumentH[ id ]['version']
        npath = str(id) + '_' + str(version) + '_' + name
        if catId == None:
            return self.repoPath
        while catId != 0 :            
            npath = str(catId) + '_' + str(self.DocumentH[ catId ]['version']) + '_' + self.DocumentH[ catId ]['name'] + '/' + npath
            catId =  self.DocumentH[ catId ]['catId']
        npath = self.repoPath + "/" + npath
        if dtype == 'd':
            npath = npath + ".doc"
        return npath

