#!/usr/bin/python
import json
import os
import sys
import shlex
import subprocess
from readconfig import readconfig

config = readconfig( "lumi_config.json" )
ftfile      = config['webBaseDir']+"/"+config['year']+'/FillTable.json'
softwareDir = os.path.join( config["softwareBase"], "bin" )
cachecmd    = os.path.join( softwareDir, "updateLumiCache.py" )


if len(sys.argv) > 1:
    fillarr = sys.argv[1:]
    fillTable = {}
    for fill in fillarr:
        fillTable[str(fill)] = {}
    print "Using filltable with fills " + repr(fillarr)
else:
    fd = open( ftfile, 'r')
    fillTable = json.load( fd )
    fd.close()



sbfills = { 'CMS' : { 'fills' : [],
                      'number' : 0 },
            'ATLAS' : { 'fills' : [],
                        'number' : 0,},
            'LHCb' : { 'fills' : [],
                       'number' : 0 },
            'ALICE' : { 'fills' : [],
                        'number' : 0 }
}


ifill = 0
for (fillno, val) in fillTable.iteritems():
    sbfills['CMS']['fills'].append( fillno )
    sbfills['CMS']['number'] += 1
    sbfills['ATLAS']['fills'].append( fillno )
    sbfills['ATLAS']['number'] += 1
    sbfills['LHCb']['fills'].append( fillno )
    sbfills['LHCb']['number'] += 1
    sbfills['ALICE']['fills'].append( fillno )
    sbfills['ALICE']['number'] += 1
    ifill+=1
#    if ifill == 5:
#        break
jsonupdates = json.dumps( sbfills )

cacheca = shlex.split( str(cachecmd) + " '" + str(jsonupdates) + "'" )
updateLumiCacheProc = subprocess.Popen( cacheca, stdout=subprocess.PIPE,
                                        stderr=subprocess.PIPE )
(ldOutput,ldErr) = updateLumiCacheProc.communicate()

print "output:"
print ldOutput
print "error:"
print ldErr

