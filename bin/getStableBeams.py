#!/usr/bin/python
import os
import sys
import readconfig
import re
from lpcutils import Logger, Mailer
from datetime import datetime
import time
import json
import urllib
import pprint

config = readconfig.readconfig( "lumi_config.json" )

logger = Logger( config["logfile"] )

logger.log( "getStableBeams" )

# Times need to be in ms for the plotting scripts

startdate = datetime.strptime( config["startDate"], "%Y-%m-%d")
year = startdate.year

dstfile = os.path.join( config['workBaseDir'],str(year),"measurements/beam_modes.csv")

print dstfile

starttime = int( time.mktime( startdate.timetuple() ) ) * 1000
endtime = int( time.mktime( datetime.today().timetuple() ) ) * 1000

logger.log( "start time : " + repr(starttime) )
logger.log( "end time: " + repr(endtime) )

# This url retrieves the so called super-table 

url = "https://acc-stats.web.cern.ch/acc-stats/api/supertable/get?from=" + str(starttime) + "&to=" + str(endtime) + '&energy=&energyRanges=[{"min":0,"max":999999999} ]&beamType=&fillType='

logger.log( "url " + url )

urlconn = urllib.urlopen( url )
jsonstr = urlconn.read()

# for debugging
fd = open("tmp.json","w")
fd.write( jsonstr )
#jsonstr = fd.read()
fd.close

table = json.loads( jsonstr )

# analyse the contents of the supertable

fillcnt = 0
fd = open (dstfile, "w")
fd.write("Timestamp (UNIX Format),HX:BMODE,HX:FILLN\n")
for fill in reversed( table['fills'] ):
    if fill['fillDetails']['LHC.STATS:IS_STABLE_BEAMS'] == str(1):
        fillcnt += 1
        startSB = fill['fillDetails']['LHC.STATS:TIME_START_STABLEBEAMS'] + '000'
        lengthSB = fill['fillDetails']['LHC.STATS:TIME_DURATION_STABLEBEAMS']
        endSB = str(int(startSB) + int( lengthSB )*1000)
        dummyt = str(int(startSB)-10000)
        fd.write( str(dummyt) + ',10,' + str(fill['fillNumber']) + "\n")
        fd.write( str(startSB) + ',11,' + str(fill['fillNumber']) + "\n")
        fd.write( str(endSB) + ',13,' + str(fill['fillNumber']) + "\n")
        
fd.close()         
print str(fillcnt) + " fills"
logger.log( 'Updated the file ' + dstfile )
