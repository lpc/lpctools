#!/usr/bin/python

import matplotlib as mpl
mpl.use('Agg')
#mpl.interactive(True)
from scipy import optimize
from datetime import datetime
import time
import numpy as np
import scipy as sp
import pylab as P
import matplotlib.pyplot as plt
import matplotlib.offsetbox as offsetbox
from matplotlib.dates import *
import matplotlib.ticker as ticker
import string
import os.path
from mytools import *
from cmode import *
import sys
if len(sys.argv) > 1:
    fillnr = int(sys.argv[1])
else:
    fillnr = 0
#from scipy.interpolate import interp1d
from scipy import interpolate
from scipy import optimize
from scipy import integrate
import readconfig
from lpcutils import Logger

config = readconfig.readconfig("lumi_config.json")

YEAR = config['year']
LPC_FIG_DIR = os.path.join( config['workBaseDir'], config['year'], "measurements/FIGURES" )
logger = Logger(config['logfile'])

logstr =  "Making 'Stable Beams' plots for the year " + str(YEAR) + "\n"

M = cmode()

sbfills = M.sbFills()
lastfill = int(sbfills[-1])
tnow = datetime.now()
tstr = str(tnow.strftime("%Y-%m-%d %H:%M"))
genstr = '( '+tstr+' including fill '+str(lastfill)+'; scripts by C. Barschel )'
print genstr
logstr += genstr + "\n"

#####

t = [] # time
s = [] # SB flag (0,1)

for fill in sbfills:
    #if fill > 2267:
    #    continue
    
    tsb,tdump = M.sb(fill,format=1)
    
    t.append(tsb)
    t.append(tsb)
    t.append(tdump)
    t.append(tdump)

    s.append(0)
    s.append(1)
    s.append(1)
    s.append(0)


si = interpolate.interp1d(t, s, kind='linear')
sie = extrap1dsame(si)


#### generate data points from logging info (cmode object)
datstr = str(YEAR) + "-02-01 00:00:1.000"
tstart = timeDateNum(datstr)
tint = tstart
sbti = 0
td = [] # timestamp
sd = [] # time per day in SB
sid = [] # integrated time per day in SB
print "do day integration"

while tint < t[-1]:

    sbresult = integrate.quad(sie,tint,tint+1,full_output=1)
    sbt = sbresult[0]*24
    sbti += sbt
    
    print "SB value at",tint,':',sbt

    td.append(tint)
    sd.append(sbt)
    sid.append(sbti)

    tint += 1

    td.append(tint)
    sd.append(sbt)
    sid.append(sbti)

td.append(tint)  # time ticks
sd.append(0)     # daily integration per 24h
sid.append(sbti) # daily integration total

    
print "do week integration"
tint = tstart
tw = [] # time week
sw = []

while tint < t[-1]:

    sbt = integrate.quad(sie,tint,tint+7,full_output=1)
    print sbt[0]

    tw.append(tint)
    tw.append(tint)
    sw.append(0)
    sw.append(sbt[0]*24)

    tint += 7

    tw.append(tint)
    sw.append(sbt[0]*24)

tw.append(tint)
sw.append(0)

####
def custom_date_axis(ax):
    ax.xaxis_date()
    #ax.xaxis.set_major_locator(WeekdayLocator(byweekday=MO))
    ax.xaxis.set_major_locator(MonthLocator())
    ax.xaxis.set_minor_locator(MonthLocator(bymonthday=15))
    #ax.xaxis.set_major_formatter( DateFormatter('%V') )
    ax.xaxis.set_major_formatter(ticker.NullFormatter())
    #ax.xaxis.set_minor_formatter( DateFormatter('%B') ) # full month name
    ax.xaxis.set_minor_formatter( DateFormatter('%b') ) # abbreviated month name
    ax.set_xlabel('Month in '+str(YEAR))
    ax.grid(b=True,linewidth=0.3,color="0.1",alpha=0.6)
    for tick in ax.xaxis.get_minor_ticks():
        tick.tick1line.set_markersize(0)
        tick.tick2line.set_markersize(0)

        
prop = mpl.font_manager.FontProperties(size='small')

fig = plt.figure(figsize=(6,4))
ax = fig.add_subplot(111)

ax.set_title('Time in stable beams ('+str(YEAR)+')')
#ax.set_xlabel('Week number')
ax.set_ylabel('Stable beams')

ax.plot(t,s,
        color='black',
       linestyle='-')
#ax.plot(xt,si(xt),
#        color='blue',
#        linestyle='-')

ax.fill_between(t, s, 0,color='b',alpha=0.9,)

ax.set_ylim(0,2)

#ax.xaxis_date()
#ax.xaxis.set_major_locator(WeekdayLocator(byweekday=1))
#ax.xaxis.set_major_formatter( DateFormatter('%V') )

custom_date_axis(ax)


##### P L O T S #####
    
##### combined day + week
    
fig = plt.figure(figsize=(6,4))
fig.subplots_adjust(bottom=0.17)
ax = fig.add_subplot(111)

ax.set_title('Time in stable beams ('+str(YEAR)+')')
#ax.set_xlabel('Week number')
ax.set_ylabel('Stable beams time (hours)')

ax.plot(tw,sw,
        color='black',
        linestyle='-',
        linewidth=1.5,
        label="SB hours per week")

ax.fill_between(tw, sw, 0,color='green',alpha=0.8,)

ax.plot(td,sd,
        color='blue',
        linestyle='-',
        linewidth=1.5,
        label="SB hours per day")

ax.fill_between(td, sd, 0,color='orange',alpha=0.8,)

#ax.set_ylim(0,100)

#ax.xaxis_date()
#ax.xaxis.set_major_locator(WeekdayLocator(byweekday=MO))
#ax.xaxis.set_major_formatter( DateFormatter('%V') )

ax.axhline(y=25, color='m',linewidth=0.5,linestyle='--')
t1 = ax.text(0.,-0.20,genstr,transform=ax.transAxes,fontsize='xx-small')

l = ax.legend(loc=1,prop=prop,numpoints=1) # loc=1: upper right
l.legendPatch.set_alpha(0.7)

custom_date_axis(ax)

fig.savefig(LPC_FIG_DIR+'/png/time_in_stable_beams.png'
               # ,bbox_inches='tight')
            ,bbox_extra_artists=[t1])
fig.savefig(LPC_FIG_DIR+'/time_in_stable_beams.pdf'
               # ,bbox_inches='tight')
            ,bbox_extra_artists=[t1])


##### SB per day
## hours per day
    
fig = plt.figure(figsize=(6,4))
fig.subplots_adjust(bottom=0.17)
ax = fig.add_subplot(111)

ax.set_title('Time in stable beams ('+str(YEAR)+')')
#ax.set_xlabel('Week number')
ax.set_ylabel('Stable beams time per day (hours)')


ax.plot(td,sd,
        color='blue',
        linestyle='-',
        linewidth=1.5,
        label="SB hours per day")

ax.fill_between(td, sd, 0,color='orange',alpha=1,)

ax.set_ylim(0,25)

#ax.xaxis_date()
#ax.xaxis.set_major_locator(WeekdayLocator(byweekday=MO))
#ax.xaxis.set_major_formatter( DateFormatter('%V') )
ax.axhline(y=25, color='m',linewidth=0.5,linestyle='--')
t1 = ax.text(0.,-0.20,genstr,transform=ax.transAxes,fontsize='xx-small')
#l = ax.legend(loc=1,prop=prop,numpoints=1) # loc=1: upper right
#l.legendPatch.set_alpha(0.7)

custom_date_axis(ax)

fig.savefig(LPC_FIG_DIR+'/png/time_in_stable_beams_day.png'
               #,bbox_inches='tight'
            ,bbox_extra_artists=[t1])
fig.savefig(LPC_FIG_DIR+'/time_in_stable_beams_day.pdf'
                #,bbox_inches='tight'
            ,bbox_extra_artists=[t1])




##### integrated hours per day
    
fig = plt.figure(figsize=(6,4))
fig.subplots_adjust(bottom=0.17,right=0.87)
ax = fig.add_subplot(111)


ax.fill_between(td, sd, 0,color='orange',alpha=1,
                 label="SB hours per day")
ax.set_ylabel('Stable beams time per day (hours)')
ax.set_ylim(0,25)

ax.set_title('Time in stable beams ('+str(YEAR)+')')
#ax.set_xlabel('Week number')


ax2 = ax.twinx() # share X axis
ax2.plot(td,sid,
        color='blue',
        linestyle='-',
        linewidth=1.5,
        label="Integrated SB hours")
ax2.set_ylabel('Integrated time in stable beams (hours)')

#ax.xaxis_date()
#ax.xaxis.set_major_locator(WeekdayLocator(byweekday=MO))
#ax.xaxis.set_major_formatter( DateFormatter('%V') )
#ax.axhline(y=25, color='m',linewidth=0.5,linestyle='--')
t1 = ax.text(0.,-0.20,genstr,transform=ax.transAxes,fontsize='xx-small')
#l = ax.legend(loc=1,prop=prop,numpoints=1) # loc=1: upper right
#l.legendPatch.set_alpha(0.7)

custom_date_axis(ax)

fig.savefig(LPC_FIG_DIR+'/png/time_integrated_in_stable_beams_day.png'
                #,bbox_inches='tight'
            ,bbox_extra_artists=[t1])
fig.savefig(LPC_FIG_DIR+'/time_integrated_in_stable_beams_day.pdf'
                #,bbox_inches='tight'
            ,bbox_extra_artists=[t1])




##### SB per week
    
fig = plt.figure(figsize=(6,4))
fig.subplots_adjust(bottom=0.17)
ax = fig.add_subplot(111)

ax.set_title('Time in stable beams ('+str(YEAR)+')')
#ax.set_xlabel('Week number')
ax.set_ylabel('Stable beams time per week (hours)')

ax.plot(tw,sw,
        color='black',
        linestyle='-',
        linewidth=1.5,
        label="SB hours per week")

ax.fill_between(tw, sw, 0,color='green',alpha=0.9,)

#ax.set_ylim(0,168)

#ax.xaxis_date()
#ax.xaxis.set_major_locator(WeekdayLocator(byweekday=MO))
#ax.xaxis.set_major_formatter( DateFormatter('%V') )
t1 = ax.text(0.,-0.20,genstr,transform=ax.transAxes,fontsize='xx-small')
#l = ax.legend(loc=1,prop=prop,numpoints=1) # loc=1: upper right
#l.legendPatch.set_alpha(0.7)

custom_date_axis(ax)

fig.savefig(LPC_FIG_DIR+'/png/time_in_stable_beams_week.png'
                #,bbox_inches='tight'
            ,bbox_extra_artists=[t1])
fig.savefig(LPC_FIG_DIR+'/time_in_stable_beams_week.pdf'
                #,bbox_inches='tight'
            ,bbox_extra_artists=[t1])

logstr += 'Plots for stable beams are updated in "' + LPC_FIG_DIR + '\n'
logger.log( logstr )
