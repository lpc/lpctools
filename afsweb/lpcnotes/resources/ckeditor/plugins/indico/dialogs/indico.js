// Note: This automatic widget to dialog window binding (the fact that every field is set up from the widget
// and is committed to the widget) is only possible when the dialog is opened by the Widgets System
// (i.e. the widgetDef.dialog property is set).
// When you are opening the dialog window by yourself, you need to take care of this by yourself too.

CKEDITOR.dialog.add( 'indico', function( editor ) {
    var commonLang = editor.lang.common;
    return {
	title: 'Indico Presentation',
	minWidth: 200,
	minHeight: 100,
	contents: [
	    {
		id: 'info',
		elements: [
		    {
			id: 'align',
			type: 'select',
			label: 'Align',
			'default': 'right',
			items: [
			    [ editor.lang.common.notSet, '' ],
			    [ editor.lang.common.alignLeft, 'left' ],
			    [ editor.lang.common.alignRight, 'right' ],
			    [ editor.lang.common.alignCenter, 'center' ]
			],
			// When setting up this field, set its value to the "align" value from widget data.
			// Note: Align values used in the widget need to be the same as those defined in the "items" array above.
			setup: function( widget ) {
			    if ( widget.data.align ) {
				this.setValue( widget.data.align );
			    } else {
				this.setValue( this.default );
			    }
			},
			// When committing (saving) this field, set its value to the widget data.
			commit: function( widget ) {
			    widget.setData( 'align', this.getValue() );
			}
		    },
		    {
			id:        'width',
			type:      'text',
			label:     'Width',
			width:     '50px',
			setup: function( widget ) {
			    if ( widget.data.width ) {
				this.setValue( widget.data.width );
			    } else {
				this.setValue( this.default );
			    }
			},
			commit: function( widget ) {
			    widget.setData( 'width', this.getValue() );
			},
			"default": "900px",
		    },
		    {
			id:        'height',
			type:      'text',
			label:     'Height',
			width:     '50px',
			setup: function( widget ) {
			    if ( widget.data.height ) {
				this.setValue( widget.data.height );
			    } else {
				this.setValue( this.default );
			    }
			},
			commit: function( widget ) {
			    widget.setData( 'height', this.getValue() );
			},
			"default": "600px"
		    },
		    {
			id:        'frameurl',
			type:      'text',
			label:     'URL',
			width:     '300px',
			setup: function( widget ) {
			    if ( widget.data.frameurl ) {
				this.setValue( widget.data.frameurl );
			    } else {
				this.setValue( this.default );
			    }
			},
			commit: function( widget ) {
			    widget.setData( 'frameurl', this.getValue() );
			},
			"default": "",
		    },
		    {
			id:        'indicolink',
			type:      'text',
			label:     'Indico URL',
			width:     '300px',
			setup: function( widget ) {
			    if ( widget.data.indicourl ) {
				this.setValue( widget.data.indicourl );
			    } else {
				this.setValue( this.default );
			    }
			},
			commit: function( widget ) {
			    widget.setData( 'indicourl', this.getValue() );
			},
			"default": "",
		    },
		    {
			id:        'twikilink',
			type:      'text',
			label:     'TWIKI URL',
			width:     '300px',
			setup: function( widget ) {
			    if ( widget.data.twikiurl ) {
				this.setValue( widget.data.twikiurl );
			    } else {
				this.setValue( this.default );
			    }
			},
			commit: function( widget ) {
			    widget.setData( 'twikiurl', this.getValue() );
			},
			"default": "",
		    }
		]
	    }
	]
    };
} );
