import matplotlib as mpl
import numpy as np
import scipy as sp
import pylab as P
import matplotlib as mpl
import matplotlib.pyplot as plt
import string
import os.path
from mytools import *
#from config import *
from cmode import *
from cdata import *

## add virtual variables to the original data
"""
emittance
"""


class cvirtual():

    e_x = 0
    e_y = 0
    e_xy = 0
    e_lum = 0
    
    def __init__(self,fillnr=0,M=None):
        self.fillnr = fillnr
        if M == None:
            self.M = cmode(fillnr)
        else:
            self.M = M
          


    # emittance from luminous reg sigmas
    def eLumireg(self,D,fillnr=0):
        if fillnr != 0:
            self.fillnr = fillnr

        # Ca = angle correction
        # ei = 2 * sigma_i_lum^2 * Ca_i^2 / B*
        bstar = self.M.betastar()[D.expt]
        Ctheta = {'x':1,'y':1}
        if 'tzsu' not in self.d.keys():
            self.trueSigmaZ(theta=theta,mode=M)
        if 'ctheta' not in self.d.keys():
            self.trueSigmaZ(theta=theta,mode=M)
        
        print D.expt,'B*',bstar
        
        D.d['e_x'] = []
        D.d['e_y'] = []
        D.d['de_x'] = []
        D.d['de_y'] = []

        for j in ('x','y'):
            for i in range(len(D.d['time'])):
                e = (2 * D.d[j+'su'][i]**2 * Ctheta[j]**2) / bstar
                D.d['e_'+j].append(e)
                de = 0
                D.d['de_'+j].append(de) # todo error e_i

        D.d['e_x'] = np.array(D.d['e_x'])
        D.d['e_y'] = np.array(D.d['e_y'])
        D.d['de_x'] = np.array(D.d['de_x'])
        D.d['de_y'] = np.array(D.d['de_y'])
        
        for k in ('e_x','e_y'):
            # warning add 1 to deal with error = 0
            avg = np.average(D.d[k],weights=1/(D.d['d'+k]+1.))
            std = np.std(D.d[k])

            D.d[k+'_avg'] = avg
            D.d[k+'_std'] = std

    # emittance from specific luminosity
    def eLumi(self,D,fillnr=0):
        if fillnr != 0:
            self.fillnr = fillnr

        flhc = 11245.5 # Hz

        # Ca = angle correction
        # ei = 2 * sigma_i_lum^2 * Ca_i^2 / B*
        bstar = self.M.betastar()[D.expt]
        Ca = {'x':1,'y':1}
        
        print D.expt,'B*',bstar
        
        D.d['e_lum'] = []

        for i in range(len(D.d['time'])):
            e = flhc / (4 * np.pi * bstar * D.d['sl'][i] * 1e34)
            D.d['e_lum'].append(e)

        D.d['e_lum'] = np.array(D.d['e_lum'])

        
## Test the cfile class
if __name__ == "__main__":
    expt = sys.argv[1]
    fillnr = int(sys.argv[2])


    M = cmode(fillnr)
    D = cdata(expt,fillnr,'r')
    # check if file exists
    if D.exist():
        print 'file exists'
        D.read()
        D.tavg(dt=300)
    else:
        sys.exit()
        
    Dl = cdata(expt,fillnr,'l')
    # check if file exists
    if Dl.exist():
        print 'file exists'
        Dl.read()
        Dl.tavg(dt=300)
    else:
        sys.exit()
        
    V = cvirtual(fillnr,M)
    V.eLumireg(D)
    V.eLumi(Dl)

    idx = []
    for i in range(len(Dl.d['time'])):
        if Dl.d['stab'][i] == 1 and Dl.d['sl'][i] < 1e-21:
            idx.append(i)

    fig = plt.figure()
    ax = fig.add_subplot(111)
    color = {'ATLAS':'k','CMS':'g','LHCb':'b','ALICE':'r'}
    mew = {'ATLAS':1,'CMS':0.5,'LHCb':0.5,'ALICE':0.5} # markeredgewidth
    mfc = {'ATLAS':'0.5','CMS':'0.8','LHCb':'0.9','ALICE':'1'} # markerfacecolor
    marker = {'ATLAS':'o','CMS':'^','LHCb':'D','ALICE':'s'}
    
    ax.errorbar(x=D.d['time'],
                 y=D.d['e_x'],
                 yerr=None,
                 marker=marker[D.expt],
                 markersize=4,
                 markeredgecolor=color[D.expt],
                 markeredgewidth=mew[D.expt],
                 color=color[D.expt],
                 markerfacecolor=mfc[D.expt],
                 alpha=0.8,
                 linewidth=0.5,
                 label='e_x',
                 linestyle='')
    
    ax.errorbar(x=D.d['time'],
                 y=D.d['e_y'],
                 yerr=None,
                 marker=marker[D.expt],
                 markersize=4,
                 markeredgecolor=color[D.expt],
                 markeredgewidth=mew[D.expt],
                 color=color[D.expt],
                 markerfacecolor='c',
                 alpha=0.8,
                 linewidth=0.5,
                 label='e_y',
                 linestyle='')
    
    ax.errorbar(x=Dl.d['time'][idx],
                 y=Dl.d['e_lum'][idx]*3.5e4,
                 yerr=None,
                 marker=marker[Dl.expt],
                 markersize=4,
                 markeredgecolor=color[Dl.expt],
                 markeredgewidth=mew[Dl.expt],
                 color=color[Dl.expt],
                 markerfacecolor='m',
                 alpha=0.8,
                 linewidth=0.5,
                 label='e_lum',
                 linestyle='')
    
