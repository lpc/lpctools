#!/usr/bin/env python
from cgi import escape
import sys, os
import glob
from werkzeug.wrappers import Request

from logger import debug,error,log
import view

from NoteFileModel import NoteFileModel
from ActionHandler import ActionHandler
from DocumentHandler import DocumentHandler

REPOPATH = "/afs/cern.ch/user/l/lpc/www/lpcnotes/documents"

def app(environ, start_response):

# some light stuff for debugging
#    start_response('200 OK', [('Content-Type', 'text/html')])
#
#    yield '<h1>Da Notes FastCGI Environment</h1>'
#    yield '<table>'
#    for k, v in sorted(environ.items()):
#        yield '<tr><th>%s</th><td>%s</td></tr>' % (k, v)
#    yield '</table>'

     request = Request(environ)
     path = request.path
     start_response('200 OK', [('Content-Type', 'text/html')])
 
     (head, tail) =  os.path.split( path )
     # An Ajax request for the Action Handler:
     if tail == "docaction" :
         yield AH.handleRequest( request )
         return
 
     # The initial page request builds up the fundamental page
 
     yield view.printHead( "/lpc-afs/lpcnotes" )
     #yield view.printHead( path )
 
     yield view.printTail( )

##########################################
# globals which are instantiated on load #
##########################################
AH = ActionHandler()
FM = NoteFileModel( REPOPATH )
DH = DocumentHandler( FM, AH )

# extra actions implemented in the NoteFileModel are also registred by hand.
AH.register( "getGantt", DH )
