CKEDITOR.addTemplates("default",
		      {imagesPath:CKEDITOR.getUrl(CKEDITOR.plugins.getPath("templates")+"templates/images/"),
		       templates:[
			   {title:"Letter Din 5001 Form A",
			    image:"template3.gif",
			    description:"A German standard letter.",
			    html:'<!-- DIN 5008 Form A -->\
\
<div style="position:relative; background:white; width:210mm; height:297mm">\
\
<!-- faltmarken -->\
<div style="position: absolute; height: 87mm; width: 6mm; border-bottom: 1px solid #a0a0a0"></div>\
<div style="position: absolute; height: 148.5mm; width: 10mm; border-bottom: 1px solid #a0a0a0"></div>\
<div style="position: absolute; height: 192mm; width: 6mm; border-bottom: 1px solid #a0a0a0"></div>\
\
<table style="margin-left:25mm; position:absolute; top:27mm;border-spacing:0mm 0mm; border-collapse:collapse;">\
<tr>\
<td><span style="border:1px solid #404040; font-size:7pt"> \
Christoph Schwick; Av. Dumas 2bis; 1206 Geneva; Switzerland</span>\
</td><td style="width:20mm"></td><td></td></tr>\
\
<tr><td style="font-size:11pt; border:1px solid #c0c0c0; height:40mm; width:80mm; vertical-align:top;">\
<div>\
<p>Adresse</p>\
</div>\
</td>\
<td></td>\
<td style="font-size:11pt; border:1px solid #c0c0c0; width:75mm; vertical-align:top;"> \
<div>\
<br>\
<br>\
<br>\
<p id="date">Date: </p> \
<script> var now = new Date(); \
var dstr = now.getDate() + " / " + (now.getMonth() + 1) + " / " + now.getFullYear(); \
$("p#date").append( dstr ); \
</script> \
</p>\
</div>\
</td></tr>\
\
\
<tr>\
<td colspan="3" style="font-size:11pt; width:165mm; border-spacing: 0mm 8mm; border:1px solid #c0c0c0; padding-right:10mm; padding-top:8mm"> \
<div>\
Anrede\
<p> -- Text here -- \
</p>\
</div>\
</td>\
</tr>\
</table>\
</div>\
'
			   },
		      {title:"Letter for Swiss Envelop old standard window right",
			    image:"template3.gif",
			    description:"A standard letter.",
			    html:'\
<div font-size:10pt; style="position:relative; background:white; width:210mm; height:297mm; border:1px solid #e0e0e0;">\
\
<!-- faltmarken -->\
<div style="position: absolute; height: 108mm; width: 6mm; border-bottom: 1pt dotted #000000"></div>\
<div style="position: absolute; height: 148.5mm; width: 10mm; border-bottom: 1pt dotted #000000"></div>\
<div style="position: absolute; height: 207mm; width: 6mm; border-bottom: 1pt dotted #000000"></div>\
\
<div style="position:static; width=210mm; height=297mm" >\
\
<div style="position:absolute; left:23mm;top:10mm;max-width:70mm;height:78mm;border:1px none #e0e0e0;">\
<br>Dr. Christoph Schwick<br>\
<br>\
Av. Dumas 2bis<br>\
1206 Geneva<br>\
SWITZERLAND<br>\
<br>\
<table style="border: none; width:100%">\
<tr><td>Tel</td><td>:</td><td>+41-22-3286578</td></tr>\
<tr><td>Mobile</td><td>:</td><td>+41-76-4870793</td></tr>\
<tr><td>email</td><td>:</td><td>Christoph.Schwick@bluewin.ch</td></tr>\
</table>\
</div>\
\
<div style="position:absolute; left:120mm;top:47mm;width:80mm;height:4mm;border-bottom:1pt solid #000000;">\
<span style="font-size:6pt;">Christoph Schwick; Av Dumas 2bis; 1206 Genève; Switzerland</span>\
</div>\
<div style="position:absolute; left:120mm;top:52mm;width:80mm;height:36mm;border:1px none #e0e0e0;">\
<p>Adresse</p>\
</div>\
</div>\
<div style="position:absolute; left:23mm;top:100mm;width:177mm;height:10mm;border:1px none #e0e0e0;">\
<table style="width:100%;border:none;"><tr><td style="text-align:left; border:none;"><span style="font-weight:bold">Betrifft:</span>&nbsp;</td><td id=\"date\" style="text-align:right; border:none;">Date</td></tr></table>\
</div>\
<div style="position:absolute; left:23mm;top:115mm;width:177mm;height:165mm;border:1px none #e0e0e0;">\
Dear Ladies and Gentlemen,\
<p>\
Text\
</p>\
</div>\
</div>\
<script> var now = new Date(); \
var dstr = now.getDate() + " / " + (now.getMonth() + 1) + " / " + now.getFullYear(); \
$("td#date").html( dstr ); \
</script> \
'
			   }
		       ]
		      }
		     );
