#!/usr/bin/python
import glob
import shutil
import os
import readconfig
import lpcutils
from lpcutils import Logger

config     = readconfig.readconfig( "lumi_config.json" )
year       = config["year"]
plotDir    = os.path.join( config['workBaseDir'],year, "measurements/FIGURES/png" )
plotWebDir = os.path.join( config['webBaseDir'],year )


logger     = Logger( config["logfile"] )

for plot in glob.glob( os.path.join( plotDir,"*" ) ):
    print plot, plotWebDir
    shutil.copy( plot, plotWebDir )

## copy cache file for optimal fill length
#tacachefile = os.path.join( config['workBaseDir'],year,"fillCache","turn_around_cache.json")
#if os.path.isfile( tacachefile ):
#    shutil.copy( tacachefile, plotWebDir )

logger.log( "Copied all plots from " + plotDir + "\n to " + plotWebDir + "." )

