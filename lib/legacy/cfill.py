import string
import os.path
from mytools import *
from cfile import *
from cdata import *

## contains the data from file cfile
class cfill(cdata):
    i = 0 # index of fillnr from the fill2010.txt data set
    fillnr = 0
    fp = None # fill pattern

    def __init__(self,fillnr=0):
        type='fills2010.txt'
        cdata.__init__(self,'',fillnr,type,bucket=0)
        if self.exist():
            self.read()
            
        self.setFill(fillnr)
        

    def setFill(self,fillnr):
        if fillnr in self.d['fill']:
            self.i = self.d['fill'].tolist().index(fillnr)
            self.fillnr = int(fillnr)
            self.fp = self.fillPattern()
    
    def stableBeam(self):
        return self.sb()
        
    def sb(self):
        d = int(self.d['dd'][self.i])
        m = int(self.d['m'][self.i])
        y = int(self.d['yyyy'][self.i])
        hh= int(self.d['hh'][self.i])
        mm= int(self.d['mm'][self.i])
        timeTuple = (y,m,d,hh,mm,0)
        return timeTuple
        
    ## stable beam start GPS time
    # return SB start as GPS time and length in h
    def sbGps(self):
        timeTuple = self.sb()
        
        # data set time = in seconds since january 1, 2010, 00:00:00  (CET).
        offset = timeTupleGps((2010,1,1,0,0,0))
        return timeTupleGps(timeTuple)-offset,self.d['hlength'][self.i]

    ## Filling scheme file name uses the following pattern:
    #   bpi = max nr of bucketes per injection 
    #   kb = bucketes per beam 
    #   nc<i> = number of colliding pairs at IP <i> (nc1=nc5)
    # bpi   kb_nc1_nc2_nc8.txt
    def fillPatternFile(self,fillnr=0):
        if fillnr == 0:
            fillnr = self.fillnr
        
        if fillnr not in self.d['fill']:
            i = 0
        else:
            i = self.d['fill'].tolist().index(fillnr)
        
        bpi = str(int(self.d['bpi'][i]))
        kb  = str(int(self.d['kb'][i]))
        nc1 = str(int(self.d['nc1'][i]))
        nc2 = str(int(self.d['nc2'][i]))
        nc8 = str(int(self.d['nc8'][i]))
        
        return bpi+'_'+kb+'_'+nc1+'_'+nc2+'_'+nc8+'.txt'
    
    # read fill pattern file for this fill configuration
    # return dict of pattern
    # FP{['nr', 'beam1', 'beam2']}
    # each collumn is a np array of the values as int
    def fillPattern(self,fillnr=0):
        file = self.dir+'/FillPatterns/'+self.fillPatternFile(fillnr)

        if os.path.isfile(file):
            #print 'fill patterns file ok'
            d = readlpcfills(file)
            for field in d:
               d[field] = d[field].astype(int) # convert to numpy array
            return d
        else:
            return None
    
    # return list of colliding buckets for this IP nr
    def collidingBuckets(self,ipnr=1):
        if self.fp == None:
            return []
        if ipnr == 5: ipnr = 1
        
        cb = [] # list of colliding bucketes
        for i in range(len(self.fp['nr'])):
            if self.fp['nr'][i] == ipnr:
                if self.fp['beam1'][i] != 0 and self.fp['beam2'][i] != 0:
                    cb.append(self.fp['beam1'][i])
        
        return cb
        

## Test the cfile class
if __name__ == "__main__":
    if len(sys.argv) > 1:
        fillnr = int(sys.argv[1])
    else:
        fillnr = 1453
    F = cfill(fillnr)

    print 'Stable beam start:',F.sbGps()
    print 'Fill Pattern file:',F.fillPatternFile()
    #print F.fillPatternFile(1005)
    FP = F.fillPattern()
    for ipnr in (1,2,8):
        cb = F.collidingBuckets(ipnr=ipnr)
        print 'colliding buckets for ip',ipnr,cb
    
