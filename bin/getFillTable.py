#!/usr/bin/python
import os
import sys
import re
from datetime import datetime, timedelta
import time
import json
import pprint
import subprocess
import shlex
import csv
import readconfig
from lpcutils import Logger, Mailer
from optparse import OptionParser

#The csv files have to be merged (so far a stright dump)
#Test it somwhere with a 

########################### utility functions #################################

def joinFillTable( newHash ):
    oldHash = {}
    dstfile = os.path.join( config['webBaseDir'],str(year),"FillTable.json")
    if os.path.isfile( dstfile ):
        fd = open( dstfile, "r" )
        oldHash = json.load( fd )
        fd.close()

    # udpate the old hash
    # To get everything in unicode
    newHash = json.loads( json.dumps( newHash) )
    for (fill,val) in newHash.iteritems():
        oldHash[ fill ] = val

    # overwrite the old hash with the updated one
    fd = open( dstfile, "w" )
    json.dump( oldHash, fd )
    fd.close()
    
    return oldHash
    
def updateBeamModesCSV() :
    oldbm = os.path.join( config['workBaseDir'], str(year), "measurements", "beam_modes.csv" )
    if os.path.isfile( oldbm ):
        oldbmf = open( oldbm, 'r' )
        oldbmr = csv.reader( oldbmf )
        # get rid of header
        oldbmr.next()
    else :
        print "No old file"
        oldbmf = None

    newbm = os.path.join( config['workBaseDir'], str(year), "measurements", "beam_modes_new.csv" )
    newbmf = open( newbm, 'w' )
    newbmw = csv.writer( newbmf )
    newbmw.writerow(['Timestamp (UNIX Format)','HX:BMODE','HX:FILLN'])

    updbm = os.path.join( config['workBaseDir'], str(year), "measurements", "beam_modes_update.csv" )
    updbmf = open( updbm, 'r' )
    updbmr = csv.reader( updbmf )
    # get rid of header
    updbmr.next()
    # find out where the update file starts (at which fill)
    try:
        while 1:
            updbmr.next()
            uline = updbmr.next()
            if uline[2]:
                break
    except StopIteration as e:
        updbmf.close()
        newbmf.close()
        if oldbmf:
            oldbmf.close()
        return

    ufill = uline[2]

    # Now advance the old file to the fill where the update file starts

    fill = "0"
    if oldbmf:
        while fill <> ufill:
            line = oldbmr.next()
            if line[2] <> '':
                fill = line[2]
            if fill <> ufill:    
                newbmw.writerow( line )

    # Now we complete the new file with the update data    
    newbmw.writerow( uline )
    for uline in updbmr:
        newbmw.writerow( uline )

    updbmf.close()
    newbmf.close()
    if oldbmf:
        oldbmf.close()

    # finally we need to rename the new file (does not work on windows like this!)
    os.rename( newbm, oldbm )


################################################################################

### parse the command line options ###
parser = OptionParser()

parser.add_option("-s", "--startDate", type="string",dest="startDate",
                  help="set the starting date of the Timber command" )
parser.add_option("-n", "--toNow", action="store_true",dest="toNow",
                  help="set the end date of the command to tomorrow" )
parser.add_option("-i", "--interval", type="int",dest="interval",
                  help="number of days to go to the past (with the start date)" )
(options, args) = parser.parse_args()

if options.interval and options.startDate:
    parse.error( "options -s and -i are mutually exclusive!" )


config = readconfig.readconfig( "lumi_config.json" )

logger = Logger( config["logfile"] )
logger.log( "getFillTable" )

timberClient = config["timberClient"]
timberClientConfig = config["timberClientConfig"]

# Times need to be in ms for the plotting scripts

endDateStr = str(config["endDate"])
if options.toNow or options.interval :
    endDateStr = (datetime.now() + timedelta(days=1)).strftime( "%Y-%m-%d" )
    logger.log( "End date set to: " + endDateStr + " via -i (interval) option" )

enddate = datetime.strptime( endDateStr, "%Y-%m-%d")


startDateStr = str(config["startDate"])
if options.startDate:
    startDateStr = options.startDate
    logger.log("Taking start date from command line: " + startDateStr )
elif options.interval:
    startDateStr = (datetime.strptime( endDateStr, "%Y-%m-%d" ) - timedelta(days=options.interval)).strftime("%Y-%m-%d")
    
else:
    logger.log("Taking start date from configuration file: " + startDateStr)

startdate = datetime.strptime( startDateStr, "%Y-%m-%d")

year      = startdate.year

#starttime = int( time.mktime( startdate.timetuple() ) ) * 1000
#endtime   = int( time.mktime( enddate.timetuple() ) ) * 1000
#logger.log( "start time : " + repr(starttime) )
#logger.log( "end time: " + repr(endtime) )


###################### launch the java command to Timber ####################
# This url retrieves the so called super-table 

command = 'java -cp ' + str(timberClient) + ' -Xms128m -Xmx1024m -jar ' + str(timberClient) + ' -C '+ str(timberClientConfig) + ' -t1 ' + startDateStr + ' -t2 ' + endDateStr + ' -G -vs "HX:FILLN,HX:BMODE,LHC:INJECTION_SCHEME"'

logger.log( "command: " + command )
print command
args = shlex.split( command )
print args


# time the timber command
sw_start = datetime.now()
try:
    proc = subprocess.Popen( args, stdout=subprocess.PIPE )
    cr = csv.reader( iter(proc.stdout) )
except subprocess.CalledProcessError as e:
    print "Error!  return code: " + str(e.returncode)
    print e.output
    sys.exit()


################# analyse the timber data ############################
######## also generate the beam modes file for the old plots #########
#
# Beam modes
#  2 SETUP
#  3 INJ PROBE BEAM
#  4 INJ SETUP BEAM
#  5 INJ PHYSICS BEAM
#  6 PREPARE RAMP
#  7 RAMP
#  8 FLAT TOP
#  9 SQUEEZE 
# 10 ADJUST
# 11 STABLE BEAMS 
# 12 UNSTABLE BEAMS
# 13 BEAM DUMP
# 14 RAMP DOWN
# 15 RECOVERY
# 16 INJECT AND DUMP
# 17 CIRCULATE AND DUMP
# 18 ABORT
# 19 CYCLING 
# 20 BEAM DUMP WARNING
# 21 NO BEAM
# 22 PREARE INJECTION
#
# We count the Beam Dump Warning period into the stable beam time

state = "wait"
cfill = 1
lfill = 0
csche = ""
cmode = ""
dstart = 0
fillHash = {}

# also make a copy of the TIMBER data get 
timberfile = os.path.join( config['workBaseDir'], str(year), "measurements", "timberdata.csv" )
bmfile = os.path.join( config['workBaseDir'], str(year), "measurements", "beam_modes_update.csv" )
tf = open( timberfile, "w" )
bm2 = open( bmfile, "w" )
bm2wr = csv.writer( bm2 )
bm2wr.writerow( ['Timestamp (UNIX Format)','HX:BMODE','HX:FILLN'] )
try :
    header = cr.next()
except StopIteration as exp:
    sw_stop = datetime.now()
    sw_delta = sw_stop - sw_start
    logger.log( "Timber command needed " + repr(sw_delta.seconds) + " seconds" );
    logger.log( "No data found in Timber! Bailing out..." )
    sys.exit()

tfwr = csv.writer( tf )
tfwr.writerow( header )

for row in cr:
    tfwr.writerow( row  ) # for the copy of the timberdata
    # in the end there is a zero length line
    if len(row) == 0:
        continue
    fillChange = False
    modeChange = False
    if row[2] <> "":
        cfill = int(row[2])
        fillChange = True
    if row[3] <> "":
        csche = row[3]
    if row[1] <> "":
        cmode = row[1]
        modeChange = True

    # advance until we have a fill number AND a scheme
    if (cfill == 1) or (csche == ""):
        continue

    ctime = int(row[0])

    # Write the update csv file when the filling scheme is not changed
    # to avoid double entries: the filling scheme is not part of this file.
    if row[3] == "":
        bm2wr.writerow(row[0:3] )

    # if the stable beam flag comes we start a fill
    if  modeChange and cmode == "11" :
        # check that we have a new fill number and a scheme
        if cfill <> lfill:
            fill = { 'fillno' : cfill,
                     'starttime' : ctime,
                     'scheme' : csche,
                     'sblength' : 0
                 }
            dstart = ctime
            state = "ongoing"
            lfill = cfill
        else:
            # we again go to stable beams in the same fill... We need to 
            # increase the fill length
            print "we go again to stable beams in fill " + repr(cfill) + " at " + repr(ctime)
            logger.log( "we go again to stable beams in fill " + repr(cfill) + " at " + repr(ctime))
            dstart = ctime
            state = "ongoing"
            
    # the stable beam flag goes away: increase the sblength and save the fill
    # in the fill hash. If a fill has multiple stable beam sections it will be
    # simply updated with the stop time and the new sblength
    elif( modeChange and cmode <> 11 and cmode <> 20  and state == "ongoing" ):
        fill['stoptime'] = ctime
        difft =  ctime - dstart
        print "adding " + repr(difft) + " to sbtime of fill " +  str(cfill)
        fill['sblength'] += (ctime - dstart)
        fl = fill['sblength'] / 3600000.
        print "fill length now " + repr( fl ) + " hours"
        state = "stopped"
        fillHash[ cfill ] = fill

sw_stop = datetime.now()
sw_delta = sw_stop - sw_start
logger.log( "Timber command needed " + repr(sw_delta.seconds) + " seconds" );

bm2.close()
tf.close()

# merge the new csv data to the old csv file for beam modes
updateBeamModesCSV()
logger.log( "Merged old beam_modes.csv files with new data from request" )
# some post processing (timestamps...)
for no,fill in fillHash.iteritems():
    dt = datetime.fromtimestamp( fill['starttime']/1000.0)
    fill['start_sb'] = dt.strftime( '%Y-%m-%d %H:%M:%S' )
    dt = datetime.fromtimestamp( fill['stoptime']/1000.0)
    fill['stop_sb'] = dt.strftime( '%Y-%m-%d %H:%M:%S' )
    # cut the fractions of the second
    delta = timedelta( seconds = int(fill['sblength']/1000) )
    fill['length_sb'] = str(delta)

# Now do the output

fillTable = joinFillTable( fillHash )

# The filltable json goes to the web directory
dstfile = os.path.join( config['webBaseDir'],str(year),"FillTable.json")
fd = open( dstfile, "w" )
json.dump( fillTable, fd )
fd.close()

logger.log( 'Fill Table created')
