/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
    // Define changes to default configuration here. For example:
    // config.language = 'fr';
    // config.uiColor = '#AADC6E';
    
    config.toolbarGroups = [
	{ name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
 	{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },			// Group's name will be used to create voice label.
 	{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
        { name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
 	{ name: 'links' },
 	{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
	{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
	{ name: 'links' },
	{ name: 'insert' },
	{ name: 'styles' },
	{ name: 'colors' },
	{ name: 'tools' },
	{ name: 'others' },
    ];
    //config.removePlugins = 'iframe';
    config.toolbarLocation = 'bottom';
    //config.toolbarCanCollapse = true;
    //config.toolbarStartupExpanded = false;
    //config.autoGrow_onStartup = true;
    //config.autoGrow_bottomSpace = 100;
    //config.scayt_autoStartup = true;
    config.height = 600;
    config.extraPlugins = 'widget';
    config.extraPlugins = 'lineutils';
    config.extraPlugins = 'indico';
//    config.contentsCss = ['./contents.css', 'ckeditor/contents.css'];
    config.templates_files = [ '/notes/resources/templates/templates.js'];
    config.allowedContent = true;
//    config.extraPlugins = 'wordcount';
    config.wordcount = {
	showWordCount: true,
	showCharCount: true,
	countHTML: false
    };
    config.keystrokes = [
      [ CKEDITOR.CTRL + 78 /* N */, 'numberedlist' ],
      [ CKEDITOR.CTRL + 77 /* M */, 'bulletedlist' ],
   ];
};
