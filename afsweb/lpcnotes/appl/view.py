# -*- coding: utf-8 -*- 
import json
import string
import re
from cgi import escape
from logger import debug,error,log,flushmsg


def printHead( base ):
    res = '<!DOCTYPE html>\n\
<html>\n\
<head>\n\
<title>Text Document</title>\n\
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">\n\
<link rel="stylesheet" type="text/css" href="' + base + '/resources/document.css">\n\
<link rel="stylesheet" type="text/css" href="' + base + '/resources/jquery/jquery-ui.min.css">\n\
<script type="text/javascript" src=\"' + base + '/resources/jquery/jquery.js" ></script>\n\
<script type="text/javascript" src=\"' + base + '/resources/jquery/jquery-ui.min.js" ></script>\n\
<script type="text/javascript" src=\"' + base + '/resources/Documents.js" ></script>\n\
<script type="text/javascript" src=\"' + base + '/resources/ProjectManager.js" ></script>\n\
<script type="text/javascript" src="' + base + '/resources/ckeditor/ckeditor.js" ></script>\n\
</head>\n\
';
    res += '''
<body onload="getDocumentTree()">
  <div id="topbar" onmouseover="toggleControl()"></div>
  <div id="pmbar" onmouseover="togglePmControl()"></div>
<div style="height:5px;"></div>
<div id="debug"></div>
<div id="log"></div>
<div id="error"></div>
<div id="deleteConfirmation" title="WARNING">You are about to delete a record<br>or an entire directory with its content. <br> Are you sure you want to do this?</div>
<script>
  var Doc = new ProjectManager( "myDoc", { "baseurl" : "''' + base + '''", divid : "document", url : "docaction" } ); 
</script>

<div class="hide" id="documentCrtl">
    Name:<input type="text" name="documentName" id="documentName" value=""/>
    Category:<select name="categoryChooser" id="categoryChooser"></select>
    <input type="button" onclick="Doc.insertNewDocument( \'d\' )" value="new document"/>
    <input type="button" onclick="Doc.insertNewDocument( \'c\' )" value="new category"/>
    <input type="button" onclick="Doc.moveTo()" value="move to category"/>
    <input type="button" onclick="Doc.rename()" value="rename"/>
    <input type="button" onclick="Doc.delete()" value="delete"/>
</div>
<div class="hide" id="PmCrtl">
    Name:<input type="text" name="documentName" id="documentName" value=""/>
    Category:<select name="categoryChooser" id="categoryChooser"></select>
    <input type="button" onclick="Doc.insertNewDocument( \'d\' )" value="new document"/>
    <input type="button" onclick="Doc.insertNewDocument( \'c\' )" value="new category"/>
    <input type="button" onclick="Doc.moveTo()" value="move to category"/>
    <input type="button" onclick="Doc.rename()" value="rename"/>
    <input type="button" onclick="Doc.delete()" value="delete"/>
</div>

<div class="head" id="documentHead"></div>

<table class="navhead" width="100%"><tr>
<td><div id="nav"></div></td>
<td align="right">
<!--
<input class="fbutton" type="button" onclick="Doc.toggleEdit()" value="edt"/>
-->
<input id="ganttbutton" class="searchbutton" type="image" onclick="Doc.getGantt()" width="25" src="''' + base + '''/resources/gantt.png"/>
<input id="editbutton" class="searchbutton" type="image" onclick="Doc.toggleEdit()" width="25" src="''' + base + '''/resources/edit.png"/>
<input class="searchbutton" type="image" onclick="Doc.makeTagList()" width="25" src="''' + base + '''/resources/tag.png" />
<input class="searchbutton" type="image" onclick="Doc.dosearch()" src="''' + base  + '''/resources/search-icon-20.png"/>
<br><input class="searchstring" type="text" name="searchstring" id="searchstring" value=""/>
</td>
</tr></table>
<table width=100%><tr><td id="title"></td><td class="right" id="state"></td></table>
<p></p>

<div class="doc" id="document"></div>
<div id="footspace"></div>
'''
    return res


def printTail():    
    res = flushmsg()
    res += "</body>\n\
</html>\n"
#    print "I am done"
    return res
