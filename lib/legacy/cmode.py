##
# @author Colin Barschel (colin.barschel@cern.ch)
# @file fill modes and times (e.g. time for SB and dump) base on logging DB
#
import matplotlib as mpl
import string
import os.path
#from matplotlib.dates import *
from mytools import *
#from config import *
import readconfig

config = readconfig.readconfig( "lumi_config.json" )

YEAR = config['year']
LPC_DATA_DIR = os.path.join( config['workBaseDir'], config['year'], "measurements" )

## contains the beam mode for each fills
""" LHC RAW beam_mode_enum
http://lhc-commissioning.web.cern.ch/lhc-commissioning/systems/data-exchange/published-topics/beam-modes.htm

SETUP 2
INJECTION PROBE BEAM 3
INJECTION SETUP BEAM 4
INJECTION PHYSICS BEAM 5
PREPARE RAMP 6
RAMP 7
FLAT TOP 8
SQUEIZE 9
ADJUST 10
STABLE BEAMS 11
UNSTABLE BEAMS 12
BEAM DUMP 13
RAMP DOWN 14
RECOVERY 15
INJECT AND DUMP 16
CIRCULATE AND DUMP 17
ABORT 18
CYCLING 19
BEAM DUMP WARNING 20
NO BEAM 21
PREPARE INJECTION 22
"""


class cmode():
    i = 0 # index of fillnr from the fill2010.txt data set
    fillnr = 0
    fp = None # fill pattern
    # beam mode. int is raw number from timber corresponding to beam mode
    bm = {
        'NOMODE':1,
        'SETUP':2,
        'INJECTION_PROBE_BEAM':3,
        'INJECTION_SETUP_BEAM':4,
        'INJECTION_PHYSICS_BEAM':5,
        'PREPARE_RAMP':6,
        'RAMP':7,
        'FLAT_TOP':8,
        'SQUEIZE':9,
        'ADJUST':10,
        'STABLE_BEAMS':11,
        'UNSTABLE_BEAMS':12,
        'BEAM_DUMP':13,
        'RAMP_DOWN':14,
        'RECOVERY':15,
        'INJECT_AND_DUMP':16,
        'CIRCULATE_AND_DUMP':17,
        'ABORT':18,
        'CYCLING':19,
        'BEAM_DUMP_WARNING':20,
        'NO_BEAM':21,
        'PREPARE_INJECTION':22
        }

    def __init__(self,fillnr=0,sbfile=''):
        if sbfile == '':
            sbfile = LPC_DATA_DIR+'/stable_beam.csv'
            sbfile = LPC_DATA_DIR+'/beam_modes.csv'
            
        print 'using file',sbfile
        self.fillnr = fillnr
        
        if os.path.isfile(sbfile):
            self.d = readcsv(sbfile,lastifempty=True,verbose=False)
        
            # add numeric time
            self.d['numTime'] = array('d')
            self.d['gpsTime'] = array('d')

            if self.d['date'][0].isdigit():
                for i in range(len(self.d['date'])):
                    # ignore miliseconds
                    self.d['numTime'].append(mpl.dates.epoch2num(int(self.d['date'][i])/1000))
                    self.d['gpsTime'].append(int(self.d['date'][i])/1000)
            else:
                for i in range(len(self.d['date'])):
                    self.d['numTime'].append(timeDateNum(self.d['date'][i]))
                    self.d['gpsTime'].append(timeDateGps(self.d['date'][i]))
            self.d['numTime'] = np.array(self.d['numTime'])
            self.d['gpsTime'] = np.array(self.d['gpsTime'])

            self.ok = True

        else:
            print 'file',sbfile,'not found -> no stable beam information'
            self.ok = False

        # correct problems from DB
        # 1617: fill 1617 is labled too early: first 2 1617 entries are for 1616
        # 1745: change of fill 1745 not present (dump 1744 is missing)
        j = 0
        d1617 = d1745 = False
        for i in range(len(self.d['date'])):
            #if j == 6:
            #    break
            if self.d['HX:FILLN'][i] == 1617 and not d1617:
                for j in range(i,i+2):
                    self.d['HX:FILLN'][j] = 1616
                d1617 = True

            if self.d['HX:FILLN'][i] == 1746 and not d1745:
                for j in range(i-10,i):
                    self.d['HX:FILLN'][j] = 1745
                d1745 = True

                


    def setFill(self,fillnr):
        self.fillnr = fillnr

    """ return index of fills with stable beam """
    def sbidx(self):
        idx = []
        fills = []

        candidate = 0
        for i in range(len(self.d['date'])):
            if self.d['HX:BMODE'][i] == self.bm['STABLE_BEAMS']\
                    and self.d['HX:FILLN'][i] not in fills:
                candidate = i

            #if self.d['HX:BMODE'][i] == self.bm['BEAM_DUMP']\
            #        and self.d['HX:FILLN'][i] not in fills\
            #        and self.d['HX:FILLN'][i] == self.d['HX:FILLN'][candidate]:

                if self.d['HX:FILLN'][candidate] < 1613:
                    continue
                
                idx.append(candidate)
                fills.append(self.d['HX:FILLN'][candidate])
        
        return idx

    def sbFills(self):
        return self.d['HX:FILLN'][self.sbidx()]

    """
    return time for this mode and fill (UTC)
    @format 0=time string UTC; 1=matplotlib numeric UTC; 2=UNIX UTC; 3=index
    """
    def mode(self,hxmode,fillnr=0,format=0):
        if fillnr == 0:
            fillnr = self.fillnr

        if hxmode in self.bm:
            hx = self.bm[hxmode]
        if str(hxmode).isdigit():
            hx = hxmode
            
        imode = 0

        f = 0 # actual fillnr in list
        bm = 0
        
        for i in range(1,len(self.d['date'])):

                
            f = int(self.d['HX:FILLN'][i])
            bm = int(self.d['HX:BMODE'][i])
                
            if imode == 0 and f == int(fillnr) and bm == hx:
                imode = i

        if imode == 0:
            print 'fill',fillnr,' mode',hx,' not found'
            

        if self.d['date'][0].isdigit():
            tnums = mpl.dates.epoch2num(int(self.d['date'][imode])/1000)
        else:
            tnums = timeDateNum(self.d['date'][imode])

        
        if format == 0:
            return self.d['date'][imode]
        elif format == 1:
            return tnums
        elif format == 2:
            return mpl.dates.num2epoch(tnums)
        elif format == 3:
            return imode

    """
    return stable beam time for this fill (UTC)
    @format 0=time string UTC; 1=matplotlib numeric UTC; 2=UNIX UTC; 3=index
    """
    def sb(self,fillnr=0,format=0):
        if fillnr == 0:
            fillnr = self.fillnr

        istable = 0
        idump = 0
        f = 0 # actual fillnr in list
        bm = 0
        
        for i in range(1,len(self.d['date'])):

                
            f = int(self.d['HX:FILLN'][i])
            bm = int(self.d['HX:BMODE'][i])
                
            if istable == 0\
                    and f == int(fillnr) and bm == self.bm['STABLE_BEAMS']:
                istable = i
                #print 'found stable',istable,f,bm

            #if idump == 0\
            #        and f == int(fillnr) and bm == self.bm['BEAM_DUMP']:
            
            if f == int(fillnr) and bm == self.bm['BEAM_DUMP']:
                idump = i
                #print 'found dump',idump,f,bm

            #print f,bm,i
            #if f > fillnr and idump == 0:   
            #    print 'fill',f,fillnr,' dump not found, take next event as dump'
            #    idump = istable + 1

        if istable == 0:
            print 'fill',fillnr,' stable beam not found'
        if idump == 0:
            print 'fill',fillnr,' dump not found, take next bmode as dump'
            idump = istable + 1
            if idump > len(self.d['date'])-1:
                idump = len(self.d['date'])-1
            

        if self.d['date'][0].isdigit():
            tnums = mpl.dates.epoch2num(int(self.d['date'][istable])/1000)
            tnumd = mpl.dates.epoch2num(int(self.d['date'][idump])/1000)
        else:
            tnums = timeDateNum(self.d['date'][istable])
            tnumd = timeDateNum(self.d['date'][idump])
        
        if format == 0:
            return self.d['date'][istable],self.d['date'][idump]
        elif format == 1:
            return tnums,tnumd
        elif format == 2:
            return mpl.dates.num2epoch(tnums),mpl.dates.num2epoch(tnumd)
        elif format == 3:
            return istable,idump

    """
    return dump time for this fill (UTC)
    @format 0=time string UTC; 1=matplotlib numeric UTC; 2=UNIX UTC
    """
    def dump(self,fillnr=0,format=0):
        return self.sb(fillnr,format)[1]

    def betastar(self,fillnr=0):
        if fillnr == 0:
            fillnr = self.fillnr

        isb,idump = self.sb(fillnr,format=3)
        

        print isb,idump

        if isb == 0 or idump == 0:
            return {}

        
        B = {'ATLAS':np.average(self.d['HX:BETASTAR_IP1'][isb:idump]/10),
             'ALICE':np.average(self.d['HX:BETASTAR_IP2'][isb:idump]/10),
             'CMS':np.average(self.d['HX:BETASTAR_IP5'][isb:idump]/10),
             'LHCb':np.average(self.d['HX:BETASTAR_IP8'][isb:idump]/10)}

        return B

    """ return half crossing angle per experiment and axis """
    def theta(self,fillnr=0):
        if fillnr == 0:
            fillnr = self.fillnr
        # ATLAS, CMS = 120 urad external
        # ALICE = 80 urad external + 70 urad internal
        # LHCb = 250 urad external +- 270 urad internal
        #
        # CMS, LHCb horizontal crossing (X)
        # ATLAS, ALICE vertical crossing (Y)

        lhcbinternal = 270
        if fillnr in [1642]:
            lhcbinternal = 0
            
        A = {'ATLAS':{'x':0.,'y':120.},
             'ALICE':{'x':0.,'y':220.}, # only if magnet on
             'CMS':{'x':120.,'y':0.},
             'LHCb':{'x':250+lhcbinternal,'y':0.}}

        return A

    def crossingPlane(self,expt):
        if expt == 'ATLAS' or expt == 'ALICE':
            return 'y'
        elif expt == 'CMS' or expt == 'LHCb':
            return 'x'
        else:
            print 'ERROR '+expt+' not a valid experiment'
        
## Test the cfile class
if __name__ == "__main__":
    if len(sys.argv) == 2:
        fillnr = int(sys.argv[1])
        sbfile = ''
    elif len(sys.argv) == 3:
        fillnr = int(sys.argv[1])
        sbfile = sys.argv[2]
    else:
        fillnr = 0
        sbfile = ''
        
    M = cmode(fillnr,sbfile)
    print 'Stable beam and dump time for fill',fillnr
    print M.sb(format=0)
    print M.sb(format=1)
    print M.sb(format=2)

    print 'setup',M.mode(2)
    print 'setup',M.mode('SETUP')
    print 'injection',M.mode(5,format=0)


    print M.sbidx()
    print M.sbFills()
    print M.sb(2338)

    #B = M.betastar(fillnr)
    #for k in B.keys():
    #    print k+' B*: '+str(B[k])

    
    #print 'Stable beam start:',F.sbGps()
    #print 'Fill Pattern file:',F.fillPatternFile()
    #print F.fillPatternFile(1005)
    #FP = F.fillPattern()
    #for ipnr in (1,2,8):
    #    cb = F.collidingBuckets(ipnr=ipnr)
    #    print 'colliding buckets for ip',ipnr,cb
    
