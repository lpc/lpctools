#!/usr/bin/python
import sys
import json
import readconfig
from FillProcessor import FillProcessor
from lpcutils import Logger

config = readconfig.readconfig( "lumi_config.json" )
logger = Logger( config["logfile"] )
logger.log( "updateLumiCache" )

fp = FillProcessor( config )

print sys.argv[1]
updates = json.loads( sys.argv[1] )

print updates

logger.log( "cache to be udpated for \n" + repr(updates) )
logger.logtime()

for (exp,upd) in updates.iteritems():
    for fillno in updates[exp]['fills']:
        logger.log( "updating " + exp + " fill " + str(fillno))
        fp.updateCache( fillno, [exp] )
logger.logtime()
