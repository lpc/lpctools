#!/bin/sh

export PATH=$PATH:\
/bin:/usr/bin:/sbin:/usr/sbin:\
/afs/cern.ch/user/l/lpc/lpctools/bin:\
/afs/cern.ch/user/l/lpc/lpctools/lib:\
./

export PYTHONPATH=\
/afs/cern.ch/user/l/lpc/lpctools/lib:\
/afs/cern.ch/user/l/lpc/lpctools/lib/legacy

export EOS_MGM_URL=root://eosproject-l.cern.ch
eosfusebind
